-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 23 juin 2019 à 22:31
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ecomcar2`
--

-- --------------------------------------------------------

--
-- Structure de la table `gift`
--

DROP TABLE IF EXISTS `gift`;
CREATE TABLE IF NOT EXISTS `gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refPiece` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prix` double DEFAULT NULL,
  `quantityDisponible` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `gift`
--

INSERT INTO `gift` (`id`, `refPiece`, `nom`, `prix`, `quantityDisponible`) VALUES
(2, 'RP28', 'Auto Goodride RP28 205/55 R16 91V', 44.08, 50),
(3, 'SW601', 'Auto Goodride SW601 165/70 R13 79T', 33.38, 30),
(4, '568643', 'ARGO Enjoliveurs', 15.38, 30),
(5, '08975566', 'KYB Kit de réparation, coupelle de suspension', 54.36, 15);

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20170912085504', '2019-06-21 20:28:07'),
('20170913125128', '2019-06-21 20:28:08'),
('20171003103916', '2019-06-21 20:28:08'),
('20180102140039', '2019-06-21 20:28:09'),
('20190109095211', '2019-06-21 20:28:11'),
('20190109160409', '2019-06-21 20:28:12');

-- --------------------------------------------------------

--
-- Structure de la table `odiseo_blog_article`
--

DROP TABLE IF EXISTS `odiseo_blog_article`;
CREATE TABLE IF NOT EXISTS `odiseo_blog_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `archived_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_EA96598A77153098` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `odiseo_blog_articles_categories`
--

DROP TABLE IF EXISTS `odiseo_blog_articles_categories`;
CREATE TABLE IF NOT EXISTS `odiseo_blog_articles_categories` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`category_id`),
  KEY `IDX_F090056C7294869C` (`article_id`),
  KEY `IDX_F090056C12469DE2` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `odiseo_blog_articles_channels`
--

DROP TABLE IF EXISTS `odiseo_blog_articles_channels`;
CREATE TABLE IF NOT EXISTS `odiseo_blog_articles_channels` (
  `article_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`channel_id`),
  KEY `IDX_A4C0CF5F7294869C` (`article_id`),
  KEY `IDX_A4C0CF5F72F5A1AA` (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `odiseo_blog_article_category`
--

DROP TABLE IF EXISTS `odiseo_blog_article_category`;
CREATE TABLE IF NOT EXISTS `odiseo_blog_article_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_AA59457F77153098` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `odiseo_blog_article_category_translation`
--

DROP TABLE IF EXISTS `odiseo_blog_article_category_translation`;
CREATE TABLE IF NOT EXISTS `odiseo_blog_article_category_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `odiseo_blog_article_category_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_2F9EF9492C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `odiseo_blog_article_image`
--

DROP TABLE IF EXISTS `odiseo_blog_article_image`;
CREATE TABLE IF NOT EXISTS `odiseo_blog_article_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F97399C97E3C61F9` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `odiseo_blog_article_translation`
--

DROP TABLE IF EXISTS `odiseo_blog_article_translation`;
CREATE TABLE IF NOT EXISTS `odiseo_blog_article_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `metaKeywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDescription` longtext COLLATE utf8_unicode_ci,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `odiseo_blog_article_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_7A606A2C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_address`
--

DROP TABLE IF EXISTS `sylius_address`;
CREATE TABLE IF NOT EXISTS `sylius_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `province_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B97FF0589395C3F3` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_address`
--

INSERT INTO `sylius_address` (`id`, `customer_id`, `first_name`, `last_name`, `phone_number`, `street`, `company`, `city`, `postcode`, `created_at`, `updated_at`, `country_code`, `province_code`, `province_name`) VALUES
(1, 17, 'Beulah', 'Heathcote', NULL, '993 Alayna Run Suite 698', NULL, 'Hettingerhaven', '06018', '2019-06-21 20:48:04', '2019-06-21 20:48:06', 'US', NULL, NULL),
(2, NULL, 'Beulah', 'Heathcote', NULL, '993 Alayna Run Suite 698', NULL, 'Hettingerhaven', '06018', '2019-06-21 20:48:04', '2019-06-21 20:48:06', 'US', NULL, NULL),
(3, NULL, 'Beulah', 'Heathcote', NULL, '993 Alayna Run Suite 698', NULL, 'Hettingerhaven', '06018', '2019-06-21 20:48:04', '2019-06-21 20:48:06', 'US', NULL, NULL),
(4, 4, 'Rowena', 'Feest', NULL, '1160 Schuster Vista Apt. 690', NULL, 'New Larissa', '18067-9425', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(5, NULL, 'Rowena', 'Feest', NULL, '1160 Schuster Vista Apt. 690', NULL, 'New Larissa', '18067-9425', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(6, NULL, 'Rowena', 'Feest', NULL, '1160 Schuster Vista Apt. 690', NULL, 'New Larissa', '18067-9425', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(7, 8, 'Annabel', 'Lowe', NULL, '50637 Raquel Union Suite 114', NULL, 'Jorgebury', '80808-5473', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(8, NULL, 'Annabel', 'Lowe', NULL, '50637 Raquel Union Suite 114', NULL, 'Jorgebury', '80808-5473', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(9, NULL, 'Annabel', 'Lowe', NULL, '50637 Raquel Union Suite 114', NULL, 'Jorgebury', '80808-5473', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(10, 16, 'Ila', 'Williamson', NULL, '933 O\'Keefe Street Suite 488', NULL, 'Siennaborough', '87935', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(11, NULL, 'Ila', 'Williamson', NULL, '933 O\'Keefe Street Suite 488', NULL, 'Siennaborough', '87935', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(12, NULL, 'Ila', 'Williamson', NULL, '933 O\'Keefe Street Suite 488', NULL, 'Siennaborough', '87935', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(13, 21, 'Deron', 'Williamson', NULL, '619 Kuhlman Lodge', NULL, 'Priceburgh', '84157', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(14, NULL, 'Deron', 'Williamson', NULL, '619 Kuhlman Lodge', NULL, 'Priceburgh', '84157', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(15, NULL, 'Deron', 'Williamson', NULL, '619 Kuhlman Lodge', NULL, 'Priceburgh', '84157', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(16, 11, 'Leora', 'Graham', NULL, '74220 Sanford Prairie Apt. 521', NULL, 'North Nicoletteton', '05074', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(17, NULL, 'Leora', 'Graham', NULL, '74220 Sanford Prairie Apt. 521', NULL, 'North Nicoletteton', '05074', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(18, NULL, 'Leora', 'Graham', NULL, '74220 Sanford Prairie Apt. 521', NULL, 'North Nicoletteton', '05074', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(19, 2, 'Urban', 'Tillman', NULL, '122 German Trace', NULL, 'Lake Lanceport', '40027', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(20, NULL, 'Urban', 'Tillman', NULL, '122 German Trace', NULL, 'Lake Lanceport', '40027', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(21, NULL, 'Urban', 'Tillman', NULL, '122 German Trace', NULL, 'Lake Lanceport', '40027', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(22, 17, 'Cristopher', 'Wiegand', NULL, '87166 Rolfson Lodge Apt. 077', NULL, 'Barrowstown', '40177-3580', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(23, NULL, 'Cristopher', 'Wiegand', NULL, '87166 Rolfson Lodge Apt. 077', NULL, 'Barrowstown', '40177-3580', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(24, NULL, 'Cristopher', 'Wiegand', NULL, '87166 Rolfson Lodge Apt. 077', NULL, 'Barrowstown', '40177-3580', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(25, 2, 'Kyle', 'Pagac', NULL, '822 Carey Passage Suite 519', NULL, 'Port William', '69562-1558', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(26, NULL, 'Kyle', 'Pagac', NULL, '822 Carey Passage Suite 519', NULL, 'Port William', '69562-1558', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(27, NULL, 'Kyle', 'Pagac', NULL, '822 Carey Passage Suite 519', NULL, 'Port William', '69562-1558', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(28, 13, 'Filomena', 'Hodkiewicz', NULL, '78577 Moriah Prairie Suite 289', NULL, 'Estaside', '37580', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(29, NULL, 'Filomena', 'Hodkiewicz', NULL, '78577 Moriah Prairie Suite 289', NULL, 'Estaside', '37580', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(30, NULL, 'Filomena', 'Hodkiewicz', NULL, '78577 Moriah Prairie Suite 289', NULL, 'Estaside', '37580', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(31, 21, 'Andy', 'D\'Amore', NULL, '156 Mraz Ridge Apt. 889', NULL, 'Effertzshire', '57260-7662', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(32, NULL, 'Andy', 'D\'Amore', NULL, '156 Mraz Ridge Apt. 889', NULL, 'Effertzshire', '57260-7662', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(33, NULL, 'Andy', 'D\'Amore', NULL, '156 Mraz Ridge Apt. 889', NULL, 'Effertzshire', '57260-7662', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(34, 21, 'Afton', 'Harris', NULL, '91917 Mertz Spring Suite 375', NULL, 'South Keshaunhaven', '69239', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(35, NULL, 'Afton', 'Harris', NULL, '91917 Mertz Spring Suite 375', NULL, 'South Keshaunhaven', '69239', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(36, NULL, 'Afton', 'Harris', NULL, '91917 Mertz Spring Suite 375', NULL, 'South Keshaunhaven', '69239', '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'US', NULL, NULL),
(37, 12, 'Ansel', 'Gutmann', NULL, '97795 Feeney Loop', NULL, 'Dellfort', '45057', '2019-06-21 20:48:07', '2019-06-21 20:48:08', 'US', NULL, NULL),
(38, NULL, 'Ansel', 'Gutmann', NULL, '97795 Feeney Loop', NULL, 'Dellfort', '45057', '2019-06-21 20:48:07', '2019-06-21 20:48:08', 'US', NULL, NULL),
(39, NULL, 'Ansel', 'Gutmann', NULL, '97795 Feeney Loop', NULL, 'Dellfort', '45057', '2019-06-21 20:48:07', '2019-06-21 20:48:08', 'US', NULL, NULL),
(40, 19, 'Karley', 'Mills', NULL, '759 Bianka Circle', NULL, 'East Zackary', '29478-4494', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(41, NULL, 'Karley', 'Mills', NULL, '759 Bianka Circle', NULL, 'East Zackary', '29478-4494', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(42, NULL, 'Karley', 'Mills', NULL, '759 Bianka Circle', NULL, 'East Zackary', '29478-4494', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(43, 1, 'Brittany', 'Fay', NULL, '250 Legros View Suite 352', NULL, 'West Arlie', '51416-7449', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(44, NULL, 'Brittany', 'Fay', NULL, '250 Legros View Suite 352', NULL, 'West Arlie', '51416-7449', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(45, NULL, 'Brittany', 'Fay', NULL, '250 Legros View Suite 352', NULL, 'West Arlie', '51416-7449', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(46, 18, 'Jesse', 'Hansen', NULL, '62682 Dare Loaf Apt. 395', NULL, 'Jameyview', '71333-1688', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(47, NULL, 'Jesse', 'Hansen', NULL, '62682 Dare Loaf Apt. 395', NULL, 'Jameyview', '71333-1688', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(48, NULL, 'Jesse', 'Hansen', NULL, '62682 Dare Loaf Apt. 395', NULL, 'Jameyview', '71333-1688', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(49, 9, 'Rocio', 'Reilly', NULL, '51468 Heath Rapids', NULL, 'Connorstad', '84307', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(50, NULL, 'Rocio', 'Reilly', NULL, '51468 Heath Rapids', NULL, 'Connorstad', '84307', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(51, NULL, 'Rocio', 'Reilly', NULL, '51468 Heath Rapids', NULL, 'Connorstad', '84307', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(52, 6, 'Reid', 'Lang', NULL, '704 Monty Creek', NULL, 'Port Jakemouth', '43529-8748', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(53, NULL, 'Reid', 'Lang', NULL, '704 Monty Creek', NULL, 'Port Jakemouth', '43529-8748', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(54, NULL, 'Reid', 'Lang', NULL, '704 Monty Creek', NULL, 'Port Jakemouth', '43529-8748', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(55, 8, 'Claudie', 'Kuvalis', NULL, '434 Hermann Mills', NULL, 'Guillermofurt', '13688', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(56, NULL, 'Claudie', 'Kuvalis', NULL, '434 Hermann Mills', NULL, 'Guillermofurt', '13688', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(57, NULL, 'Claudie', 'Kuvalis', NULL, '434 Hermann Mills', NULL, 'Guillermofurt', '13688', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(58, 4, 'Name', 'Boehm', NULL, '9899 Ward Radial', NULL, 'Donnellyfort', '94304', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(59, NULL, 'Name', 'Boehm', NULL, '9899 Ward Radial', NULL, 'Donnellyfort', '94304', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(60, NULL, 'Name', 'Boehm', NULL, '9899 Ward Radial', NULL, 'Donnellyfort', '94304', '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'US', NULL, NULL),
(61, 8, 'Autumn', 'Wolf', NULL, '7052 Beatrice Lakes', 'Zulauf, Rowe and Brown', 'Tyramouth', '54237-3269', '2019-06-21 20:48:10', '2019-06-21 20:48:10', 'US', NULL, NULL),
(62, 13, 'Marge', 'Reichert', '(648) 299-0364 x78057', '32365 Kuphal Road Suite 904', NULL, 'Shannaton', '78697-7307', '2019-06-21 20:48:10', '2019-06-21 20:48:10', 'US', NULL, NULL),
(63, 17, 'Stacy', 'Kassulke', '292-512-0164 x438', '5022 Sabryna Light', NULL, 'Port Ericaburgh', '72239-6388', '2019-06-21 20:48:10', '2019-06-21 20:48:10', 'US', NULL, NULL),
(64, 9, 'Bryon', 'Labadie', '+1.442.581.0641', '723 Stanton Mountain Apt. 889', 'McLaughlin-Pfeffer', 'Vilmashire', '90651', '2019-06-21 20:48:10', '2019-06-21 20:48:10', 'US', NULL, NULL),
(65, 9, 'Tia', 'Douglas', NULL, '9858 Calista Forest Apt. 192', NULL, 'South Roxannebury', '80531', '2019-06-21 20:48:10', '2019-06-21 20:48:10', 'US', NULL, NULL),
(66, 19, 'Cathy', 'Fisher', NULL, '10418 Reichert Pines', 'Reynolds-Green', 'West Dinashire', '42471', '2019-06-21 20:48:10', '2019-06-21 20:48:10', 'US', NULL, NULL),
(67, 6, 'Korbin', 'Volkman', '1-641-375-0849', '49315 Mayert Squares Apt. 254', NULL, 'Goldnerside', '38747-5843', '2019-06-21 20:48:10', '2019-06-21 20:48:10', 'US', NULL, NULL),
(68, 9, 'Anastacio', 'Mayer', NULL, '787 Madonna Turnpike Apt. 757', 'Strosin-Pfeffer', 'Felicitymouth', '51017', '2019-06-21 20:48:10', '2019-06-21 20:48:10', 'US', NULL, NULL),
(69, 20, 'Hallie', 'Koss', '665-885-8074 x94561', '8016 Jones Inlet', NULL, 'East Kyleigh', '07586-8805', '2019-06-21 20:48:10', '2019-06-21 20:48:10', 'US', NULL, NULL),
(70, 19, 'Matteo', 'Sawayn', '580.883.6794', '2311 Veum Green', 'Dickens and Sons', 'Schadenmouth', '93140-5449', '2019-06-21 20:48:10', '2019-06-21 20:48:10', 'US', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_address_log_entries`
--

DROP TABLE IF EXISTS `sylius_address_log_entries`;
CREATE TABLE IF NOT EXISTS `sylius_address_log_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logged_at` datetime NOT NULL,
  `object_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` int(11) NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_address_log_entries`
--

INSERT INTO `sylius_address_log_entries` (`id`, `action`, `logged_at`, `object_id`, `object_class`, `version`, `data`, `username`) VALUES
(1, 'create', '2019-06-21 20:48:06', '1', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Beulah\";s:8:\"lastName\";s:9:\"Heathcote\";s:11:\"phoneNumber\";N;s:6:\"street\";s:24:\"993 Alayna Run Suite 698\";s:7:\"company\";N;s:4:\"city\";s:14:\"Hettingerhaven\";s:8:\"postcode\";s:5:\"06018\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(2, 'create', '2019-06-21 20:48:06', '2', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Beulah\";s:8:\"lastName\";s:9:\"Heathcote\";s:11:\"phoneNumber\";N;s:6:\"street\";s:24:\"993 Alayna Run Suite 698\";s:7:\"company\";N;s:4:\"city\";s:14:\"Hettingerhaven\";s:8:\"postcode\";s:5:\"06018\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(3, 'create', '2019-06-21 20:48:06', '3', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Beulah\";s:8:\"lastName\";s:9:\"Heathcote\";s:11:\"phoneNumber\";N;s:6:\"street\";s:24:\"993 Alayna Run Suite 698\";s:7:\"company\";N;s:4:\"city\";s:14:\"Hettingerhaven\";s:8:\"postcode\";s:5:\"06018\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(4, 'create', '2019-06-21 20:48:08', '4', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Rowena\";s:8:\"lastName\";s:5:\"Feest\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"1160 Schuster Vista Apt. 690\";s:7:\"company\";N;s:4:\"city\";s:11:\"New Larissa\";s:8:\"postcode\";s:10:\"18067-9425\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(5, 'create', '2019-06-21 20:48:08', '5', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Rowena\";s:8:\"lastName\";s:5:\"Feest\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"1160 Schuster Vista Apt. 690\";s:7:\"company\";N;s:4:\"city\";s:11:\"New Larissa\";s:8:\"postcode\";s:10:\"18067-9425\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(6, 'create', '2019-06-21 20:48:08', '6', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Rowena\";s:8:\"lastName\";s:5:\"Feest\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"1160 Schuster Vista Apt. 690\";s:7:\"company\";N;s:4:\"city\";s:11:\"New Larissa\";s:8:\"postcode\";s:10:\"18067-9425\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(7, 'create', '2019-06-21 20:48:08', '7', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:7:\"Annabel\";s:8:\"lastName\";s:4:\"Lowe\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"50637 Raquel Union Suite 114\";s:7:\"company\";N;s:4:\"city\";s:9:\"Jorgebury\";s:8:\"postcode\";s:10:\"80808-5473\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(8, 'create', '2019-06-21 20:48:08', '8', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:7:\"Annabel\";s:8:\"lastName\";s:4:\"Lowe\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"50637 Raquel Union Suite 114\";s:7:\"company\";N;s:4:\"city\";s:9:\"Jorgebury\";s:8:\"postcode\";s:10:\"80808-5473\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(9, 'create', '2019-06-21 20:48:08', '9', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:7:\"Annabel\";s:8:\"lastName\";s:4:\"Lowe\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"50637 Raquel Union Suite 114\";s:7:\"company\";N;s:4:\"city\";s:9:\"Jorgebury\";s:8:\"postcode\";s:10:\"80808-5473\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(10, 'create', '2019-06-21 20:48:08', '10', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:3:\"Ila\";s:8:\"lastName\";s:10:\"Williamson\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"933 O\'Keefe Street Suite 488\";s:7:\"company\";N;s:4:\"city\";s:13:\"Siennaborough\";s:8:\"postcode\";s:5:\"87935\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(11, 'create', '2019-06-21 20:48:08', '11', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:3:\"Ila\";s:8:\"lastName\";s:10:\"Williamson\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"933 O\'Keefe Street Suite 488\";s:7:\"company\";N;s:4:\"city\";s:13:\"Siennaborough\";s:8:\"postcode\";s:5:\"87935\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(12, 'create', '2019-06-21 20:48:08', '12', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:3:\"Ila\";s:8:\"lastName\";s:10:\"Williamson\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"933 O\'Keefe Street Suite 488\";s:7:\"company\";N;s:4:\"city\";s:13:\"Siennaborough\";s:8:\"postcode\";s:5:\"87935\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(13, 'create', '2019-06-21 20:48:08', '13', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Deron\";s:8:\"lastName\";s:10:\"Williamson\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"619 Kuhlman Lodge\";s:7:\"company\";N;s:4:\"city\";s:10:\"Priceburgh\";s:8:\"postcode\";s:5:\"84157\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(14, 'create', '2019-06-21 20:48:08', '14', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Deron\";s:8:\"lastName\";s:10:\"Williamson\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"619 Kuhlman Lodge\";s:7:\"company\";N;s:4:\"city\";s:10:\"Priceburgh\";s:8:\"postcode\";s:5:\"84157\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(15, 'create', '2019-06-21 20:48:08', '15', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Deron\";s:8:\"lastName\";s:10:\"Williamson\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"619 Kuhlman Lodge\";s:7:\"company\";N;s:4:\"city\";s:10:\"Priceburgh\";s:8:\"postcode\";s:5:\"84157\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(16, 'create', '2019-06-21 20:48:08', '16', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Leora\";s:8:\"lastName\";s:6:\"Graham\";s:11:\"phoneNumber\";N;s:6:\"street\";s:30:\"74220 Sanford Prairie Apt. 521\";s:7:\"company\";N;s:4:\"city\";s:18:\"North Nicoletteton\";s:8:\"postcode\";s:5:\"05074\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(17, 'create', '2019-06-21 20:48:08', '17', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Leora\";s:8:\"lastName\";s:6:\"Graham\";s:11:\"phoneNumber\";N;s:6:\"street\";s:30:\"74220 Sanford Prairie Apt. 521\";s:7:\"company\";N;s:4:\"city\";s:18:\"North Nicoletteton\";s:8:\"postcode\";s:5:\"05074\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(18, 'create', '2019-06-21 20:48:08', '18', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Leora\";s:8:\"lastName\";s:6:\"Graham\";s:11:\"phoneNumber\";N;s:6:\"street\";s:30:\"74220 Sanford Prairie Apt. 521\";s:7:\"company\";N;s:4:\"city\";s:18:\"North Nicoletteton\";s:8:\"postcode\";s:5:\"05074\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(19, 'create', '2019-06-21 20:48:08', '19', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Urban\";s:8:\"lastName\";s:7:\"Tillman\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"122 German Trace\";s:7:\"company\";N;s:4:\"city\";s:14:\"Lake Lanceport\";s:8:\"postcode\";s:5:\"40027\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(20, 'create', '2019-06-21 20:48:08', '20', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Urban\";s:8:\"lastName\";s:7:\"Tillman\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"122 German Trace\";s:7:\"company\";N;s:4:\"city\";s:14:\"Lake Lanceport\";s:8:\"postcode\";s:5:\"40027\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(21, 'create', '2019-06-21 20:48:08', '21', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Urban\";s:8:\"lastName\";s:7:\"Tillman\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"122 German Trace\";s:7:\"company\";N;s:4:\"city\";s:14:\"Lake Lanceport\";s:8:\"postcode\";s:5:\"40027\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(22, 'create', '2019-06-21 20:48:08', '22', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:10:\"Cristopher\";s:8:\"lastName\";s:7:\"Wiegand\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"87166 Rolfson Lodge Apt. 077\";s:7:\"company\";N;s:4:\"city\";s:11:\"Barrowstown\";s:8:\"postcode\";s:10:\"40177-3580\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(23, 'create', '2019-06-21 20:48:08', '23', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:10:\"Cristopher\";s:8:\"lastName\";s:7:\"Wiegand\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"87166 Rolfson Lodge Apt. 077\";s:7:\"company\";N;s:4:\"city\";s:11:\"Barrowstown\";s:8:\"postcode\";s:10:\"40177-3580\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(24, 'create', '2019-06-21 20:48:08', '24', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:10:\"Cristopher\";s:8:\"lastName\";s:7:\"Wiegand\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"87166 Rolfson Lodge Apt. 077\";s:7:\"company\";N;s:4:\"city\";s:11:\"Barrowstown\";s:8:\"postcode\";s:10:\"40177-3580\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(25, 'create', '2019-06-21 20:48:08', '25', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:4:\"Kyle\";s:8:\"lastName\";s:5:\"Pagac\";s:11:\"phoneNumber\";N;s:6:\"street\";s:27:\"822 Carey Passage Suite 519\";s:7:\"company\";N;s:4:\"city\";s:12:\"Port William\";s:8:\"postcode\";s:10:\"69562-1558\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(26, 'create', '2019-06-21 20:48:08', '26', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:4:\"Kyle\";s:8:\"lastName\";s:5:\"Pagac\";s:11:\"phoneNumber\";N;s:6:\"street\";s:27:\"822 Carey Passage Suite 519\";s:7:\"company\";N;s:4:\"city\";s:12:\"Port William\";s:8:\"postcode\";s:10:\"69562-1558\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(27, 'create', '2019-06-21 20:48:08', '27', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:4:\"Kyle\";s:8:\"lastName\";s:5:\"Pagac\";s:11:\"phoneNumber\";N;s:6:\"street\";s:27:\"822 Carey Passage Suite 519\";s:7:\"company\";N;s:4:\"city\";s:12:\"Port William\";s:8:\"postcode\";s:10:\"69562-1558\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(28, 'create', '2019-06-21 20:48:08', '28', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:8:\"Filomena\";s:8:\"lastName\";s:10:\"Hodkiewicz\";s:11:\"phoneNumber\";N;s:6:\"street\";s:30:\"78577 Moriah Prairie Suite 289\";s:7:\"company\";N;s:4:\"city\";s:8:\"Estaside\";s:8:\"postcode\";s:5:\"37580\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(29, 'create', '2019-06-21 20:48:08', '29', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:8:\"Filomena\";s:8:\"lastName\";s:10:\"Hodkiewicz\";s:11:\"phoneNumber\";N;s:6:\"street\";s:30:\"78577 Moriah Prairie Suite 289\";s:7:\"company\";N;s:4:\"city\";s:8:\"Estaside\";s:8:\"postcode\";s:5:\"37580\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(30, 'create', '2019-06-21 20:48:08', '30', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:8:\"Filomena\";s:8:\"lastName\";s:10:\"Hodkiewicz\";s:11:\"phoneNumber\";N;s:6:\"street\";s:30:\"78577 Moriah Prairie Suite 289\";s:7:\"company\";N;s:4:\"city\";s:8:\"Estaside\";s:8:\"postcode\";s:5:\"37580\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(31, 'create', '2019-06-21 20:48:08', '31', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:4:\"Andy\";s:8:\"lastName\";s:7:\"D\'Amore\";s:11:\"phoneNumber\";N;s:6:\"street\";s:23:\"156 Mraz Ridge Apt. 889\";s:7:\"company\";N;s:4:\"city\";s:12:\"Effertzshire\";s:8:\"postcode\";s:10:\"57260-7662\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(32, 'create', '2019-06-21 20:48:08', '32', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:4:\"Andy\";s:8:\"lastName\";s:7:\"D\'Amore\";s:11:\"phoneNumber\";N;s:6:\"street\";s:23:\"156 Mraz Ridge Apt. 889\";s:7:\"company\";N;s:4:\"city\";s:12:\"Effertzshire\";s:8:\"postcode\";s:10:\"57260-7662\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(33, 'create', '2019-06-21 20:48:08', '33', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:4:\"Andy\";s:8:\"lastName\";s:7:\"D\'Amore\";s:11:\"phoneNumber\";N;s:6:\"street\";s:23:\"156 Mraz Ridge Apt. 889\";s:7:\"company\";N;s:4:\"city\";s:12:\"Effertzshire\";s:8:\"postcode\";s:10:\"57260-7662\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(34, 'create', '2019-06-21 20:48:08', '34', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Afton\";s:8:\"lastName\";s:6:\"Harris\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"91917 Mertz Spring Suite 375\";s:7:\"company\";N;s:4:\"city\";s:18:\"South Keshaunhaven\";s:8:\"postcode\";s:5:\"69239\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(35, 'create', '2019-06-21 20:48:08', '35', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Afton\";s:8:\"lastName\";s:6:\"Harris\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"91917 Mertz Spring Suite 375\";s:7:\"company\";N;s:4:\"city\";s:18:\"South Keshaunhaven\";s:8:\"postcode\";s:5:\"69239\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(36, 'create', '2019-06-21 20:48:08', '36', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Afton\";s:8:\"lastName\";s:6:\"Harris\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"91917 Mertz Spring Suite 375\";s:7:\"company\";N;s:4:\"city\";s:18:\"South Keshaunhaven\";s:8:\"postcode\";s:5:\"69239\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(37, 'create', '2019-06-21 20:48:08', '37', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Ansel\";s:8:\"lastName\";s:7:\"Gutmann\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"97795 Feeney Loop\";s:7:\"company\";N;s:4:\"city\";s:8:\"Dellfort\";s:8:\"postcode\";s:5:\"45057\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(38, 'create', '2019-06-21 20:48:08', '38', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Ansel\";s:8:\"lastName\";s:7:\"Gutmann\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"97795 Feeney Loop\";s:7:\"company\";N;s:4:\"city\";s:8:\"Dellfort\";s:8:\"postcode\";s:5:\"45057\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(39, 'create', '2019-06-21 20:48:08', '39', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Ansel\";s:8:\"lastName\";s:7:\"Gutmann\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"97795 Feeney Loop\";s:7:\"company\";N;s:4:\"city\";s:8:\"Dellfort\";s:8:\"postcode\";s:5:\"45057\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(40, 'create', '2019-06-21 20:48:08', '40', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Karley\";s:8:\"lastName\";s:5:\"Mills\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"759 Bianka Circle\";s:7:\"company\";N;s:4:\"city\";s:12:\"East Zackary\";s:8:\"postcode\";s:10:\"29478-4494\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(41, 'create', '2019-06-21 20:48:08', '41', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Karley\";s:8:\"lastName\";s:5:\"Mills\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"759 Bianka Circle\";s:7:\"company\";N;s:4:\"city\";s:12:\"East Zackary\";s:8:\"postcode\";s:10:\"29478-4494\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(42, 'create', '2019-06-21 20:48:08', '42', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Karley\";s:8:\"lastName\";s:5:\"Mills\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"759 Bianka Circle\";s:7:\"company\";N;s:4:\"city\";s:12:\"East Zackary\";s:8:\"postcode\";s:10:\"29478-4494\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(43, 'create', '2019-06-21 20:48:08', '43', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:8:\"Brittany\";s:8:\"lastName\";s:3:\"Fay\";s:11:\"phoneNumber\";N;s:6:\"street\";s:25:\"250 Legros View Suite 352\";s:7:\"company\";N;s:4:\"city\";s:10:\"West Arlie\";s:8:\"postcode\";s:10:\"51416-7449\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(44, 'create', '2019-06-21 20:48:08', '44', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:8:\"Brittany\";s:8:\"lastName\";s:3:\"Fay\";s:11:\"phoneNumber\";N;s:6:\"street\";s:25:\"250 Legros View Suite 352\";s:7:\"company\";N;s:4:\"city\";s:10:\"West Arlie\";s:8:\"postcode\";s:10:\"51416-7449\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(45, 'create', '2019-06-21 20:48:08', '45', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:8:\"Brittany\";s:8:\"lastName\";s:3:\"Fay\";s:11:\"phoneNumber\";N;s:6:\"street\";s:25:\"250 Legros View Suite 352\";s:7:\"company\";N;s:4:\"city\";s:10:\"West Arlie\";s:8:\"postcode\";s:10:\"51416-7449\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(46, 'create', '2019-06-21 20:48:08', '46', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Jesse\";s:8:\"lastName\";s:6:\"Hansen\";s:11:\"phoneNumber\";N;s:6:\"street\";s:24:\"62682 Dare Loaf Apt. 395\";s:7:\"company\";N;s:4:\"city\";s:9:\"Jameyview\";s:8:\"postcode\";s:10:\"71333-1688\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(47, 'create', '2019-06-21 20:48:08', '47', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Jesse\";s:8:\"lastName\";s:6:\"Hansen\";s:11:\"phoneNumber\";N;s:6:\"street\";s:24:\"62682 Dare Loaf Apt. 395\";s:7:\"company\";N;s:4:\"city\";s:9:\"Jameyview\";s:8:\"postcode\";s:10:\"71333-1688\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(48, 'create', '2019-06-21 20:48:08', '48', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Jesse\";s:8:\"lastName\";s:6:\"Hansen\";s:11:\"phoneNumber\";N;s:6:\"street\";s:24:\"62682 Dare Loaf Apt. 395\";s:7:\"company\";N;s:4:\"city\";s:9:\"Jameyview\";s:8:\"postcode\";s:10:\"71333-1688\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(49, 'create', '2019-06-21 20:48:08', '49', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Rocio\";s:8:\"lastName\";s:6:\"Reilly\";s:11:\"phoneNumber\";N;s:6:\"street\";s:18:\"51468 Heath Rapids\";s:7:\"company\";N;s:4:\"city\";s:10:\"Connorstad\";s:8:\"postcode\";s:5:\"84307\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(50, 'create', '2019-06-21 20:48:08', '50', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Rocio\";s:8:\"lastName\";s:6:\"Reilly\";s:11:\"phoneNumber\";N;s:6:\"street\";s:18:\"51468 Heath Rapids\";s:7:\"company\";N;s:4:\"city\";s:10:\"Connorstad\";s:8:\"postcode\";s:5:\"84307\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(51, 'create', '2019-06-21 20:48:08', '51', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Rocio\";s:8:\"lastName\";s:6:\"Reilly\";s:11:\"phoneNumber\";N;s:6:\"street\";s:18:\"51468 Heath Rapids\";s:7:\"company\";N;s:4:\"city\";s:10:\"Connorstad\";s:8:\"postcode\";s:5:\"84307\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(52, 'create', '2019-06-21 20:48:08', '52', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:4:\"Reid\";s:8:\"lastName\";s:4:\"Lang\";s:11:\"phoneNumber\";N;s:6:\"street\";s:15:\"704 Monty Creek\";s:7:\"company\";N;s:4:\"city\";s:14:\"Port Jakemouth\";s:8:\"postcode\";s:10:\"43529-8748\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(53, 'create', '2019-06-21 20:48:08', '53', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:4:\"Reid\";s:8:\"lastName\";s:4:\"Lang\";s:11:\"phoneNumber\";N;s:6:\"street\";s:15:\"704 Monty Creek\";s:7:\"company\";N;s:4:\"city\";s:14:\"Port Jakemouth\";s:8:\"postcode\";s:10:\"43529-8748\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(54, 'create', '2019-06-21 20:48:08', '54', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:4:\"Reid\";s:8:\"lastName\";s:4:\"Lang\";s:11:\"phoneNumber\";N;s:6:\"street\";s:15:\"704 Monty Creek\";s:7:\"company\";N;s:4:\"city\";s:14:\"Port Jakemouth\";s:8:\"postcode\";s:10:\"43529-8748\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(55, 'create', '2019-06-21 20:48:08', '55', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:7:\"Claudie\";s:8:\"lastName\";s:7:\"Kuvalis\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"434 Hermann Mills\";s:7:\"company\";N;s:4:\"city\";s:13:\"Guillermofurt\";s:8:\"postcode\";s:5:\"13688\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(56, 'create', '2019-06-21 20:48:08', '56', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:7:\"Claudie\";s:8:\"lastName\";s:7:\"Kuvalis\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"434 Hermann Mills\";s:7:\"company\";N;s:4:\"city\";s:13:\"Guillermofurt\";s:8:\"postcode\";s:5:\"13688\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(57, 'create', '2019-06-21 20:48:08', '57', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:7:\"Claudie\";s:8:\"lastName\";s:7:\"Kuvalis\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"434 Hermann Mills\";s:7:\"company\";N;s:4:\"city\";s:13:\"Guillermofurt\";s:8:\"postcode\";s:5:\"13688\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(58, 'create', '2019-06-21 20:48:08', '58', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:4:\"Name\";s:8:\"lastName\";s:5:\"Boehm\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"9899 Ward Radial\";s:7:\"company\";N;s:4:\"city\";s:12:\"Donnellyfort\";s:8:\"postcode\";s:5:\"94304\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(59, 'create', '2019-06-21 20:48:08', '59', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:4:\"Name\";s:8:\"lastName\";s:5:\"Boehm\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"9899 Ward Radial\";s:7:\"company\";N;s:4:\"city\";s:12:\"Donnellyfort\";s:8:\"postcode\";s:5:\"94304\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(60, 'create', '2019-06-21 20:48:08', '60', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:4:\"Name\";s:8:\"lastName\";s:5:\"Boehm\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"9899 Ward Radial\";s:7:\"company\";N;s:4:\"city\";s:12:\"Donnellyfort\";s:8:\"postcode\";s:5:\"94304\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(61, 'create', '2019-06-21 20:48:10', '61', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Autumn\";s:8:\"lastName\";s:4:\"Wolf\";s:11:\"phoneNumber\";N;s:6:\"street\";s:19:\"7052 Beatrice Lakes\";s:7:\"company\";s:22:\"Zulauf, Rowe and Brown\";s:4:\"city\";s:9:\"Tyramouth\";s:8:\"postcode\";s:10:\"54237-3269\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(62, 'create', '2019-06-21 20:48:10', '62', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Marge\";s:8:\"lastName\";s:8:\"Reichert\";s:11:\"phoneNumber\";s:21:\"(648) 299-0364 x78057\";s:6:\"street\";s:27:\"32365 Kuphal Road Suite 904\";s:7:\"company\";N;s:4:\"city\";s:9:\"Shannaton\";s:8:\"postcode\";s:10:\"78697-7307\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(63, 'create', '2019-06-21 20:48:10', '63', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Stacy\";s:8:\"lastName\";s:8:\"Kassulke\";s:11:\"phoneNumber\";s:17:\"292-512-0164 x438\";s:6:\"street\";s:18:\"5022 Sabryna Light\";s:7:\"company\";N;s:4:\"city\";s:15:\"Port Ericaburgh\";s:8:\"postcode\";s:10:\"72239-6388\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(64, 'create', '2019-06-21 20:48:10', '64', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Bryon\";s:8:\"lastName\";s:7:\"Labadie\";s:11:\"phoneNumber\";s:15:\"+1.442.581.0641\";s:6:\"street\";s:29:\"723 Stanton Mountain Apt. 889\";s:7:\"company\";s:18:\"McLaughlin-Pfeffer\";s:4:\"city\";s:10:\"Vilmashire\";s:8:\"postcode\";s:5:\"90651\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(65, 'create', '2019-06-21 20:48:10', '65', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:3:\"Tia\";s:8:\"lastName\";s:7:\"Douglas\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"9858 Calista Forest Apt. 192\";s:7:\"company\";N;s:4:\"city\";s:17:\"South Roxannebury\";s:8:\"postcode\";s:5:\"80531\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(66, 'create', '2019-06-21 20:48:10', '66', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:5:\"Cathy\";s:8:\"lastName\";s:6:\"Fisher\";s:11:\"phoneNumber\";N;s:6:\"street\";s:20:\"10418 Reichert Pines\";s:7:\"company\";s:14:\"Reynolds-Green\";s:4:\"city\";s:14:\"West Dinashire\";s:8:\"postcode\";s:5:\"42471\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(67, 'create', '2019-06-21 20:48:10', '67', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Korbin\";s:8:\"lastName\";s:7:\"Volkman\";s:11:\"phoneNumber\";s:14:\"1-641-375-0849\";s:6:\"street\";s:29:\"49315 Mayert Squares Apt. 254\";s:7:\"company\";N;s:4:\"city\";s:11:\"Goldnerside\";s:8:\"postcode\";s:10:\"38747-5843\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(68, 'create', '2019-06-21 20:48:10', '68', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:9:\"Anastacio\";s:8:\"lastName\";s:5:\"Mayer\";s:11:\"phoneNumber\";N;s:6:\"street\";s:29:\"787 Madonna Turnpike Apt. 757\";s:7:\"company\";s:15:\"Strosin-Pfeffer\";s:4:\"city\";s:13:\"Felicitymouth\";s:8:\"postcode\";s:5:\"51017\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(69, 'create', '2019-06-21 20:48:10', '69', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Hallie\";s:8:\"lastName\";s:4:\"Koss\";s:11:\"phoneNumber\";s:19:\"665-885-8074 x94561\";s:6:\"street\";s:16:\"8016 Jones Inlet\";s:7:\"company\";N;s:4:\"city\";s:12:\"East Kyleigh\";s:8:\"postcode\";s:10:\"07586-8805\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL),
(70, 'create', '2019-06-21 20:48:10', '70', 'App\\Entity\\Addressing\\Address', 1, 'a:10:{s:9:\"firstName\";s:6:\"Matteo\";s:8:\"lastName\";s:6:\"Sawayn\";s:11:\"phoneNumber\";s:12:\"580.883.6794\";s:6:\"street\";s:15:\"2311 Veum Green\";s:7:\"company\";s:16:\"Dickens and Sons\";s:4:\"city\";s:12:\"Schadenmouth\";s:8:\"postcode\";s:10:\"93140-5449\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_adjustment`
--

DROP TABLE IF EXISTS `sylius_adjustment`;
CREATE TABLE IF NOT EXISTS `sylius_adjustment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `order_item_id` int(11) DEFAULT NULL,
  `order_item_unit_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `is_neutral` tinyint(1) NOT NULL,
  `is_locked` tinyint(1) NOT NULL,
  `origin_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ACA6E0F28D9F6D38` (`order_id`),
  KEY `IDX_ACA6E0F2E415FB15` (`order_item_id`),
  KEY `IDX_ACA6E0F2F720C233` (`order_item_unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_adjustment`
--

INSERT INTO `sylius_adjustment` (`id`, `order_id`, `order_item_id`, `order_item_unit_id`, `type`, `label`, `amount`, `is_neutral`, `is_locked`, `origin_code`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 1, 'order_promotion', 'Christmas', -558, 0, 0, 'christmas', '2019-06-21 20:48:05', '2019-06-21 20:48:06'),
(2, 1, NULL, NULL, 'shipping', 'UPS', 451, 0, 0, NULL, '2019-06-21 20:48:05', '2019-06-21 20:48:06'),
(3, NULL, NULL, 2, 'order_promotion', 'Christmas', -639, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(4, NULL, NULL, 3, 'order_promotion', 'Christmas', -639, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(5, NULL, NULL, 4, 'order_promotion', 'Christmas', -552, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(6, NULL, NULL, 5, 'order_promotion', 'Christmas', -850, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(7, NULL, NULL, 6, 'order_promotion', 'Christmas', -849, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(8, NULL, NULL, 7, 'order_promotion', 'Christmas', -849, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(9, 2, NULL, NULL, 'shipping', 'DHL Express', 5618, 0, 0, NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(10, NULL, NULL, 8, 'order_promotion', 'Christmas', -572, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(11, NULL, NULL, 9, 'order_promotion', 'Christmas', -572, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(12, NULL, NULL, 10, 'order_promotion', 'Christmas', -572, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(13, NULL, NULL, 11, 'order_promotion', 'Christmas', -292, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(14, NULL, NULL, 12, 'order_promotion', 'Christmas', -292, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(15, NULL, NULL, 13, 'order_promotion', 'Christmas', -292, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(16, NULL, NULL, 14, 'order_promotion', 'Christmas', -291, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(17, NULL, NULL, 15, 'order_promotion', 'Christmas', -291, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(18, NULL, NULL, 16, 'order_promotion', 'Christmas', -29, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(19, NULL, NULL, 17, 'order_promotion', 'Christmas', -29, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(20, NULL, NULL, 18, 'order_promotion', 'Christmas', -29, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(21, 3, NULL, NULL, 'shipping', 'DHL Express', 5618, 0, 0, NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(22, NULL, NULL, 19, 'order_promotion', 'Christmas', -732, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(23, NULL, NULL, 20, 'order_promotion', 'Christmas', -732, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(24, NULL, NULL, 21, 'order_promotion', 'Christmas', -732, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(25, NULL, NULL, 22, 'order_promotion', 'Christmas', -732, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(26, NULL, NULL, 23, 'order_promotion', 'Christmas', -732, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(27, NULL, NULL, 24, 'order_promotion', 'Christmas', -732, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(28, NULL, NULL, 25, 'order_promotion', 'Christmas', -731, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(29, 4, NULL, NULL, 'shipping', 'UPS', 451, 0, 0, NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(30, NULL, NULL, 26, 'order_promotion', 'Christmas', -738, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(31, NULL, NULL, 27, 'order_promotion', 'Christmas', -738, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(32, NULL, NULL, 28, 'order_promotion', 'Christmas', -738, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(33, NULL, NULL, 29, 'order_promotion', 'Christmas', -737, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(34, NULL, NULL, 30, 'order_promotion', 'Christmas', -754, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(35, 5, NULL, NULL, 'shipping', 'DHL Express', 5618, 0, 0, NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(36, NULL, NULL, 31, 'order_promotion', 'Christmas', -586, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(37, NULL, NULL, 32, 'order_promotion', 'Christmas', -586, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(38, NULL, NULL, 33, 'order_promotion', 'Christmas', -585, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(39, NULL, NULL, 34, 'order_promotion', 'Christmas', -572, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(40, 6, NULL, NULL, 'shipping', 'UPS', 451, 0, 0, NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(41, NULL, NULL, 35, 'order_promotion', 'Christmas', -372, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(42, 7, NULL, NULL, 'shipping', 'DHL Express', 5618, 0, 0, NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(43, NULL, NULL, 36, 'order_promotion', 'Christmas', -583, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(44, NULL, NULL, 37, 'order_promotion', 'Christmas', -583, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(45, NULL, NULL, 38, 'order_promotion', 'Christmas', -583, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(46, 8, NULL, NULL, 'shipping', 'DHL Express', 5618, 0, 0, NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(47, NULL, NULL, 39, 'order_promotion', 'Christmas', -656, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(48, NULL, NULL, 40, 'order_promotion', 'Christmas', -655, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(49, 9, NULL, NULL, 'shipping', 'DHL Express', 5618, 0, 0, NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(50, NULL, NULL, 41, 'order_promotion', 'Christmas', -515, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(51, NULL, NULL, 42, 'order_promotion', 'Christmas', -300, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(52, NULL, NULL, 43, 'order_promotion', 'Christmas', -299, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(53, NULL, NULL, 44, 'order_promotion', 'Christmas', -299, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(54, NULL, NULL, 45, 'order_promotion', 'Christmas', -299, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(55, NULL, NULL, 46, 'order_promotion', 'Christmas', -299, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(56, 10, NULL, NULL, 'shipping', 'DHL Express', 5618, 0, 0, NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(57, NULL, NULL, 47, 'order_promotion', 'Christmas', -805, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(58, NULL, NULL, 48, 'order_promotion', 'Christmas', -805, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(59, NULL, NULL, 49, 'order_promotion', 'Christmas', -805, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(60, 11, NULL, NULL, 'shipping', 'UPS', 451, 0, 0, NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(61, NULL, NULL, 50, 'order_promotion', 'Christmas', -113, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(62, NULL, NULL, 51, 'order_promotion', 'Christmas', -112, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(63, NULL, NULL, 52, 'order_promotion', 'Christmas', -112, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(64, NULL, NULL, 53, 'order_promotion', 'Christmas', -49, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(65, NULL, NULL, 54, 'order_promotion', 'Christmas', -117, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(66, NULL, NULL, 55, 'order_promotion', 'Christmas', -117, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(67, NULL, NULL, 56, 'order_promotion', 'Christmas', -116, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(68, NULL, NULL, 57, 'order_promotion', 'Christmas', -116, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(69, NULL, NULL, 58, 'order_promotion', 'Christmas', -116, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(70, NULL, NULL, 59, 'order_promotion', 'Christmas', -591, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(71, NULL, NULL, 60, 'order_promotion', 'Christmas', -591, 0, 0, 'christmas', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(72, 12, NULL, NULL, 'shipping', 'UPS', 451, 0, 0, NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(73, NULL, NULL, 61, 'order_promotion', 'Christmas', -582, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(74, NULL, NULL, 62, 'order_promotion', 'Christmas', -582, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(75, NULL, NULL, 63, 'order_promotion', 'Christmas', -581, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(76, NULL, NULL, 64, 'order_promotion', 'Christmas', -581, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(77, NULL, NULL, 65, 'order_promotion', 'Christmas', -581, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(78, NULL, NULL, 66, 'order_promotion', 'Christmas', -198, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(79, NULL, NULL, 67, 'order_promotion', 'Christmas', -198, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(80, NULL, NULL, 68, 'order_promotion', 'Christmas', -198, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(81, NULL, NULL, 69, 'order_promotion', 'Christmas', -191, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(82, NULL, NULL, 70, 'order_promotion', 'Christmas', -190, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(83, NULL, NULL, 71, 'order_promotion', 'Christmas', -190, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(84, 13, NULL, NULL, 'shipping', 'DHL Express', 5618, 0, 0, NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(85, NULL, NULL, 72, 'order_promotion', 'Christmas', -211, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(86, NULL, NULL, 73, 'order_promotion', 'Christmas', -211, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(87, NULL, NULL, 74, 'order_promotion', 'Christmas', -211, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(88, NULL, NULL, 75, 'order_promotion', 'Christmas', -211, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(89, NULL, NULL, 76, 'order_promotion', 'Christmas', -210, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(90, 14, NULL, NULL, 'shipping', 'UPS', 451, 0, 0, NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(91, NULL, NULL, 77, 'order_promotion', 'Christmas', -396, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(92, NULL, NULL, 78, 'order_promotion', 'Christmas', -396, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(93, NULL, NULL, 79, 'order_promotion', 'Christmas', -395, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(94, NULL, NULL, 80, 'order_promotion', 'Christmas', -395, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(95, NULL, NULL, 81, 'order_promotion', 'Christmas', -395, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(96, NULL, NULL, 82, 'order_promotion', 'Christmas', -2, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(97, NULL, NULL, 83, 'order_promotion', 'Christmas', -2, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(98, NULL, NULL, 84, 'order_promotion', 'Christmas', -1, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(99, NULL, NULL, 85, 'order_promotion', 'Christmas', -738, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(100, NULL, NULL, 86, 'order_promotion', 'Christmas', -738, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(101, NULL, NULL, 87, 'order_promotion', 'Christmas', -738, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(102, NULL, NULL, 88, 'order_promotion', 'Christmas', -737, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(103, 15, NULL, NULL, 'shipping', 'DHL Express', 5618, 0, 0, NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(104, NULL, NULL, 89, 'order_promotion', 'Christmas', -211, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(105, NULL, NULL, 90, 'order_promotion', 'Christmas', -211, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(106, NULL, NULL, 91, 'order_promotion', 'Christmas', -211, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(107, NULL, NULL, 92, 'order_promotion', 'Christmas', -211, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(108, NULL, NULL, 93, 'order_promotion', 'Christmas', -210, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(109, NULL, NULL, 94, 'order_promotion', 'Christmas', -329, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(110, NULL, NULL, 95, 'order_promotion', 'Christmas', -329, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(111, NULL, NULL, 96, 'order_promotion', 'Christmas', -329, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(112, NULL, NULL, 97, 'order_promotion', 'Christmas', -329, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(113, NULL, NULL, 98, 'order_promotion', 'Christmas', -329, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(114, NULL, NULL, 99, 'order_promotion', 'Christmas', -187, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(115, NULL, NULL, 100, 'order_promotion', 'Christmas', -187, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(116, NULL, NULL, 101, 'order_promotion', 'Christmas', -187, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(117, 16, NULL, NULL, 'shipping', 'DHL Express', 5618, 0, 0, NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(118, NULL, NULL, 102, 'order_promotion', 'New Year', -44, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(119, NULL, NULL, 102, 'order_promotion', 'Christmas', -359, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(120, NULL, NULL, 103, 'order_promotion', 'New Year', -43, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(121, NULL, NULL, 103, 'order_promotion', 'Christmas', -359, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(122, NULL, NULL, 104, 'order_promotion', 'New Year', -43, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(123, NULL, NULL, 104, 'order_promotion', 'Christmas', -358, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(124, NULL, NULL, 105, 'order_promotion', 'New Year', -43, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(125, NULL, NULL, 105, 'order_promotion', 'Christmas', -358, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(126, NULL, NULL, 106, 'order_promotion', 'New Year', -61, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(127, NULL, NULL, 106, 'order_promotion', 'Christmas', -504, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(128, NULL, NULL, 107, 'order_promotion', 'New Year', -61, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(129, NULL, NULL, 107, 'order_promotion', 'Christmas', -504, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(130, NULL, NULL, 108, 'order_promotion', 'New Year', -61, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(131, NULL, NULL, 108, 'order_promotion', 'Christmas', -504, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(132, NULL, NULL, 109, 'order_promotion', 'New Year', -61, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(133, NULL, NULL, 109, 'order_promotion', 'Christmas', -504, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(134, NULL, NULL, 110, 'order_promotion', 'New Year', -61, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(135, NULL, NULL, 110, 'order_promotion', 'Christmas', -504, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(136, NULL, NULL, 111, 'order_promotion', 'New Year', -75, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(137, NULL, NULL, 111, 'order_promotion', 'Christmas', -614, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(138, NULL, NULL, 112, 'order_promotion', 'New Year', -74, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(139, NULL, NULL, 112, 'order_promotion', 'Christmas', -614, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(140, NULL, NULL, 113, 'order_promotion', 'New Year', -94, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(141, NULL, NULL, 113, 'order_promotion', 'Christmas', -770, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(142, NULL, NULL, 114, 'order_promotion', 'New Year', -93, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(143, NULL, NULL, 114, 'order_promotion', 'Christmas', -770, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(144, NULL, NULL, 115, 'order_promotion', 'New Year', -93, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(145, NULL, NULL, 115, 'order_promotion', 'Christmas', -770, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(146, NULL, NULL, 116, 'order_promotion', 'New Year', -93, 0, 0, 'new_year', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(147, NULL, NULL, 116, 'order_promotion', 'Christmas', -770, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(148, 17, NULL, NULL, 'shipping', 'UPS', 451, 0, 0, NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(149, NULL, NULL, 117, 'order_promotion', 'Christmas', -425, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(150, NULL, NULL, 118, 'order_promotion', 'Christmas', -425, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(151, NULL, NULL, 119, 'order_promotion', 'Christmas', -425, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(152, NULL, NULL, 120, 'order_promotion', 'Christmas', -197, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(153, NULL, NULL, 121, 'order_promotion', 'Christmas', -196, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(154, NULL, NULL, 122, 'order_promotion', 'Christmas', -196, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(155, 18, NULL, NULL, 'shipping', 'DHL Express', 5618, 0, 0, NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(156, NULL, NULL, 123, 'order_promotion', 'Christmas', -849, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(157, NULL, NULL, 124, 'order_promotion', 'Christmas', -849, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(158, 19, NULL, NULL, 'shipping', 'DHL Express', 5618, 0, 0, NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(159, NULL, NULL, 125, 'order_promotion', 'Christmas', -640, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(160, NULL, NULL, 126, 'order_promotion', 'Christmas', -639, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(161, NULL, NULL, 127, 'order_promotion', 'Christmas', -639, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(162, NULL, NULL, 128, 'order_promotion', 'Christmas', -486, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(163, NULL, NULL, 129, 'order_promotion', 'Christmas', -485, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(164, NULL, NULL, 130, 'order_promotion', 'Christmas', -485, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(165, NULL, NULL, 131, 'order_promotion', 'Christmas', -485, 0, 0, 'christmas', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(166, 20, NULL, NULL, 'shipping', 'UPS', 451, 0, 0, NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_admin_api_access_token`
--

DROP TABLE IF EXISTS `sylius_admin_api_access_token`;
CREATE TABLE IF NOT EXISTS `sylius_admin_api_access_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expires_at` int(11) DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_2AA4915D5F37A13B` (`token`),
  KEY `IDX_2AA4915D19EB6921` (`client_id`),
  KEY `IDX_2AA4915DA76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_admin_api_access_token`
--

INSERT INTO `sylius_admin_api_access_token` (`id`, `client_id`, `user_id`, `token`, `expires_at`, `scope`) VALUES
(1, 1, 2, 'SampleToken', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_admin_api_auth_code`
--

DROP TABLE IF EXISTS `sylius_admin_api_auth_code`;
CREATE TABLE IF NOT EXISTS `sylius_admin_api_auth_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` longtext COLLATE utf8_unicode_ci NOT NULL,
  `expires_at` int(11) DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E366D8485F37A13B` (`token`),
  KEY `IDX_E366D84819EB6921` (`client_id`),
  KEY `IDX_E366D848A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_admin_api_client`
--

DROP TABLE IF EXISTS `sylius_admin_api_client`;
CREATE TABLE IF NOT EXISTS `sylius_admin_api_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `random_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uris` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `allowed_grant_types` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_admin_api_client`
--

INSERT INTO `sylius_admin_api_client` (`id`, `random_id`, `redirect_uris`, `secret`, `allowed_grant_types`) VALUES
(1, 'demo_client', 'a:0:{}', 'secret_demo_client', 'a:1:{i:0;s:8:\"password\";}');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_admin_api_refresh_token`
--

DROP TABLE IF EXISTS `sylius_admin_api_refresh_token`;
CREATE TABLE IF NOT EXISTS `sylius_admin_api_refresh_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expires_at` int(11) DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9160E3FA5F37A13B` (`token`),
  KEY `IDX_9160E3FA19EB6921` (`client_id`),
  KEY `IDX_9160E3FAA76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_admin_user`
--

DROP TABLE IF EXISTS `sylius_admin_user`;
CREATE TABLE IF NOT EXISTS `sylius_admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `email_verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale_code` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `encoder_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_admin_user`
--

INSERT INTO `sylius_admin_user` (`id`, `username`, `username_canonical`, `enabled`, `salt`, `password`, `last_login`, `password_reset_token`, `password_requested_at`, `email_verification_token`, `verified_at`, `locked`, `expires_at`, `credentials_expire_at`, `roles`, `email`, `email_canonical`, `created_at`, `updated_at`, `first_name`, `last_name`, `locale_code`, `encoder_name`) VALUES
(1, 'sylius', 'sylius', 1, 'hzdexxb33lcswsoc0kw8oc488kgsk8w', '$argon2i$v=19$m=1024,t=2,p=2$YWZnNHJpYlVOdzJlcWl4aA$a0ef1CFlobLovhYdjksrsaajR/omae9cLRHl+2y5OPI', '2019-06-23 21:51:57', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:26:\"ROLE_ADMINISTRATION_ACCESS\";}', 'sylius@example.com', 'sylius@example.com', '2019-06-21 20:47:50', '2019-06-23 21:51:57', 'John', 'Doe', 'en_US', 'argon2i'),
(2, 'api', 'api', 1, '2607wtziirvo8s04k000ks4s0ks0o80', '$argon2i$v=19$m=1024,t=2,p=2$WEFwMnZzUkNzb3lOdjhZdw$WQ1l7DKsNLcYRqZ7HNdOoQ6gC8eXxBlhWG2N2xK1aXE', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:2:{i:0;s:26:\"ROLE_ADMINISTRATION_ACCESS\";i:1;s:15:\"ROLE_API_ACCESS\";}', 'api@example.com', 'api@example.com', '2019-06-21 20:47:50', '2019-06-21 20:47:50', 'Luke', 'Brushwood', 'en_US', 'argon2i');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_channel`
--

DROP TABLE IF EXISTS `sylius_channel`;
CREATE TABLE IF NOT EXISTS `sylius_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_locale_id` int(11) NOT NULL,
  `base_currency_id` int(11) NOT NULL,
  `default_tax_zone_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `theme_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_calculation_strategy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skipping_shipping_step_allowed` tinyint(1) NOT NULL,
  `skipping_payment_step_allowed` tinyint(1) NOT NULL,
  `account_verification_required` tinyint(1) NOT NULL,
  `shop_billing_data_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_16C8119E77153098` (`code`),
  UNIQUE KEY `UNIQ_16C8119EB5282EDF` (`shop_billing_data_id`),
  KEY `IDX_16C8119E743BF776` (`default_locale_id`),
  KEY `IDX_16C8119E3101778E` (`base_currency_id`),
  KEY `IDX_16C8119EA978C17` (`default_tax_zone_id`),
  KEY `IDX_16C8119EE551C011` (`hostname`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_channel`
--

INSERT INTO `sylius_channel` (`id`, `default_locale_id`, `base_currency_id`, `default_tax_zone_id`, `code`, `name`, `color`, `description`, `enabled`, `hostname`, `created_at`, `updated_at`, `theme_name`, `tax_calculation_strategy`, `contact_email`, `skipping_shipping_step_allowed`, `skipping_payment_step_allowed`, `account_verification_required`, `shop_billing_data_id`) VALUES
(1, 1, 1, 1, 'US_WEB', 'US Web Store', 'FloralWhite', NULL, 1, 'localhost', '2019-06-21 20:47:48', '2019-06-21 20:47:48', NULL, 'order_items_based', NULL, 0, 0, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_channel_currencies`
--

DROP TABLE IF EXISTS `sylius_channel_currencies`;
CREATE TABLE IF NOT EXISTS `sylius_channel_currencies` (
  `channel_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  PRIMARY KEY (`channel_id`,`currency_id`),
  KEY `IDX_AE491F9372F5A1AA` (`channel_id`),
  KEY `IDX_AE491F9338248176` (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_channel_currencies`
--

INSERT INTO `sylius_channel_currencies` (`channel_id`, `currency_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_channel_locales`
--

DROP TABLE IF EXISTS `sylius_channel_locales`;
CREATE TABLE IF NOT EXISTS `sylius_channel_locales` (
  `channel_id` int(11) NOT NULL,
  `locale_id` int(11) NOT NULL,
  PRIMARY KEY (`channel_id`,`locale_id`),
  KEY `IDX_786B7A8472F5A1AA` (`channel_id`),
  KEY `IDX_786B7A84E559DFD1` (`locale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_channel_locales`
--

INSERT INTO `sylius_channel_locales` (`channel_id`, `locale_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_channel_pricing`
--

DROP TABLE IF EXISTS `sylius_channel_pricing`;
CREATE TABLE IF NOT EXISTS `sylius_channel_pricing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_variant_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `original_price` int(11) DEFAULT NULL,
  `channel_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_variant_channel_idx` (`product_variant_id`,`channel_code`),
  KEY `IDX_7801820CA80EF684` (`product_variant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=335 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_channel_pricing`
--

INSERT INTO `sylius_channel_pricing` (`id`, `product_variant_id`, `price`, `original_price`, `channel_code`) VALUES
(4, 4, 752, NULL, 'US_WEB'),
(5, 5, 224, NULL, 'US_WEB'),
(6, 6, 374, NULL, 'US_WEB'),
(7, 7, 198, NULL, 'US_WEB'),
(8, 8, 91, NULL, 'US_WEB'),
(9, 9, 684, NULL, 'US_WEB'),
(10, 10, 825, NULL, 'US_WEB'),
(11, 11, 387, NULL, 'US_WEB'),
(12, 12, 500, NULL, 'US_WEB'),
(13, 13, 259, NULL, 'US_WEB'),
(14, 14, 757, NULL, 'US_WEB'),
(15, 15, 289, NULL, 'US_WEB'),
(16, 16, 747, NULL, 'US_WEB'),
(17, 17, 510, NULL, 'US_WEB'),
(18, 18, 646, NULL, 'US_WEB'),
(19, 19, 999, NULL, 'US_WEB'),
(20, 20, 797, NULL, 'US_WEB'),
(21, 21, 408, NULL, 'US_WEB'),
(22, 22, 263, NULL, 'US_WEB'),
(23, 23, 748, NULL, 'US_WEB'),
(24, 24, 504, NULL, 'US_WEB'),
(25, 25, 889, NULL, 'US_WEB'),
(26, 26, 330, NULL, 'US_WEB'),
(27, 27, 694, NULL, 'US_WEB'),
(28, 28, 430, NULL, 'US_WEB'),
(29, 29, 198, NULL, 'US_WEB'),
(30, 30, 711, NULL, 'US_WEB'),
(31, 31, 259, NULL, 'US_WEB'),
(32, 32, 220, NULL, 'US_WEB'),
(33, 33, 880, NULL, 'US_WEB'),
(34, 34, 654, NULL, 'US_WEB'),
(35, 35, 757, NULL, 'US_WEB'),
(36, 36, 8, NULL, 'US_WEB'),
(37, 37, 673, NULL, 'US_WEB'),
(38, 38, 762, NULL, 'US_WEB'),
(39, 39, 438, NULL, 'US_WEB'),
(40, 40, 581, NULL, 'US_WEB'),
(41, 41, 167, NULL, 'US_WEB'),
(42, 42, 463, NULL, 'US_WEB'),
(43, 43, 284, NULL, 'US_WEB'),
(44, 44, 695, NULL, 'US_WEB'),
(45, 45, 797, NULL, 'US_WEB'),
(46, 46, 116, NULL, 'US_WEB'),
(47, 47, 946, NULL, 'US_WEB'),
(48, 48, 989, NULL, 'US_WEB'),
(49, 49, 442, NULL, 'US_WEB'),
(50, 50, 802, NULL, 'US_WEB'),
(51, 51, 903, NULL, 'US_WEB'),
(52, 52, 519, NULL, 'US_WEB'),
(53, 53, 34, NULL, 'US_WEB'),
(54, 54, 687, NULL, 'US_WEB'),
(55, 55, 606, NULL, 'US_WEB'),
(56, 56, 711, NULL, 'US_WEB'),
(57, 57, 685, NULL, 'US_WEB'),
(58, 58, 653, NULL, 'US_WEB'),
(59, 59, 395, NULL, 'US_WEB'),
(60, 60, 351, NULL, 'US_WEB'),
(61, 61, 861, NULL, 'US_WEB'),
(62, 62, 773, NULL, 'US_WEB'),
(63, 63, 656, NULL, 'US_WEB'),
(64, 64, 58, NULL, 'US_WEB'),
(65, 65, 733, NULL, 'US_WEB'),
(66, 66, 336, NULL, 'US_WEB'),
(67, 67, 143, NULL, 'US_WEB'),
(68, 68, 879, NULL, 'US_WEB'),
(69, 69, 405, NULL, 'US_WEB'),
(70, 70, 701, NULL, 'US_WEB'),
(71, 71, 255, NULL, 'US_WEB'),
(72, 72, 660, NULL, 'US_WEB'),
(73, 73, 127, NULL, 'US_WEB'),
(74, 74, 435, NULL, 'US_WEB'),
(75, 75, 125, NULL, 'US_WEB'),
(76, 76, 413, NULL, 'US_WEB'),
(77, 77, 132, NULL, 'US_WEB'),
(78, 78, 523, NULL, 'US_WEB'),
(79, 79, 302, NULL, 'US_WEB'),
(80, 80, 607, NULL, 'US_WEB'),
(81, 81, 176, NULL, 'US_WEB'),
(82, 82, 352, NULL, 'US_WEB'),
(83, 83, 809, NULL, 'US_WEB'),
(84, 84, 233, NULL, 'US_WEB'),
(85, 85, 868, NULL, 'US_WEB'),
(86, 86, 156, NULL, 'US_WEB'),
(87, 87, 392, NULL, 'US_WEB'),
(88, 88, 490, NULL, 'US_WEB'),
(89, 89, 587, NULL, 'US_WEB'),
(90, 90, 197, NULL, 'US_WEB'),
(91, 91, 686, NULL, 'US_WEB'),
(92, 92, 947, NULL, 'US_WEB'),
(93, 93, 649, NULL, 'US_WEB'),
(94, 94, 248, NULL, 'US_WEB'),
(95, 95, 437, NULL, 'US_WEB'),
(96, 96, 4, NULL, 'US_WEB'),
(97, 97, 231, NULL, 'US_WEB'),
(98, 98, 739, NULL, 'US_WEB'),
(99, 99, 887, NULL, 'US_WEB'),
(100, 100, 689, NULL, 'US_WEB'),
(101, 101, 450, NULL, 'US_WEB'),
(102, 102, 465, NULL, 'US_WEB'),
(103, 103, 999, NULL, 'US_WEB'),
(104, 104, 457, NULL, 'US_WEB'),
(105, 105, 584, NULL, 'US_WEB'),
(106, 106, 986, NULL, 'US_WEB'),
(107, 107, 119, NULL, 'US_WEB'),
(108, 108, 13, NULL, 'US_WEB'),
(109, 109, 510, NULL, 'US_WEB'),
(110, 110, 873, NULL, 'US_WEB'),
(111, 111, 954, NULL, 'US_WEB'),
(112, 112, 454, NULL, 'US_WEB'),
(113, 113, 271, NULL, 'US_WEB'),
(114, 114, 762, NULL, 'US_WEB'),
(115, 115, 894, NULL, 'US_WEB'),
(116, 116, 134, NULL, 'US_WEB'),
(117, 117, 295, NULL, 'US_WEB'),
(118, 118, 355, NULL, 'US_WEB'),
(119, 119, 278, NULL, 'US_WEB'),
(120, 120, 223, NULL, 'US_WEB'),
(121, 121, 2, NULL, 'US_WEB'),
(122, 122, 763, NULL, 'US_WEB'),
(123, 123, 130, NULL, 'US_WEB'),
(124, 124, 571, NULL, 'US_WEB'),
(125, 125, 583, NULL, 'US_WEB'),
(126, 126, 795, NULL, 'US_WEB'),
(127, 127, 727, NULL, 'US_WEB'),
(128, 128, 53, NULL, 'US_WEB'),
(129, 129, 639, NULL, 'US_WEB'),
(130, 130, 294, NULL, 'US_WEB'),
(131, 131, 266, NULL, 'US_WEB'),
(132, 132, 309, NULL, 'US_WEB'),
(133, 133, 825, NULL, 'US_WEB'),
(134, 134, 245, NULL, 'US_WEB'),
(135, 135, 737, NULL, 'US_WEB'),
(136, 136, 974, NULL, 'US_WEB'),
(137, 137, 847, NULL, 'US_WEB'),
(138, 138, 915, NULL, 'US_WEB'),
(139, 139, 733, NULL, 'US_WEB'),
(140, 140, 914, NULL, 'US_WEB'),
(141, 141, 370, NULL, 'US_WEB'),
(142, 142, 604, NULL, 'US_WEB'),
(143, 143, 910, NULL, 'US_WEB'),
(144, 144, 818, NULL, 'US_WEB'),
(145, 145, 765, NULL, 'US_WEB'),
(146, 146, 483, NULL, 'US_WEB'),
(147, 147, 654, NULL, 'US_WEB'),
(148, 148, 433, NULL, 'US_WEB'),
(149, 149, 98, NULL, 'US_WEB'),
(150, 150, 17, NULL, 'US_WEB'),
(151, 151, 681, NULL, 'US_WEB'),
(152, 152, 967, NULL, 'US_WEB'),
(153, 153, 554, NULL, 'US_WEB'),
(154, 154, 630, NULL, 'US_WEB'),
(155, 155, 418, NULL, 'US_WEB'),
(156, 156, 165, NULL, 'US_WEB'),
(157, 157, 9, NULL, 'US_WEB'),
(158, 158, 247, NULL, 'US_WEB'),
(159, 159, 124, NULL, 'US_WEB'),
(160, 160, 282, NULL, 'US_WEB'),
(161, 161, 710, NULL, 'US_WEB'),
(162, 162, 548, NULL, 'US_WEB'),
(163, 163, 754, NULL, 'US_WEB'),
(164, 164, 705, NULL, 'US_WEB'),
(165, 165, 478, NULL, 'US_WEB'),
(166, 166, 496, NULL, 'US_WEB'),
(167, 167, 22, NULL, 'US_WEB'),
(168, 168, 623, NULL, 'US_WEB'),
(169, 169, 741, NULL, 'US_WEB'),
(170, 170, 241, NULL, 'US_WEB'),
(171, 171, 574, NULL, 'US_WEB'),
(172, 172, 346, NULL, 'US_WEB'),
(173, 173, 33, NULL, 'US_WEB'),
(174, 174, 417, NULL, 'US_WEB'),
(175, 175, 354, NULL, 'US_WEB'),
(176, 176, 731, NULL, 'US_WEB'),
(177, 177, 574, NULL, 'US_WEB'),
(178, 178, 273, NULL, 'US_WEB'),
(179, 179, 171, NULL, 'US_WEB'),
(180, 180, 414, NULL, 'US_WEB'),
(181, 181, 205, NULL, 'US_WEB'),
(182, 182, 452, NULL, 'US_WEB'),
(183, 183, 344, NULL, 'US_WEB'),
(184, 184, 620, NULL, 'US_WEB'),
(185, 185, 325, NULL, 'US_WEB'),
(186, 186, 604, NULL, 'US_WEB'),
(187, 187, 416, NULL, 'US_WEB'),
(188, 188, 901, NULL, 'US_WEB'),
(189, 189, 352, NULL, 'US_WEB'),
(190, 190, 143, NULL, 'US_WEB'),
(191, 191, 943, NULL, 'US_WEB'),
(192, 192, 785, NULL, 'US_WEB'),
(193, 193, 27, NULL, 'US_WEB'),
(194, 194, 645, NULL, 'US_WEB'),
(195, 195, 100, NULL, 'US_WEB'),
(196, 196, 956, NULL, 'US_WEB'),
(197, 197, 111, NULL, 'US_WEB'),
(198, 198, 330, NULL, 'US_WEB'),
(199, 199, 551, NULL, 'US_WEB'),
(200, 200, 951, NULL, 'US_WEB'),
(201, 201, 630, NULL, 'US_WEB'),
(202, 202, 646, NULL, 'US_WEB'),
(203, 203, 263, NULL, 'US_WEB'),
(204, 204, 586, NULL, 'US_WEB'),
(205, 205, 242, NULL, 'US_WEB'),
(206, 206, 422, NULL, 'US_WEB'),
(207, 207, 599, NULL, 'US_WEB'),
(208, 208, 795, NULL, 'US_WEB'),
(209, 209, 31, NULL, 'US_WEB'),
(210, 210, 367, NULL, 'US_WEB'),
(211, 211, 708, NULL, 'US_WEB'),
(212, 212, 223, NULL, 'US_WEB'),
(213, 213, 415, NULL, 'US_WEB'),
(214, 214, 256, NULL, 'US_WEB'),
(215, 215, 865, NULL, 'US_WEB'),
(216, 216, 99, NULL, 'US_WEB'),
(217, 217, 140, NULL, 'US_WEB'),
(218, 218, 219, NULL, 'US_WEB'),
(219, 219, 368, NULL, 'US_WEB'),
(220, 220, 102, NULL, 'US_WEB'),
(221, 221, 277, NULL, 'US_WEB'),
(222, 222, 299, NULL, 'US_WEB'),
(223, 223, 885, NULL, 'US_WEB'),
(224, 224, 786, NULL, 'US_WEB'),
(225, 225, 287, NULL, 'US_WEB'),
(226, 226, 336, NULL, 'US_WEB'),
(227, 227, 929, NULL, 'US_WEB'),
(228, 228, 668, NULL, 'US_WEB'),
(229, 229, 605, NULL, 'US_WEB'),
(230, 230, 867, NULL, 'US_WEB'),
(231, 231, 536, NULL, 'US_WEB'),
(232, 232, 743, NULL, 'US_WEB'),
(233, 233, 395, NULL, 'US_WEB'),
(234, 234, 253, NULL, 'US_WEB'),
(235, 235, 648, NULL, 'US_WEB'),
(236, 236, 398, NULL, 'US_WEB'),
(237, 237, 569, NULL, 'US_WEB'),
(238, 238, 705, NULL, 'US_WEB'),
(239, 239, 769, NULL, 'US_WEB'),
(240, 240, 339, NULL, 'US_WEB'),
(241, 241, 433, NULL, 'US_WEB'),
(242, 242, 470, NULL, 'US_WEB'),
(243, 243, 324, NULL, 'US_WEB'),
(244, 244, 191, NULL, 'US_WEB'),
(245, 245, 436, NULL, 'US_WEB'),
(246, 246, 63, NULL, 'US_WEB'),
(247, 247, 74, NULL, 'US_WEB'),
(248, 248, 50, NULL, 'US_WEB'),
(249, 249, 508, NULL, 'US_WEB'),
(250, 250, 843, NULL, 'US_WEB'),
(251, 251, 415, NULL, 'US_WEB'),
(252, 252, 523, NULL, 'US_WEB'),
(253, 253, 926, NULL, 'US_WEB'),
(254, 254, 740, NULL, 'US_WEB'),
(255, 255, 516, NULL, 'US_WEB'),
(256, 256, 736, NULL, 'US_WEB'),
(257, 257, 383, NULL, 'US_WEB'),
(258, 258, 771, NULL, 'US_WEB'),
(259, 259, 307, NULL, 'US_WEB'),
(260, 260, 193, NULL, 'US_WEB'),
(261, 261, 526, NULL, 'US_WEB'),
(262, 262, 9, NULL, 'US_WEB'),
(263, 263, 137, NULL, 'US_WEB'),
(264, 264, 444, NULL, 'US_WEB'),
(265, 265, 377, NULL, 'US_WEB'),
(266, 266, 936, NULL, 'US_WEB'),
(267, 267, 427, NULL, 'US_WEB'),
(268, 268, 436, NULL, 'US_WEB'),
(269, 269, 167, NULL, 'US_WEB'),
(270, 270, 294, NULL, 'US_WEB'),
(271, 271, 795, NULL, 'US_WEB'),
(272, 272, 871, NULL, 'US_WEB'),
(273, 273, 310, NULL, 'US_WEB'),
(274, 274, 202, NULL, 'US_WEB'),
(275, 275, 594, NULL, 'US_WEB'),
(276, 276, 22, NULL, 'US_WEB'),
(277, 277, 160, NULL, 'US_WEB'),
(278, 278, 852, NULL, 'US_WEB'),
(279, 279, 10, NULL, 'US_WEB'),
(280, 280, 192, NULL, 'US_WEB'),
(281, 281, 58, NULL, 'US_WEB'),
(282, 282, 123, NULL, 'US_WEB'),
(283, 283, 983, NULL, 'US_WEB'),
(284, 284, 440, NULL, 'US_WEB'),
(285, 285, 631, NULL, 'US_WEB'),
(286, 286, 703, NULL, 'US_WEB'),
(287, 287, 314, NULL, 'US_WEB'),
(288, 288, 156, NULL, 'US_WEB'),
(289, 289, 403, NULL, 'US_WEB'),
(290, 290, 2, NULL, 'US_WEB'),
(291, 291, 860, NULL, 'US_WEB'),
(292, 292, 131, NULL, 'US_WEB'),
(293, 293, 68, NULL, 'US_WEB'),
(294, 294, 313, NULL, 'US_WEB'),
(295, 295, 16, NULL, 'US_WEB'),
(296, 296, 773, NULL, 'US_WEB'),
(297, 297, 100, NULL, 'US_WEB'),
(298, 298, 112, NULL, 'US_WEB'),
(299, 299, 138, NULL, 'US_WEB'),
(300, 300, 64, NULL, 'US_WEB'),
(301, 301, 465, NULL, 'US_WEB'),
(302, 302, 765, NULL, 'US_WEB'),
(303, 303, 37, NULL, 'US_WEB'),
(304, 304, 563, NULL, 'US_WEB'),
(305, 305, 836, NULL, 'US_WEB'),
(306, 306, 208, NULL, 'US_WEB'),
(307, 307, 96, NULL, 'US_WEB'),
(308, 308, 859, NULL, 'US_WEB'),
(309, 309, 927, NULL, 'US_WEB'),
(310, 310, 910, NULL, 'US_WEB'),
(311, 311, 896, NULL, 'US_WEB'),
(312, 312, 934, NULL, 'US_WEB'),
(313, 313, 176, NULL, 'US_WEB'),
(314, 314, 342, NULL, 'US_WEB'),
(315, 315, 293, NULL, 'US_WEB'),
(316, 316, 152, NULL, 'US_WEB'),
(317, 317, 595, NULL, 'US_WEB'),
(318, 318, 531, NULL, 'US_WEB'),
(319, 319, 260, NULL, 'US_WEB'),
(320, 320, 775, NULL, 'US_WEB'),
(321, 321, 32, NULL, 'US_WEB'),
(322, 322, 284, NULL, 'US_WEB'),
(323, 323, 844, NULL, 'US_WEB'),
(324, 324, 251, NULL, 'US_WEB'),
(325, 325, 291, NULL, 'US_WEB'),
(326, 326, 156, NULL, 'US_WEB'),
(327, 327, 361, NULL, 'US_WEB'),
(328, 328, 567, NULL, 'US_WEB'),
(329, 329, 5, NULL, 'US_WEB'),
(330, 330, 343, NULL, 'US_WEB'),
(331, 331, 860000, NULL, 'US_WEB'),
(332, 332, 1156400, 1156400, 'US_WEB'),
(333, 333, 1000000, 1000000, 'US_WEB'),
(334, 334, 5000000, 5000000, 'US_WEB');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_country`
--

DROP TABLE IF EXISTS `sylius_country`;
CREATE TABLE IF NOT EXISTS `sylius_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E74256BF77153098` (`code`),
  KEY `IDX_E74256BF77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_country`
--

INSERT INTO `sylius_country` (`id`, `code`, `enabled`) VALUES
(1, 'US', 1);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_currency`
--

DROP TABLE IF EXISTS `sylius_currency`;
CREATE TABLE IF NOT EXISTS `sylius_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_96EDD3D077153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_currency`
--

INSERT INTO `sylius_currency` (`id`, `code`, `created_at`, `updated_at`) VALUES
(1, 'USD', '2019-06-21 20:47:48', '2019-06-21 20:47:48');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_customer`
--

DROP TABLE IF EXISTS `sylius_customer`;
CREATE TABLE IF NOT EXISTS `sylius_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_group_id` int(11) DEFAULT NULL,
  `default_address_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'u',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscribed_to_newsletter` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7E82D5E6E7927C74` (`email`),
  UNIQUE KEY `UNIQ_7E82D5E6A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_7E82D5E6BD94FB16` (`default_address_id`),
  KEY `IDX_7E82D5E6D2919A68` (`customer_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_customer`
--

INSERT INTO `sylius_customer` (`id`, `customer_group_id`, `default_address_id`, `email`, `email_canonical`, `first_name`, `last_name`, `birthday`, `gender`, `created_at`, `updated_at`, `phone_number`, `subscribed_to_newsletter`) VALUES
(1, 2, NULL, 'shop@example.com', 'shop@example.com', 'John', 'Doe', NULL, 'u', '2019-06-21 20:47:49', '2019-06-21 20:47:49', NULL, 0),
(2, 2, NULL, 'rae.glover@yahoo.com', 'rae.glover@yahoo.com', 'Lonny', 'Corkery', NULL, 'u', '2019-06-21 20:47:49', '2019-06-21 20:47:49', NULL, 0),
(3, 2, NULL, 'maxine.runolfsdottir@schmidt.com', 'maxine.runolfsdottir@schmidt.com', 'Agustin', 'Emmerich', NULL, 'u', '2019-06-21 20:47:49', '2019-06-21 20:47:49', NULL, 0),
(4, 1, NULL, 'kiehn.dixie@yahoo.com', 'kiehn.dixie@yahoo.com', 'Ettie', 'D\'Amore', NULL, 'u', '2019-06-21 20:47:49', '2019-06-21 20:47:49', NULL, 0),
(5, 1, NULL, 'xrempel@hotmail.com', 'xrempel@hotmail.com', 'Eriberto', 'Klein', NULL, 'u', '2019-06-21 20:47:49', '2019-06-21 20:47:49', NULL, 0),
(6, 1, NULL, 'rice.clinton@yahoo.com', 'rice.clinton@yahoo.com', 'Yasmine', 'Corkery', NULL, 'u', '2019-06-21 20:47:49', '2019-06-21 20:47:49', NULL, 0),
(7, 2, NULL, 'lucienne79@kessler.info', 'lucienne79@kessler.info', 'Bo', 'King', NULL, 'u', '2019-06-21 20:47:49', '2019-06-21 20:47:49', NULL, 0),
(8, 1, NULL, 'zboncak.kirk@conroy.com', 'zboncak.kirk@conroy.com', 'Hilton', 'Wehner', NULL, 'u', '2019-06-21 20:47:49', '2019-06-21 20:47:49', NULL, 0),
(9, 1, NULL, 'alberta.cummerata@koss.com', 'alberta.cummerata@koss.com', 'Moses', 'Reilly', NULL, 'u', '2019-06-21 20:47:49', '2019-06-21 20:47:49', NULL, 0),
(10, 1, NULL, 'konopelski.earnest@murray.com', 'konopelski.earnest@murray.com', 'Angela', 'Reilly', NULL, 'u', '2019-06-21 20:47:49', '2019-06-21 20:47:49', NULL, 0),
(11, 2, NULL, 'elnora36@marvin.com', 'elnora36@marvin.com', 'Albert', 'Kreiger', NULL, 'u', '2019-06-21 20:47:50', '2019-06-21 20:47:50', NULL, 0),
(12, 2, NULL, 'rosenbaum.fleta@gmail.com', 'rosenbaum.fleta@gmail.com', 'Maximillia', 'Quigley', NULL, 'u', '2019-06-21 20:47:50', '2019-06-21 20:47:50', NULL, 0),
(13, 1, NULL, 'mayer.vinnie@hotmail.com', 'mayer.vinnie@hotmail.com', 'Therese', 'Quitzon', NULL, 'u', '2019-06-21 20:47:50', '2019-06-21 20:47:50', NULL, 0),
(14, 1, NULL, 'piper.rempel@dibbert.org', 'piper.rempel@dibbert.org', 'Cydney', 'Dare', NULL, 'u', '2019-06-21 20:47:50', '2019-06-21 20:47:50', NULL, 0),
(15, 2, NULL, 'ashanahan@williamson.com', 'ashanahan@williamson.com', 'Darian', 'Gutmann', NULL, 'u', '2019-06-21 20:47:50', '2019-06-21 20:47:50', NULL, 0),
(16, 1, NULL, 'bjones@yahoo.com', 'bjones@yahoo.com', 'Ashly', 'Kris', NULL, 'u', '2019-06-21 20:47:50', '2019-06-21 20:47:50', NULL, 0),
(17, 2, NULL, 'greenfelder.adan@gmail.com', 'greenfelder.adan@gmail.com', 'Golden', 'Parker', NULL, 'u', '2019-06-21 20:47:50', '2019-06-21 20:47:50', NULL, 0),
(18, 2, NULL, 'qlubowitz@gmail.com', 'qlubowitz@gmail.com', 'Amie', 'Barrows', NULL, 'u', '2019-06-21 20:47:50', '2019-06-21 20:47:50', NULL, 0),
(19, 2, NULL, 'nienow.helmer@hotmail.com', 'nienow.helmer@hotmail.com', 'Price', 'Cormier', NULL, 'u', '2019-06-21 20:47:50', '2019-06-21 20:47:50', NULL, 0),
(20, 1, NULL, 'hodkiewicz.beth@boehm.net', 'hodkiewicz.beth@boehm.net', 'Dina', 'Macejkovic', NULL, 'u', '2019-06-21 20:47:50', '2019-06-21 20:47:50', NULL, 0),
(21, 1, NULL, 'pwitting@gmail.com', 'pwitting@gmail.com', 'Jamey', 'Boyer', NULL, 'u', '2019-06-21 20:47:50', '2019-06-21 20:47:50', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_customer_group`
--

DROP TABLE IF EXISTS `sylius_customer_group`;
CREATE TABLE IF NOT EXISTS `sylius_customer_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7FCF9B0577153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_customer_group`
--

INSERT INTO `sylius_customer_group` (`id`, `code`, `name`) VALUES
(1, 'retail', 'Retail'),
(2, 'wholesale', 'Wholesale');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_exchange_rate`
--

DROP TABLE IF EXISTS `sylius_exchange_rate`;
CREATE TABLE IF NOT EXISTS `sylius_exchange_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_currency` int(11) NOT NULL,
  `target_currency` int(11) NOT NULL,
  `ratio` decimal(10,5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5F52B852A76BEEDB3FD5856` (`source_currency`,`target_currency`),
  KEY `IDX_5F52B852A76BEED` (`source_currency`),
  KEY `IDX_5F52B85B3FD5856` (`target_currency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_gateway_config`
--

DROP TABLE IF EXISTS `sylius_gateway_config`;
CREATE TABLE IF NOT EXISTS `sylius_gateway_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `factory_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `config` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_gateway_config`
--

INSERT INTO `sylius_gateway_config` (`id`, `gateway_name`, `factory_name`, `config`) VALUES
(1, 'Offline', 'offline', '[]'),
(2, 'Offline', 'offline', '[]');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_locale`
--

DROP TABLE IF EXISTS `sylius_locale`;
CREATE TABLE IF NOT EXISTS `sylius_locale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7BA1286477153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_locale`
--

INSERT INTO `sylius_locale` (`id`, `code`, `created_at`, `updated_at`) VALUES
(1, 'en_US', '2019-06-21 20:47:46', '2019-06-21 20:47:47');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_order`
--

DROP TABLE IF EXISTS `sylius_order`;
CREATE TABLE IF NOT EXISTS `sylius_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_address_id` int(11) DEFAULT NULL,
  `billing_address_id` int(11) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `promotion_coupon_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `checkout_completed_at` datetime DEFAULT NULL,
  `items_total` int(11) NOT NULL,
  `adjustments_total` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `currency_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `locale_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `checkout_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6196A1F996901F54` (`number`),
  UNIQUE KEY `UNIQ_6196A1F94D4CFF2B` (`shipping_address_id`),
  UNIQUE KEY `UNIQ_6196A1F979D0C0E4` (`billing_address_id`),
  KEY `IDX_6196A1F972F5A1AA` (`channel_id`),
  KEY `IDX_6196A1F917B24436` (`promotion_coupon_id`),
  KEY `IDX_6196A1F99395C3F3` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_order`
--

INSERT INTO `sylius_order` (`id`, `shipping_address_id`, `billing_address_id`, `channel_id`, `promotion_coupon_id`, `customer_id`, `number`, `notes`, `state`, `checkout_completed_at`, `items_total`, `adjustments_total`, `total`, `created_at`, `updated_at`, `currency_code`, `locale_code`, `checkout_state`, `payment_state`, `shipping_state`, `token_value`, `customer_ip`) VALUES
(1, 2, 3, 1, NULL, 17, '000000001', NULL, 'new', '2019-06-21 20:48:05', 98, 451, 549, '2019-06-21 20:48:04', '2019-06-21 20:48:06', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'nzh-xaid~1', NULL),
(2, 5, 6, 1, NULL, 4, '000000002', NULL, 'new', '2019-06-21 20:48:07', 772, 5618, 6390, '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'xq7wgHCleE', NULL),
(3, 8, 9, 1, NULL, 8, '000000003', NULL, 'new', '2019-06-21 20:48:07', 575, 5618, 6193, '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'YuPH7dd88s', NULL),
(4, 11, 12, 1, NULL, 16, '000000004', NULL, 'new', '2019-06-21 20:48:07', 904, 451, 1355, '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'FFfyFf55vZ', NULL),
(5, 14, 15, 1, NULL, 21, '000000005', NULL, 'new', '2019-06-21 20:48:07', 654, 5618, 6272, '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'E6tlS6GWra', NULL),
(6, 17, 18, 1, NULL, 11, '000000006', 'Nulla debitis harum et voluptatem esse corporis.', 'new', '2019-06-21 20:48:07', 411, 451, 862, '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'Th-QuKQoBh', NULL),
(7, 20, 21, 1, NULL, 2, '000000007', 'Minus necessitatibus aut suscipit autem.', 'new', '2019-06-21 20:48:07', 66, 5618, 5684, '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'M6nEljzSIY', NULL),
(8, 23, 24, 1, NULL, 17, '000000008', NULL, 'new', '2019-06-21 20:48:07', 309, 5618, 5927, '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'Y7IKMlQXIs', NULL),
(9, 26, 27, 1, NULL, 2, '000000009', NULL, 'new', '2019-06-21 20:48:07', 231, 5618, 5849, '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'JPIAgvOnh9', NULL),
(10, 29, 30, 1, NULL, 13, '000000010', NULL, 'new', '2019-06-21 20:48:07', 355, 5618, 5973, '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'NftkE4d87T', NULL),
(11, 32, 33, 1, NULL, 21, '000000011', NULL, 'new', '2019-06-21 20:48:07', 426, 451, 877, '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'FQ9FscamyG', NULL),
(12, 35, 36, 1, NULL, 21, '000000012', NULL, 'new', '2019-06-21 20:48:07', 379, 451, 830, '2019-06-21 20:48:07', '2019-06-21 20:48:07', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', '87Q3RZHBAk', NULL),
(13, 38, 39, 1, NULL, 12, '000000013', NULL, 'new', '2019-06-21 20:48:08', 719, 5618, 6337, '2019-06-21 20:48:07', '2019-06-21 20:48:08', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'MY-tahzTVx', NULL),
(14, 41, 42, 1, NULL, 19, '000000014', NULL, 'new', '2019-06-21 20:48:08', 186, 451, 637, '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'JM1oTwTgBg', NULL),
(15, 44, 45, 1, NULL, 1, '000000015', NULL, 'new', '2019-06-21 20:48:08', 870, 5618, 6488, '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'uHEoHbbKMK', NULL),
(16, 47, 48, 1, NULL, 18, '000000016', NULL, 'new', '2019-06-21 20:48:08', 575, 5618, 6193, '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'Elba9C~nnX', NULL),
(17, 50, 51, 1, NULL, 9, '000000017', NULL, 'new', '2019-06-21 20:48:08', 1458, 451, 1909, '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', '5VYpoyPWv4', NULL),
(18, 53, 54, 1, NULL, 6, '000000018', NULL, 'new', '2019-06-21 20:48:08', 329, 5618, 5947, '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'm2cZG5xosd', NULL),
(19, 56, 57, 1, NULL, 8, '000000019', NULL, 'new', '2019-06-21 20:48:08', 300, 5618, 5918, '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'celwOLkgnq', NULL),
(20, 59, 60, 1, NULL, 4, '000000020', NULL, 'new', '2019-06-21 20:48:08', 681, 451, 1132, '2019-06-21 20:48:08', '2019-06-21 20:48:08', 'USD', 'en_US', 'completed', 'awaiting_payment', 'ready', 'CnN37JW9km', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_order_item`
--

DROP TABLE IF EXISTS `sylius_order_item`;
CREATE TABLE IF NOT EXISTS `sylius_order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `variant_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `units_total` int(11) NOT NULL,
  `adjustments_total` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `is_immutable` tinyint(1) NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `variant_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_77B587ED8D9F6D38` (`order_id`),
  KEY `IDX_77B587ED3B69A9AF` (`variant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_order_item`
--

INSERT INTO `sylius_order_item` (`id`, `order_id`, `variant_id`, `quantity`, `unit_price`, `units_total`, `adjustments_total`, `total`, `is_immutable`, `product_name`, `variant_name`) VALUES
(1, 1, 63, 1, 656, 98, 0, 98, 0, 'Sticker \"sint\"', 'aut'),
(2, 2, 4, 2, 752, 226, 0, 226, 0, 'Mug \"atque\"', 'sed'),
(3, 2, 93, 1, 649, 97, 0, 97, 0, 'Book \"eveniet\" by Kyla Heidenreich', 'veniam'),
(4, 2, 103, 3, 999, 449, 0, 449, 0, 'Book \"eos\" by Noble Kshlerin I', 'perferendis'),
(5, 3, 37, 3, 673, 303, 0, 303, 0, 'Mug \"sapiente\"', 'magnam'),
(6, 3, 330, 5, 343, 257, 0, 257, 0, 'T-Shirt \"voluptatem\"', 'odit'),
(7, 3, 53, 3, 34, 15, 0, 15, 0, 'Sticker \"aut\"', 'laborum'),
(8, 4, 61, 2, 861, 258, 0, 258, 0, 'Sticker \"sint\"', 'aliquid'),
(9, 4, 61, 5, 861, 646, 0, 646, 0, 'Sticker \"sint\"', 'aliquid'),
(10, 5, 85, 4, 868, 521, 0, 521, 0, 'Sticker \"unde\"', 'esse'),
(11, 5, 99, 1, 887, 133, 0, 133, 0, 'Book \"vero\" by Mrs. Joelle Zulauf', 'doloremque'),
(12, 6, 100, 3, 689, 310, 0, 310, 0, 'Book \"possimus\" by Marlin Marquardt I', 'facere'),
(13, 6, 37, 1, 673, 101, 0, 101, 0, 'Mug \"sapiente\"', 'magnam'),
(14, 7, 39, 1, 438, 66, 0, 66, 0, 'Mug \"sapiente\"', 'et'),
(15, 8, 91, 3, 686, 309, 0, 309, 0, 'Book \"quidem\" by Eladio Smitham', 'ut'),
(16, 9, 258, 2, 771, 231, 0, 231, 0, 'T-Shirt \"veniam\"', 'pariatur'),
(17, 10, 55, 1, 606, 91, 0, 91, 0, 'Sticker \"incidunt\"', 'ut'),
(18, 10, 82, 5, 352, 264, 0, 264, 0, 'Sticker \"velit\"', 'est'),
(19, 11, 92, 3, 947, 426, 0, 426, 0, 'Book \"ea\" by Mrs. Maggie Mitchell III', 'unde'),
(20, 12, 77, 3, 132, 59, 0, 59, 0, 'Sticker \"molestias\"', 'architecto'),
(21, 12, 64, 1, 58, 9, 0, 9, 0, 'Sticker \"harum\"', 'eos'),
(22, 12, 263, 5, 137, 103, 0, 103, 0, 'T-Shirt \"veniam\"', 'cumque'),
(23, 12, 44, 2, 695, 208, 0, 208, 0, 'Mug \"eligendi\"', 'magni'),
(24, 13, 9, 5, 684, 513, 0, 513, 0, 'Mug \"aut\"', 'voluptates'),
(25, 13, 84, 3, 233, 105, 0, 105, 0, 'Sticker \"velit\"', 'libero'),
(26, 13, 5, 3, 224, 101, 0, 101, 0, 'Mug \"atque\"', 'porro'),
(27, 14, 94, 2, 248, 74, 0, 74, 0, 'Book \"qui\" by Harrison Fritsch', 'aut'),
(28, 14, 94, 3, 248, 112, 0, 112, 0, 'Book \"qui\" by Harrison Fritsch', 'aut'),
(29, 15, 102, 5, 465, 348, 0, 348, 0, 'Book \"architecto\" by Manuela Metz', 'consequatur'),
(30, 15, 121, 3, 2, 1, 0, 1, 0, 'T-Shirt \"vel\"', 'nobis'),
(31, 15, 85, 4, 868, 521, 0, 521, 0, 'Sticker \"unde\"', 'esse'),
(32, 16, 94, 5, 248, 186, 0, 186, 0, 'Book \"qui\" by Harrison Fritsch', 'aut'),
(33, 16, 11, 5, 387, 290, 0, 290, 0, 'Mug \"non\"', 'enim'),
(34, 16, 32, 3, 220, 99, 0, 99, 0, 'Mug \"ducimus\"', 'et'),
(35, 17, 102, 4, 465, 253, 0, 253, 0, 'Book \"architecto\" by Manuela Metz', 'consequatur'),
(36, 17, 34, 5, 654, 445, 0, 445, 0, 'Mug \"veritatis\"', 'explicabo'),
(37, 17, 45, 2, 797, 217, 0, 217, 0, 'Mug \"eligendi\"', 'in'),
(38, 17, 19, 4, 999, 543, 0, 543, 0, 'Mug \"tempore\"', 'doloribus'),
(39, 18, 12, 3, 500, 225, 0, 225, 0, 'Mug \"non\"', 'ipsa'),
(40, 18, 97, 3, 231, 104, 0, 104, 0, 'Book \"dolore\" by Jalen Russel', 'ea'),
(41, 19, 19, 2, 999, 300, 0, 300, 0, 'Mug \"tempore\"', 'doloribus'),
(42, 20, 4, 3, 752, 338, 0, 338, 0, 'Mug \"atque\"', 'sed'),
(43, 20, 124, 4, 571, 343, 0, 343, 0, 'T-Shirt \"vel\"', 'sed');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_order_item_unit`
--

DROP TABLE IF EXISTS `sylius_order_item_unit`;
CREATE TABLE IF NOT EXISTS `sylius_order_item_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_item_id` int(11) NOT NULL,
  `shipment_id` int(11) DEFAULT NULL,
  `adjustments_total` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_82BF226EE415FB15` (`order_item_id`),
  KEY `IDX_82BF226E7BE036FC` (`shipment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_order_item_unit`
--

INSERT INTO `sylius_order_item_unit` (`id`, `order_item_id`, `shipment_id`, `adjustments_total`, `created_at`, `updated_at`) VALUES
(1, 1, 1, -558, '2019-06-21 20:48:04', '2019-06-21 20:48:06'),
(2, 2, 2, -639, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(3, 2, 2, -639, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(4, 3, 2, -552, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(5, 4, 2, -850, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(6, 4, 2, -849, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(7, 4, 2, -849, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(8, 5, 3, -572, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(9, 5, 3, -572, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(10, 5, 3, -572, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(11, 6, 3, -292, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(12, 6, 3, -292, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(13, 6, 3, -292, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(14, 6, 3, -291, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(15, 6, 3, -291, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(16, 7, 3, -29, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(17, 7, 3, -29, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(18, 7, 3, -29, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(19, 8, 4, -732, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(20, 8, 4, -732, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(21, 9, 4, -732, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(22, 9, 4, -732, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(23, 9, 4, -732, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(24, 9, 4, -732, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(25, 9, 4, -731, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(26, 10, 5, -738, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(27, 10, 5, -738, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(28, 10, 5, -738, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(29, 10, 5, -737, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(30, 11, 5, -754, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(31, 12, 6, -586, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(32, 12, 6, -586, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(33, 12, 6, -585, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(34, 13, 6, -572, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(35, 14, 7, -372, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(36, 15, 8, -583, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(37, 15, 8, -583, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(38, 15, 8, -583, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(39, 16, 9, -656, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(40, 16, 9, -655, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(41, 17, 10, -515, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(42, 18, 10, -300, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(43, 18, 10, -299, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(44, 18, 10, -299, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(45, 18, 10, -299, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(46, 18, 10, -299, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(47, 19, 11, -805, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(48, 19, 11, -805, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(49, 19, 11, -805, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(50, 20, 12, -113, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(51, 20, 12, -112, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(52, 20, 12, -112, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(53, 21, 12, -49, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(54, 22, 12, -117, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(55, 22, 12, -117, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(56, 22, 12, -116, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(57, 22, 12, -116, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(58, 22, 12, -116, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(59, 23, 12, -591, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(60, 23, 12, -591, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(61, 24, 13, -582, '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(62, 24, 13, -582, '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(63, 24, 13, -581, '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(64, 24, 13, -581, '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(65, 24, 13, -581, '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(66, 25, 13, -198, '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(67, 25, 13, -198, '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(68, 25, 13, -198, '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(69, 26, 13, -191, '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(70, 26, 13, -190, '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(71, 26, 13, -190, '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(72, 27, 14, -211, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(73, 27, 14, -211, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(74, 28, 14, -211, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(75, 28, 14, -211, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(76, 28, 14, -210, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(77, 29, 15, -396, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(78, 29, 15, -396, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(79, 29, 15, -395, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(80, 29, 15, -395, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(81, 29, 15, -395, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(82, 30, 15, -2, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(83, 30, 15, -2, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(84, 30, 15, -1, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(85, 31, 15, -738, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(86, 31, 15, -738, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(87, 31, 15, -738, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(88, 31, 15, -737, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(89, 32, 16, -211, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(90, 32, 16, -211, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(91, 32, 16, -211, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(92, 32, 16, -211, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(93, 32, 16, -210, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(94, 33, 16, -329, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(95, 33, 16, -329, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(96, 33, 16, -329, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(97, 33, 16, -329, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(98, 33, 16, -329, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(99, 34, 16, -187, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(100, 34, 16, -187, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(101, 34, 16, -187, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(102, 35, 17, -403, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(103, 35, 17, -402, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(104, 35, 17, -401, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(105, 35, 17, -401, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(106, 36, 17, -565, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(107, 36, 17, -565, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(108, 36, 17, -565, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(109, 36, 17, -565, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(110, 36, 17, -565, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(111, 37, 17, -689, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(112, 37, 17, -688, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(113, 38, 17, -864, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(114, 38, 17, -863, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(115, 38, 17, -863, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(116, 38, 17, -863, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(117, 39, 18, -425, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(118, 39, 18, -425, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(119, 39, 18, -425, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(120, 40, 18, -197, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(121, 40, 18, -196, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(122, 40, 18, -196, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(123, 41, 19, -849, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(124, 41, 19, -849, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(125, 42, 20, -640, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(126, 42, 20, -639, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(127, 42, 20, -639, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(128, 43, 20, -486, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(129, 43, 20, -485, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(130, 43, 20, -485, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(131, 43, 20, -485, '2019-06-21 20:48:08', '2019-06-21 20:48:08');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_order_sequence`
--

DROP TABLE IF EXISTS `sylius_order_sequence`;
CREATE TABLE IF NOT EXISTS `sylius_order_sequence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_order_sequence`
--

INSERT INTO `sylius_order_sequence` (`id`, `idx`, `version`) VALUES
(1, 20, 2);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_payment`
--

DROP TABLE IF EXISTS `sylius_payment`;
CREATE TABLE IF NOT EXISTS `sylius_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `method_id` int(11) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `currency_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D9191BD419883967` (`method_id`),
  KEY `IDX_D9191BD48D9F6D38` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_payment`
--

INSERT INTO `sylius_payment` (`id`, `method_id`, `order_id`, `currency_code`, `amount`, `state`, `details`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'USD', 549, 'new', '[]', '2019-06-21 20:48:04', '2019-06-21 20:48:06'),
(2, 1, 2, 'USD', 6390, 'new', '[]', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(3, 2, 3, 'USD', 6193, 'new', '[]', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(4, 1, 4, 'USD', 1355, 'new', '[]', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(5, 1, 5, 'USD', 6272, 'new', '[]', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(6, 1, 6, 'USD', 862, 'new', '[]', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(7, 2, 7, 'USD', 5684, 'new', '[]', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(8, 1, 8, 'USD', 5927, 'new', '[]', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(9, 2, 9, 'USD', 5849, 'new', '[]', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(10, 1, 10, 'USD', 5973, 'new', '[]', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(11, 2, 11, 'USD', 877, 'new', '[]', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(12, 1, 12, 'USD', 830, 'new', '[]', '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(13, 1, 13, 'USD', 6337, 'new', '[]', '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(14, 2, 14, 'USD', 637, 'new', '[]', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(15, 2, 15, 'USD', 6488, 'new', '[]', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(16, 2, 16, 'USD', 6193, 'new', '[]', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(17, 1, 17, 'USD', 1909, 'new', '[]', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(18, 2, 18, 'USD', 5947, 'new', '[]', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(19, 1, 19, 'USD', 5918, 'new', '[]', '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(20, 1, 20, 'USD', 1132, 'new', '[]', '2019-06-21 20:48:08', '2019-06-21 20:48:08');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_payment_method`
--

DROP TABLE IF EXISTS `sylius_payment_method`;
CREATE TABLE IF NOT EXISTS `sylius_payment_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_config_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `environment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A75B0B0D77153098` (`code`),
  KEY `IDX_A75B0B0DF23D6140` (`gateway_config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_payment_method`
--

INSERT INTO `sylius_payment_method` (`id`, `gateway_config_id`, `code`, `environment`, `is_enabled`, `position`, `created_at`, `updated_at`) VALUES
(1, 1, 'cash_on_delivery', NULL, 1, 0, '2019-06-21 20:47:48', '2019-06-21 20:47:48'),
(2, 2, 'bank_transfer', NULL, 1, 1, '2019-06-21 20:47:49', '2019-06-21 20:47:49');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_payment_method_channels`
--

DROP TABLE IF EXISTS `sylius_payment_method_channels`;
CREATE TABLE IF NOT EXISTS `sylius_payment_method_channels` (
  `payment_method_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY (`payment_method_id`,`channel_id`),
  KEY `IDX_543AC0CC5AA1164F` (`payment_method_id`),
  KEY `IDX_543AC0CC72F5A1AA` (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_payment_method_channels`
--

INSERT INTO `sylius_payment_method_channels` (`payment_method_id`, `channel_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_payment_method_translation`
--

DROP TABLE IF EXISTS `sylius_payment_method_translation`;
CREATE TABLE IF NOT EXISTS `sylius_payment_method_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `instructions` longtext COLLATE utf8_unicode_ci,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_payment_method_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_966BE3A12C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_payment_method_translation`
--

INSERT INTO `sylius_payment_method_translation` (`id`, `translatable_id`, `name`, `description`, `instructions`, `locale`) VALUES
(1, 1, 'Cash on delivery', 'Ut vel nesciunt harum.', NULL, 'en_US'),
(2, 2, 'Bank transfer', 'Est necessitatibus placeat ullam odit non.', NULL, 'en_US');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_payment_security_token`
--

DROP TABLE IF EXISTS `sylius_payment_security_token`;
CREATE TABLE IF NOT EXISTS `sylius_payment_security_token` (
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:object)',
  `after_url` longtext COLLATE utf8_unicode_ci,
  `target_url` longtext COLLATE utf8_unicode_ci NOT NULL,
  `gateway_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product`
--

DROP TABLE IF EXISTS `sylius_product`;
CREATE TABLE IF NOT EXISTS `sylius_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_taxon_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `variant_selection_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `average_rating` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_677B9B7477153098` (`code`),
  KEY `IDX_677B9B74731E505` (`main_taxon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product`
--

INSERT INTO `sylius_product` (`id`, `main_taxon_id`, `code`, `created_at`, `updated_at`, `enabled`, `variant_selection_method`, `average_rating`) VALUES
(2, 2, '18f4dffe-cdb2-308f-ac6f-83878bc1a062', '2019-06-16 14:58:35', '2019-06-21 20:47:51', 1, 'match', 0),
(3, 2, 'bdbea748-3f59-3162-bb3e-ece47eca7daf', '2019-06-17 18:50:03', '2019-06-21 20:47:51', 1, 'match', 0),
(4, 2, '9200760b-ae9d-338c-9c30-d220768fc33d', '2019-06-17 08:54:08', '2019-06-21 20:47:52', 1, 'match', 0),
(5, 2, 'bf45361c-9c3c-3ca9-ada4-c7d284daabed', '2019-06-16 14:02:50', '2019-06-21 20:47:52', 0, 'match', 0),
(6, 2, '8507e667-3ce6-3cff-9efb-481c92553141', '2019-06-19 00:20:20', '2019-06-21 20:47:52', 0, 'match', 0),
(7, 2, '3b375da0-0844-3d2b-b6d0-1a23e8e37fa8', '2019-06-18 03:52:35', '2019-06-21 20:47:52', 1, 'match', 0),
(8, 2, '1b202c66-4b6d-396d-9b33-c11e13bb2ff1', '2019-06-17 15:17:41', '2019-06-21 20:47:52', 1, 'match', 0),
(9, 2, 'e853369a-2e46-32e0-8079-0f63aa838d74', '2019-06-15 01:56:33', '2019-06-21 20:47:52', 1, 'match', 0),
(10, 2, '3b8bf6bf-619d-3d32-aa46-405fd34ceb7d', '2019-06-15 08:43:07', '2019-06-21 20:47:52', 0, 'match', 0),
(11, 2, '0bef49bd-90c1-36da-a544-c5813c470eb0', '2019-06-15 12:31:50', '2019-06-21 20:47:53', 1, 'match', 0),
(12, 2, 'b06353c6-6ec6-3e9f-971d-419674bb6d3e', '2019-06-20 06:51:29', '2019-06-21 20:47:53', 1, 'match', 0),
(13, 2, 'bca41952-a193-3d7d-9607-a6c0cf730e8b', '2019-06-17 21:05:34', '2019-06-21 20:47:53', 1, 'match', 0),
(14, 2, '1bef578d-ce2b-30b6-b38c-534ab26991ee', '2019-06-21 18:22:35', '2019-06-21 20:47:53', 1, 'match', 0),
(15, 2, 'e61ebc69-3197-3bb5-a80c-ae67a9f377ad', '2019-06-18 03:10:41', '2019-06-21 20:47:53', 1, 'match', 0),
(16, 3, '67a04451-51a7-3579-bc98-e9374c193034', '2019-06-18 00:37:23', '2019-06-21 20:48:03', 1, 'choice', 1),
(17, 3, '72ea7df3-af00-3a27-987c-afabf6a1046f', '2019-06-20 08:46:32', '2019-06-21 20:47:55', 1, 'choice', 0),
(18, 3, 'f22bb104-1f21-336e-beed-04e5b559012c', '2019-06-17 08:12:38', '2019-06-21 20:47:55', 1, 'choice', 0),
(19, 3, 'a38ccc2b-5e04-3c19-a4f9-cc16f22a70bf', '2019-06-21 19:31:51', '2019-06-21 20:47:55', 1, 'choice', 0),
(20, 3, '2af6b2f5-9c45-3495-940b-b94664981b3f', '2019-06-20 03:25:13', '2019-06-21 20:47:55', 1, 'choice', 0),
(21, 3, '77565c18-5d7c-3689-b588-d869e7a780b9', '2019-06-18 18:15:43', '2019-06-21 20:48:01', 1, 'choice', 1),
(22, 3, '0a86bd2a-f39b-304e-9a1d-1df98ae51b5a', '2019-06-16 19:57:41', '2019-06-21 20:47:55', 1, 'choice', 0),
(23, 3, '9eecb9b2-7c08-3fbd-894f-685147062cf7', '2019-06-21 20:06:08', '2019-06-21 20:47:55', 1, 'choice', 0),
(24, 3, '371714cf-e546-39af-96f4-1ff2f53cf7e2', '2019-06-20 06:23:18', '2019-06-21 20:47:55', 1, 'choice', 0),
(25, 3, 'd3bb2c6a-db02-3b40-9721-cf3af92d65fe', '2019-06-17 01:17:56', '2019-06-21 20:47:55', 0, 'choice', 0),
(26, 3, '87fa3ede-6769-30dc-b807-828977916204', '2019-06-18 00:11:25', '2019-06-21 20:47:55', 1, 'choice', 0),
(27, 3, '8cdc99ad-8801-37ff-b137-4af9e006d731', '2019-06-18 23:26:14', '2019-06-21 20:47:55', 1, 'choice', 0),
(28, 3, 'a545a0bb-bfdd-3eca-aae0-4eb48f9eb772', '2019-06-21 12:06:04', '2019-06-21 20:47:55', 1, 'choice', 0),
(29, 3, 'b726df34-5ffd-33f9-a0d4-b422ab855089', '2019-06-17 04:51:20', '2019-06-21 20:47:55', 1, 'choice', 0),
(30, 3, 'f383d495-9eb7-3d28-8f51-22f3b39ea81f', '2019-06-18 19:20:01', '2019-06-21 20:47:55', 0, 'choice', 0),
(31, 4, '3549d8d8-406f-3688-ad75-d1c53f3d8999', '2019-06-20 23:04:45', '2019-06-21 20:47:56', 1, 'match', 0),
(32, 4, 'b70e7bdb-59b7-33db-a53e-0ad14df239bf', '2019-06-19 01:16:15', '2019-06-21 20:47:56', 1, 'match', 0),
(33, 4, 'b8277950-b5c1-3ead-a0ba-2165ee520f18', '2019-06-17 18:43:01', '2019-06-21 20:47:56', 1, 'match', 0),
(34, 4, '065beed8-fd4f-3dc3-b3fd-3d8ab238f6b7', '2019-06-15 23:59:27', '2019-06-21 20:47:56', 1, 'match', 0),
(35, 4, '1485879f-277c-358f-8001-0cc5802c2022', '2019-06-19 20:12:39', '2019-06-21 20:47:56', 1, 'match', 0),
(36, 4, '462acf83-c96a-3d40-aab3-1f5d26c9e6c7', '2019-06-18 16:35:04', '2019-06-21 20:47:56', 1, 'match', 0),
(37, 4, 'f672687e-92d5-3637-8dcb-a554e811af1d', '2019-06-21 01:19:21', '2019-06-21 20:48:02', 1, 'match', 3),
(38, 4, 'a997746f-2fc9-3b22-8cce-71ad80937e12', '2019-06-17 19:10:14', '2019-06-21 20:47:56', 1, 'match', 0),
(39, 4, '10821f4e-2226-34a1-88f7-d291ae7bfcc4', '2019-06-18 00:47:02', '2019-06-21 20:47:56', 0, 'match', 0),
(40, 4, '1ad980b4-adcd-3988-bc0b-0e66818191eb', '2019-06-19 13:48:21', '2019-06-21 20:48:02', 1, 'match', 3),
(41, 4, '3a28413f-bd97-3cff-8d63-9f5b63fd23d8', '2019-06-17 21:24:18', '2019-06-21 20:47:57', 1, 'match', 0),
(42, 4, 'df3dab06-c287-3fbc-9175-942fd8272c7c', '2019-06-17 16:22:48', '2019-06-21 20:48:02', 0, 'match', 2),
(43, 4, 'f661adc9-4384-3c03-8092-8d1f92918274', '2019-06-20 00:40:11', '2019-06-21 20:47:57', 1, 'match', 0),
(44, 4, '551bc608-1911-3d9d-923d-c908ec5c5894', '2019-06-15 06:32:36', '2019-06-21 20:47:57', 1, 'match', 0),
(45, 4, '5ba4f7de-4e62-3554-a909-028183d6d444', '2019-06-20 09:08:53', '2019-06-21 20:47:57', 1, 'match', 0),
(46, 7, '4dc1f1c0-bdce-3c03-a569-c7113dab0097', '2019-06-20 06:35:33', '2019-06-21 20:47:57', 1, 'match', 0),
(47, 6, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472', '2019-06-18 18:48:19', '2019-06-21 20:47:58', 1, 'match', 0),
(48, 6, 'fc4353a0-e399-3501-902b-0fcf1db3c280', '2019-06-15 09:34:34', '2019-06-21 20:47:58', 1, 'match', 0),
(49, 6, '648c848c-9f26-3575-8b1d-a5713d05243a', '2019-06-19 12:50:00', '2019-06-21 20:48:02', 1, 'match', 4),
(50, 7, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4', '2019-06-15 00:31:30', '2019-06-21 20:47:58', 1, 'match', 0),
(51, 6, '8691c9b2-5b97-30e8-b785-eba2550b3add', '2019-06-16 04:14:21', '2019-06-21 20:48:03', 1, 'match', 3),
(52, 6, 'e5b66509-2034-3a4a-8389-765b00260a2f', '2019-06-21 20:05:55', '2019-06-21 20:47:58', 1, 'match', 0),
(53, 7, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d', '2019-06-21 03:23:28', '2019-06-21 20:47:58', 1, 'match', 0),
(54, 7, '83dfbc83-b91c-3c30-8d8d-3406370f1e63', '2019-06-17 21:17:41', '2019-06-21 20:48:02', 1, 'match', 2),
(55, 6, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c', '2019-06-16 18:38:03', '2019-06-21 20:48:03', 1, 'match', 3),
(56, 7, '5a9c0476-9be6-3025-899d-ce8dec3700ef', '2019-06-18 12:42:38', '2019-06-21 20:48:00', 1, 'match', 0),
(57, 7, '4d874db4-9b9f-3167-8568-88cec184e3fc', '2019-06-18 02:48:28', '2019-06-21 20:48:00', 1, 'match', 0),
(58, 7, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac', '2019-06-19 15:38:54', '2019-06-21 20:48:00', 1, 'match', 0),
(59, 7, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b', '2019-06-21 07:23:52', '2019-06-21 20:48:00', 1, 'match', 0),
(60, 7, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8', '2019-06-21 16:59:38', '2019-06-21 20:48:00', 1, 'match', 0),
(61, NULL, '0000000', '2019-06-23 12:22:50', '2019-06-23 12:26:47', 1, 'choice', 0),
(62, NULL, 'P076537', '2019-06-23 22:02:02', '2019-06-23 22:02:09', 1, 'choice', 0),
(63, NULL, 'P8766565', '2019-06-23 22:04:25', '2019-06-23 22:04:32', 1, 'choice', 0),
(64, NULL, 'P9875775', '2019-06-23 22:12:43', '2019-06-23 22:12:51', 1, 'choice', 0);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_association`
--

DROP TABLE IF EXISTS `sylius_product_association`;
CREATE TABLE IF NOT EXISTS `sylius_product_association` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `association_type_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_association_idx` (`product_id`,`association_type_id`),
  KEY `IDX_48E9CDABB1E1C39` (`association_type_id`),
  KEY `IDX_48E9CDAB4584665A` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_association`
--

INSERT INTO `sylius_product_association` (`id`, `association_type_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 1, 26, '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(2, 1, 20, '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(3, 1, 33, '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(4, 1, 4, '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(5, 1, 25, '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(6, 1, 57, '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(7, 1, 32, '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(8, 1, 54, '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(9, 1, 45, '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(10, 1, 21, '2019-06-21 20:48:03', '2019-06-21 20:48:03');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_association_product`
--

DROP TABLE IF EXISTS `sylius_product_association_product`;
CREATE TABLE IF NOT EXISTS `sylius_product_association_product` (
  `association_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`association_id`,`product_id`),
  KEY `IDX_A427B983EFB9C8A5` (`association_id`),
  KEY `IDX_A427B9834584665A` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_association_product`
--

INSERT INTO `sylius_product_association_product` (`association_id`, `product_id`) VALUES
(1, 21),
(1, 25),
(1, 26),
(2, 16),
(2, 20),
(2, 30),
(3, 34),
(3, 37),
(3, 45),
(4, 11),
(4, 12),
(4, 13),
(5, 19),
(5, 26),
(5, 29),
(6, 56),
(6, 59),
(6, 60),
(7, 32),
(7, 33),
(7, 41),
(8, 50),
(8, 53),
(8, 54),
(9, 39),
(9, 44),
(9, 45),
(10, 16),
(10, 24),
(10, 28);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_association_type`
--

DROP TABLE IF EXISTS `sylius_product_association_type`;
CREATE TABLE IF NOT EXISTS `sylius_product_association_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CCB8914C77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_association_type`
--

INSERT INTO `sylius_product_association_type` (`id`, `code`, `created_at`, `updated_at`) VALUES
(1, 'similar_products', '2019-06-21 20:48:03', '2019-06-21 20:48:03');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_association_type_translation`
--

DROP TABLE IF EXISTS `sylius_product_association_type_translation`;
CREATE TABLE IF NOT EXISTS `sylius_product_association_type_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_product_association_type_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_4F618E52C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_association_type_translation`
--

INSERT INTO `sylius_product_association_type_translation` (`id`, `translatable_id`, `name`, `locale`) VALUES
(1, 1, 'Similar products', 'en_US');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_attribute`
--

DROP TABLE IF EXISTS `sylius_product_attribute`;
CREATE TABLE IF NOT EXISTS `sylius_product_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `storage_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configuration` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_BFAF484A77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_attribute`
--

INSERT INTO `sylius_product_attribute` (`id`, `code`, `type`, `storage_type`, `configuration`, `created_at`, `updated_at`, `position`) VALUES
(1, 'mug_material', 'select', 'json', 'a:2:{s:8:\"multiple\";b:0;s:7:\"choices\";a:4:{s:36:\"5f7d2c84-89df-3069-b017-d7f337b60fb7\";a:1:{s:5:\"en_US\";s:19:\"Invisible porcelain\";}s:36:\"cd833595-bb37-3d1a-84c7-7a6371d13a30\";a:1:{s:5:\"en_US\";s:11:\"Banana skin\";}s:36:\"6d331c1d-8214-3880-90f7-14b5a0875947\";a:1:{s:5:\"en_US\";s:9:\"Porcelain\";}s:36:\"f6130e87-1304-30ba-9872-591132312032\";a:1:{s:5:\"en_US\";s:9:\"Centipede\";}}}', '2019-06-21 20:47:51', '2019-06-21 20:47:51', 0),
(2, 'sticker_paper', 'text', 'text', 'a:0:{}', '2019-06-21 20:47:54', '2019-06-21 20:47:54', 1),
(3, 'sticker_resolution', 'text', 'text', 'a:0:{}', '2019-06-21 20:47:54', '2019-06-21 20:47:54', 2),
(4, 'book_author', 'text', 'text', 'a:0:{}', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 3),
(5, 'book_isbn', 'text', 'text', 'a:0:{}', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 4),
(6, 'book_pages', 'integer', 'integer', 'a:0:{}', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 5),
(7, 'book_genre', 'select', 'json', 'a:2:{s:8:\"multiple\";b:1;s:7:\"choices\";a:4:{s:36:\"5b2e3575-c1df-3314-bd7a-b4f49cf409fe\";a:1:{s:5:\"en_US\";s:15:\"Science Fiction\";}s:36:\"7811693d-d054-32df-a2f0-f79185abc72a\";a:1:{s:5:\"en_US\";s:7:\"Romance\";}s:36:\"5a141e3e-0d18-36d9-b7aa-416357e01c08\";a:1:{s:5:\"en_US\";s:8:\"Thriller\";}s:36:\"f996471a-60f3-341f-acd8-578432488510\";a:1:{s:5:\"en_US\";s:6:\"Sports\";}}}', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 6),
(8, 't_shirt_brand', 'text', 'text', 'a:0:{}', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 7),
(9, 't_shirt_collection', 'text', 'text', 'a:0:{}', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 8),
(10, 't_shirt_material', 'text', 'text', 'a:0:{}', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 9);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_attribute_translation`
--

DROP TABLE IF EXISTS `sylius_product_attribute_translation`;
CREATE TABLE IF NOT EXISTS `sylius_product_attribute_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_product_attribute_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_93850EBA2C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_attribute_translation`
--

INSERT INTO `sylius_product_attribute_translation` (`id`, `translatable_id`, `name`, `locale`) VALUES
(1, 1, 'Mug material', 'en_US'),
(2, 2, 'Sticker paper', 'en_US'),
(3, 3, 'Sticker resolution', 'en_US'),
(4, 4, 'Book author', 'en_US'),
(5, 5, 'Book ISBN', 'en_US'),
(6, 6, 'Book pages', 'en_US'),
(7, 7, 'Book genre', 'en_US'),
(8, 8, 'T-Shirt brand', 'en_US'),
(9, 9, 'T-Shirt collection', 'en_US'),
(10, 10, 'T-Shirt material', 'en_US');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_attribute_value`
--

DROP TABLE IF EXISTS `sylius_product_attribute_value`;
CREATE TABLE IF NOT EXISTS `sylius_product_attribute_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `locale_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_value` longtext COLLATE utf8_unicode_ci,
  `boolean_value` tinyint(1) DEFAULT NULL,
  `integer_value` int(11) DEFAULT NULL,
  `float_value` double DEFAULT NULL,
  `datetime_value` datetime DEFAULT NULL,
  `date_value` date DEFAULT NULL,
  `json_value` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)',
  PRIMARY KEY (`id`),
  KEY `IDX_8A053E544584665A` (`product_id`),
  KEY `IDX_8A053E54B6E62EFA` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_attribute_value`
--

INSERT INTO `sylius_product_attribute_value` (`id`, `product_id`, `attribute_id`, `locale_code`, `text_value`, `boolean_value`, `integer_value`, `float_value`, `datetime_value`, `date_value`, `json_value`) VALUES
(2, 2, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"cd833595-bb37-3d1a-84c7-7a6371d13a30\"]'),
(3, 3, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"f6130e87-1304-30ba-9872-591132312032\"]'),
(4, 4, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"6d331c1d-8214-3880-90f7-14b5a0875947\"]'),
(5, 5, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"6d331c1d-8214-3880-90f7-14b5a0875947\"]'),
(6, 6, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"cd833595-bb37-3d1a-84c7-7a6371d13a30\"]'),
(7, 7, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"f6130e87-1304-30ba-9872-591132312032\"]'),
(8, 8, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"cd833595-bb37-3d1a-84c7-7a6371d13a30\"]'),
(9, 9, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"6d331c1d-8214-3880-90f7-14b5a0875947\"]'),
(10, 10, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"5f7d2c84-89df-3069-b017-d7f337b60fb7\"]'),
(11, 11, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"6d331c1d-8214-3880-90f7-14b5a0875947\"]'),
(12, 12, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"cd833595-bb37-3d1a-84c7-7a6371d13a30\"]'),
(13, 13, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"cd833595-bb37-3d1a-84c7-7a6371d13a30\"]'),
(14, 14, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"5f7d2c84-89df-3069-b017-d7f337b60fb7\"]'),
(15, 15, 1, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"5f7d2c84-89df-3069-b017-d7f337b60fb7\"]'),
(16, 16, 2, 'en_US', 'Paper from tree Tanajno', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 16, 3, 'en_US', 'JKM XD', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 17, 2, 'en_US', 'Paper from tree Lemon-San', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 17, 3, 'en_US', 'FULL HD', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 18, 2, 'en_US', 'Paper from tree Lemon-San', NULL, NULL, NULL, NULL, NULL, NULL),
(21, 18, 3, 'en_US', 'JKM XD', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 19, 2, 'en_US', 'Paper from tree Me-Gusta', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 19, 3, 'en_US', '476DPI', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 20, 2, 'en_US', 'Paper from tree Tanajno', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 20, 3, 'en_US', 'JKM XD', NULL, NULL, NULL, NULL, NULL, NULL),
(26, 21, 2, 'en_US', 'Paper from tree Lemon-San', NULL, NULL, NULL, NULL, NULL, NULL),
(27, 21, 3, 'en_US', '476DPI', NULL, NULL, NULL, NULL, NULL, NULL),
(28, 22, 2, 'en_US', 'Paper from tree Tanajno', NULL, NULL, NULL, NULL, NULL, NULL),
(29, 22, 3, 'en_US', '200DPI', NULL, NULL, NULL, NULL, NULL, NULL),
(30, 23, 2, 'en_US', 'Paper from tree Tanajno', NULL, NULL, NULL, NULL, NULL, NULL),
(31, 23, 3, 'en_US', '476DPI', NULL, NULL, NULL, NULL, NULL, NULL),
(32, 24, 2, 'en_US', 'Paper from tree Me-Gusta', NULL, NULL, NULL, NULL, NULL, NULL),
(33, 24, 3, 'en_US', 'FULL HD', NULL, NULL, NULL, NULL, NULL, NULL),
(34, 25, 2, 'en_US', 'Paper from tree Wung', NULL, NULL, NULL, NULL, NULL, NULL),
(35, 25, 3, 'en_US', '200DPI', NULL, NULL, NULL, NULL, NULL, NULL),
(36, 26, 2, 'en_US', 'Paper from tree Me-Gusta', NULL, NULL, NULL, NULL, NULL, NULL),
(37, 26, 3, 'en_US', 'FULL HD', NULL, NULL, NULL, NULL, NULL, NULL),
(38, 27, 2, 'en_US', 'Paper from tree Wung', NULL, NULL, NULL, NULL, NULL, NULL),
(39, 27, 3, 'en_US', 'FULL HD', NULL, NULL, NULL, NULL, NULL, NULL),
(40, 28, 2, 'en_US', 'Paper from tree Me-Gusta', NULL, NULL, NULL, NULL, NULL, NULL),
(41, 28, 3, 'en_US', 'JKM XD', NULL, NULL, NULL, NULL, NULL, NULL),
(42, 29, 2, 'en_US', 'Paper from tree Wung', NULL, NULL, NULL, NULL, NULL, NULL),
(43, 29, 3, 'en_US', '476DPI', NULL, NULL, NULL, NULL, NULL, NULL),
(44, 30, 2, 'en_US', 'Paper from tree Me-Gusta', NULL, NULL, NULL, NULL, NULL, NULL),
(45, 30, 3, 'en_US', '200DPI', NULL, NULL, NULL, NULL, NULL, NULL),
(46, 31, 4, 'en_US', 'Eladio Smitham', NULL, NULL, NULL, NULL, NULL, NULL),
(47, 31, 5, 'en_US', '9790316475357', NULL, NULL, NULL, NULL, NULL, NULL),
(48, 31, 6, 'en_US', NULL, NULL, 828, NULL, NULL, NULL, NULL),
(49, 31, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"f996471a-60f3-341f-acd8-578432488510\"]'),
(50, 32, 4, 'en_US', 'Mrs. Maggie Mitchell III', NULL, NULL, NULL, NULL, NULL, NULL),
(51, 32, 5, 'en_US', '9783668777163', NULL, NULL, NULL, NULL, NULL, NULL),
(52, 32, 6, 'en_US', NULL, NULL, 950, NULL, NULL, NULL, NULL),
(53, 32, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"f996471a-60f3-341f-acd8-578432488510\",\"5b2e3575-c1df-3314-bd7a-b4f49cf409fe\",\"5a141e3e-0d18-36d9-b7aa-416357e01c08\"]'),
(54, 33, 4, 'en_US', 'Kyla Heidenreich', NULL, NULL, NULL, NULL, NULL, NULL),
(55, 33, 5, 'en_US', '9782389578042', NULL, NULL, NULL, NULL, NULL, NULL),
(56, 33, 6, 'en_US', NULL, NULL, 239, NULL, NULL, NULL, NULL),
(57, 33, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"f996471a-60f3-341f-acd8-578432488510\",\"5a141e3e-0d18-36d9-b7aa-416357e01c08\"]'),
(58, 34, 4, 'en_US', 'Harrison Fritsch', NULL, NULL, NULL, NULL, NULL, NULL),
(59, 34, 5, 'en_US', '9787703221604', NULL, NULL, NULL, NULL, NULL, NULL),
(60, 34, 6, 'en_US', NULL, NULL, 246, NULL, NULL, NULL, NULL),
(61, 34, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"5b2e3575-c1df-3314-bd7a-b4f49cf409fe\",\"5a141e3e-0d18-36d9-b7aa-416357e01c08\"]'),
(62, 35, 4, 'en_US', 'Ms. Damaris Lindgren Sr.', NULL, NULL, NULL, NULL, NULL, NULL),
(63, 35, 5, 'en_US', '9794601653543', NULL, NULL, NULL, NULL, NULL, NULL),
(64, 35, 6, 'en_US', NULL, NULL, 385, NULL, NULL, NULL, NULL),
(65, 35, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"5a141e3e-0d18-36d9-b7aa-416357e01c08\",\"7811693d-d054-32df-a2f0-f79185abc72a\",\"f996471a-60f3-341f-acd8-578432488510\",\"5b2e3575-c1df-3314-bd7a-b4f49cf409fe\"]'),
(66, 36, 4, 'en_US', 'Dorothea Hartmann', NULL, NULL, NULL, NULL, NULL, NULL),
(67, 36, 5, 'en_US', '9790414771009', NULL, NULL, NULL, NULL, NULL, NULL),
(68, 36, 6, 'en_US', NULL, NULL, 321, NULL, NULL, NULL, NULL),
(69, 36, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"7811693d-d054-32df-a2f0-f79185abc72a\"]'),
(70, 37, 4, 'en_US', 'Jalen Russel', NULL, NULL, NULL, NULL, NULL, NULL),
(71, 37, 5, 'en_US', '9795504243831', NULL, NULL, NULL, NULL, NULL, NULL),
(72, 37, 6, 'en_US', NULL, NULL, 757, NULL, NULL, NULL, NULL),
(73, 37, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"5b2e3575-c1df-3314-bd7a-b4f49cf409fe\"]'),
(74, 38, 4, 'en_US', 'Nash Stanton', NULL, NULL, NULL, NULL, NULL, NULL),
(75, 38, 5, 'en_US', '9792521459146', NULL, NULL, NULL, NULL, NULL, NULL),
(76, 38, 6, 'en_US', NULL, NULL, 901, NULL, NULL, NULL, NULL),
(77, 38, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"7811693d-d054-32df-a2f0-f79185abc72a\"]'),
(78, 39, 4, 'en_US', 'Mrs. Joelle Zulauf', NULL, NULL, NULL, NULL, NULL, NULL),
(79, 39, 5, 'en_US', '9793182717422', NULL, NULL, NULL, NULL, NULL, NULL),
(80, 39, 6, 'en_US', NULL, NULL, 705, NULL, NULL, NULL, NULL),
(81, 39, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"5a141e3e-0d18-36d9-b7aa-416357e01c08\"]'),
(82, 40, 4, 'en_US', 'Marlin Marquardt I', NULL, NULL, NULL, NULL, NULL, NULL),
(83, 40, 5, 'en_US', '9790753423492', NULL, NULL, NULL, NULL, NULL, NULL),
(84, 40, 6, 'en_US', NULL, NULL, 46, NULL, NULL, NULL, NULL),
(85, 40, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"f996471a-60f3-341f-acd8-578432488510\",\"5a141e3e-0d18-36d9-b7aa-416357e01c08\",\"7811693d-d054-32df-a2f0-f79185abc72a\",\"5b2e3575-c1df-3314-bd7a-b4f49cf409fe\"]'),
(86, 41, 4, 'en_US', 'Pietro Gaylord', NULL, NULL, NULL, NULL, NULL, NULL),
(87, 41, 5, 'en_US', '9780351091773', NULL, NULL, NULL, NULL, NULL, NULL),
(88, 41, 6, 'en_US', NULL, NULL, 258, NULL, NULL, NULL, NULL),
(89, 41, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"f996471a-60f3-341f-acd8-578432488510\",\"5a141e3e-0d18-36d9-b7aa-416357e01c08\",\"5b2e3575-c1df-3314-bd7a-b4f49cf409fe\",\"7811693d-d054-32df-a2f0-f79185abc72a\"]'),
(90, 42, 4, 'en_US', 'Manuela Metz', NULL, NULL, NULL, NULL, NULL, NULL),
(91, 42, 5, 'en_US', '9781922850270', NULL, NULL, NULL, NULL, NULL, NULL),
(92, 42, 6, 'en_US', NULL, NULL, 639, NULL, NULL, NULL, NULL),
(93, 42, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"f996471a-60f3-341f-acd8-578432488510\",\"5a141e3e-0d18-36d9-b7aa-416357e01c08\"]'),
(94, 43, 4, 'en_US', 'Noble Kshlerin I', NULL, NULL, NULL, NULL, NULL, NULL),
(95, 43, 5, 'en_US', '9785662239180', NULL, NULL, NULL, NULL, NULL, NULL),
(96, 43, 6, 'en_US', NULL, NULL, 42, NULL, NULL, NULL, NULL),
(97, 43, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"f996471a-60f3-341f-acd8-578432488510\",\"5a141e3e-0d18-36d9-b7aa-416357e01c08\",\"5b2e3575-c1df-3314-bd7a-b4f49cf409fe\",\"7811693d-d054-32df-a2f0-f79185abc72a\"]'),
(98, 44, 4, 'en_US', 'Ms. Amalia Ratke', NULL, NULL, NULL, NULL, NULL, NULL),
(99, 44, 5, 'en_US', '9780576832649', NULL, NULL, NULL, NULL, NULL, NULL),
(100, 44, 6, 'en_US', NULL, NULL, 735, NULL, NULL, NULL, NULL),
(101, 44, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"7811693d-d054-32df-a2f0-f79185abc72a\",\"f996471a-60f3-341f-acd8-578432488510\",\"5b2e3575-c1df-3314-bd7a-b4f49cf409fe\"]'),
(102, 45, 4, 'en_US', 'Prof. Quincy Gaylord MD', NULL, NULL, NULL, NULL, NULL, NULL),
(103, 45, 5, 'en_US', '9796002150881', NULL, NULL, NULL, NULL, NULL, NULL),
(104, 45, 6, 'en_US', NULL, NULL, 393, NULL, NULL, NULL, NULL),
(105, 45, 7, 'en_US', NULL, NULL, NULL, NULL, NULL, NULL, '[\"7811693d-d054-32df-a2f0-f79185abc72a\"]'),
(106, 46, 8, 'en_US', 'Adidas', NULL, NULL, NULL, NULL, NULL, NULL),
(107, 46, 9, 'en_US', 'Sylius Autumn 1997', NULL, NULL, NULL, NULL, NULL, NULL),
(108, 46, 10, 'en_US', 'Potato 100%', NULL, NULL, NULL, NULL, NULL, NULL),
(109, 47, 8, 'en_US', 'Nike', NULL, NULL, NULL, NULL, NULL, NULL),
(110, 47, 9, 'en_US', 'Sylius Autumn 2003', NULL, NULL, NULL, NULL, NULL, NULL),
(111, 47, 10, 'en_US', 'Potato 100%', NULL, NULL, NULL, NULL, NULL, NULL),
(112, 48, 8, 'en_US', 'JKM-476 Streetwear', NULL, NULL, NULL, NULL, NULL, NULL),
(113, 48, 9, 'en_US', 'Sylius Winter 2004', NULL, NULL, NULL, NULL, NULL, NULL),
(114, 48, 10, 'en_US', 'Potato 100%', NULL, NULL, NULL, NULL, NULL, NULL),
(115, 49, 8, 'en_US', 'Nike', NULL, NULL, NULL, NULL, NULL, NULL),
(116, 49, 9, 'en_US', 'Sylius Autumn 2008', NULL, NULL, NULL, NULL, NULL, NULL),
(117, 49, 10, 'en_US', 'Wool', NULL, NULL, NULL, NULL, NULL, NULL),
(118, 50, 8, 'en_US', 'JKM-476 Streetwear', NULL, NULL, NULL, NULL, NULL, NULL),
(119, 50, 9, 'en_US', 'Sylius Winter 2012', NULL, NULL, NULL, NULL, NULL, NULL),
(120, 50, 10, 'en_US', 'Centipede 10% / Wool 90%', NULL, NULL, NULL, NULL, NULL, NULL),
(121, 51, 8, 'en_US', 'JKM-476 Streetwear', NULL, NULL, NULL, NULL, NULL, NULL),
(122, 51, 9, 'en_US', 'Sylius Winter 2011', NULL, NULL, NULL, NULL, NULL, NULL),
(123, 51, 10, 'en_US', 'Potato 100%', NULL, NULL, NULL, NULL, NULL, NULL),
(124, 52, 8, 'en_US', 'JKM-476 Streetwear', NULL, NULL, NULL, NULL, NULL, NULL),
(125, 52, 9, 'en_US', 'Sylius Winter 2004', NULL, NULL, NULL, NULL, NULL, NULL),
(126, 52, 10, 'en_US', 'Wool', NULL, NULL, NULL, NULL, NULL, NULL),
(127, 53, 8, 'en_US', 'Potato', NULL, NULL, NULL, NULL, NULL, NULL),
(128, 53, 9, 'en_US', 'Sylius Summer 1998', NULL, NULL, NULL, NULL, NULL, NULL),
(129, 53, 10, 'en_US', 'Centipede 10% / Wool 90%', NULL, NULL, NULL, NULL, NULL, NULL),
(130, 54, 8, 'en_US', 'Centipede Wear', NULL, NULL, NULL, NULL, NULL, NULL),
(131, 54, 9, 'en_US', 'Sylius Spring 2011', NULL, NULL, NULL, NULL, NULL, NULL),
(132, 54, 10, 'en_US', 'Potato 100%', NULL, NULL, NULL, NULL, NULL, NULL),
(133, 55, 8, 'en_US', 'JKM-476 Streetwear', NULL, NULL, NULL, NULL, NULL, NULL),
(134, 55, 9, 'en_US', 'Sylius Summer 2012', NULL, NULL, NULL, NULL, NULL, NULL),
(135, 55, 10, 'en_US', 'Wool', NULL, NULL, NULL, NULL, NULL, NULL),
(136, 56, 8, 'en_US', 'Centipede Wear', NULL, NULL, NULL, NULL, NULL, NULL),
(137, 56, 9, 'en_US', 'Sylius Spring 2000', NULL, NULL, NULL, NULL, NULL, NULL),
(138, 56, 10, 'en_US', 'Wool', NULL, NULL, NULL, NULL, NULL, NULL),
(139, 57, 8, 'en_US', 'Adidas', NULL, NULL, NULL, NULL, NULL, NULL),
(140, 57, 9, 'en_US', 'Sylius Spring 1998', NULL, NULL, NULL, NULL, NULL, NULL),
(141, 57, 10, 'en_US', 'Centipede', NULL, NULL, NULL, NULL, NULL, NULL),
(142, 58, 8, 'en_US', 'JKM-476 Streetwear', NULL, NULL, NULL, NULL, NULL, NULL),
(143, 58, 9, 'en_US', 'Sylius Winter 2010', NULL, NULL, NULL, NULL, NULL, NULL),
(144, 58, 10, 'en_US', 'Potato 100%', NULL, NULL, NULL, NULL, NULL, NULL),
(145, 59, 8, 'en_US', 'Centipede Wear', NULL, NULL, NULL, NULL, NULL, NULL),
(146, 59, 9, 'en_US', 'Sylius Summer 1996', NULL, NULL, NULL, NULL, NULL, NULL),
(147, 59, 10, 'en_US', 'Centipede 10% / Wool 90%', NULL, NULL, NULL, NULL, NULL, NULL),
(148, 60, 8, 'en_US', 'Adidas', NULL, NULL, NULL, NULL, NULL, NULL),
(149, 60, 9, 'en_US', 'Sylius Spring 2003', NULL, NULL, NULL, NULL, NULL, NULL),
(150, 60, 10, 'en_US', 'Wool', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_channels`
--

DROP TABLE IF EXISTS `sylius_product_channels`;
CREATE TABLE IF NOT EXISTS `sylius_product_channels` (
  `product_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`channel_id`),
  KEY `IDX_F9EF269B4584665A` (`product_id`),
  KEY `IDX_F9EF269B72F5A1AA` (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_channels`
--

INSERT INTO `sylius_product_channels` (`product_id`, `channel_id`) VALUES
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_image`
--

DROP TABLE IF EXISTS `sylius_product_image`;
CREATE TABLE IF NOT EXISTS `sylius_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_88C64B2D7E3C61F9` (`owner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_image`
--

INSERT INTO `sylius_product_image` (`id`, `owner_id`, `type`, `path`) VALUES
(3, 2, 'main', '46/9d/22a2cb68c04479050c00aa1860c0.jpeg'),
(4, 2, 'thumbnail', '78/5c/ef01e8fc5a4b840ce332cdf702a3.jpeg'),
(5, 3, 'main', '7a/8a/16deaa6bfdcc4a339a5e3a5c35d6.jpeg'),
(6, 3, 'thumbnail', '73/37/d01c473064620b1865a2b6c5ac46.jpeg'),
(7, 4, 'main', '65/bd/32f38d2715644925b9532e63f133.jpeg'),
(8, 4, 'thumbnail', '9e/f6/3af9f907801744b33e6d5d33e61d.jpeg'),
(9, 5, 'main', 'e1/05/950acb8617eb89edc0c1fa67b356.jpeg'),
(10, 5, 'thumbnail', 'ff/50/466a727e13041be20e9a09de6da9.jpeg'),
(11, 6, 'main', 'c8/77/56715827baa03d3d43d43abb0de5.jpeg'),
(12, 6, 'thumbnail', '2e/93/40117cc234aeca500bcef025d893.jpeg'),
(13, 7, 'main', '2d/70/609eb141ea4af08b8245879d9411.jpeg'),
(14, 7, 'thumbnail', '81/79/9971d6b6e8a6ea4c74bb174cca04.jpeg'),
(15, 8, 'main', '1d/6e/e5034c9866e7c8bc59c90d02fb2f.jpeg'),
(16, 8, 'thumbnail', '4a/f6/59261dd6129a573c78242f9725e1.jpeg'),
(17, 9, 'main', 'e4/37/7415a2cdf6651f08b84b208c0e61.jpeg'),
(18, 9, 'thumbnail', '01/bb/e993a663be7c28b8d5b012d4bdd6.jpeg'),
(19, 10, 'main', '0e/49/f278ed206ca22a1e74bdd18afec8.jpeg'),
(20, 10, 'thumbnail', 'db/06/053cb5841d2ec34e97c2f1521635.jpeg'),
(21, 11, 'main', '3f/30/f94d7ff3cb9c34476d30b6af9c35.jpeg'),
(22, 11, 'thumbnail', '50/b0/71764ab5c0e72483b2b9f1b351da.jpeg'),
(23, 12, 'main', '43/6a/ef30e8157d7ca40f95abbf5d6e95.jpeg'),
(24, 12, 'thumbnail', '23/b8/85b212844befe2fda6e2ea71ffd5.jpeg'),
(25, 13, 'main', '80/69/59ff3203dbe7af723f0c66dd4a11.jpeg'),
(26, 13, 'thumbnail', '39/15/76d6a5f5a8a2b0a10b66d0ce338a.jpeg'),
(27, 14, 'main', '7a/e1/1cf5aa9e64baf3003ded39e8ca88.jpeg'),
(28, 14, 'thumbnail', '69/29/fc701240a41a35a888d4aa8cb5c5.jpeg'),
(29, 15, 'main', 'b2/ba/2e31f34a916a8f792962550c2909.jpeg'),
(30, 15, 'thumbnail', 'f3/23/5e94ff6c26b267ea651910bd71b8.jpeg'),
(31, 16, 'main', 'cf/06/8327606fe6b5b872e5aeffeebb96.jpeg'),
(32, 16, 'thumbnail', '62/aa/9951fd663530b5c649ebb59dda74.jpeg'),
(33, 17, 'main', '5c/6c/d86bac1429777905de9dddc458ec.jpeg'),
(34, 17, 'thumbnail', '59/bc/2f4ed62755bb71d543e699fae93e.jpeg'),
(35, 18, 'main', '2a/31/27f76be1bc747b7892ec783da629.jpeg'),
(36, 18, 'thumbnail', '6f/3d/c51618fb49a5a705b917a1b995b8.jpeg'),
(37, 19, 'main', '82/d8/9d6a14df331e8b743de8420c53b4.jpeg'),
(38, 19, 'thumbnail', '11/81/94916c75c48c995762b6ddae9787.jpeg'),
(39, 20, 'main', '29/f4/d7ec330c5a22d69330aee06c229a.jpeg'),
(40, 20, 'thumbnail', '7c/77/e4f6ec7434c31117ac3f81cdaa14.jpeg'),
(41, 21, 'main', '69/c7/8b77bcbf3fa7b6436e104b14d6cb.jpeg'),
(42, 21, 'thumbnail', '08/91/0b49d732616f93380f3d2b847eab.jpeg'),
(43, 22, 'main', '4a/57/294447b9e02a98c317efc617b3ab.jpeg'),
(44, 22, 'thumbnail', '40/e6/751d814650cd4016f57ce06c54de.jpeg'),
(45, 23, 'main', '88/38/61abff045fdb44a63ff5afee62db.jpeg'),
(46, 23, 'thumbnail', '50/86/244303de003756921d4b6358592b.jpeg'),
(47, 24, 'main', '0f/fa/d4a565e2e095481003e5b4224e5e.jpeg'),
(48, 24, 'thumbnail', '22/96/bb2da1d020124f150025fc61fca2.jpeg'),
(49, 25, 'main', '7e/5e/c06350044ab72e2244b5ef61b90f.jpeg'),
(50, 25, 'thumbnail', '05/b3/cdacdde12d9fdee52d16ef2c545c.jpeg'),
(51, 26, 'main', '62/e3/3572193661898354b466f49e3aea.jpeg'),
(52, 26, 'thumbnail', 'f7/df/1b3d48ec0c39409a517d4f5d5129.jpeg'),
(53, 27, 'main', '0c/6f/185c557d6ef4e3dc9df4e44188b6.jpeg'),
(54, 27, 'thumbnail', 'df/38/05568c64bd114f4c7378d0004bde.jpeg'),
(55, 28, 'main', 'de/db/d3e8421db4bb89460a68f1496188.jpeg'),
(56, 28, 'thumbnail', '41/37/66c68371def4345baeb9140a602c.jpeg'),
(57, 29, 'main', 'ff/df/23d2112247c0dd22c24e12a54846.jpeg'),
(58, 29, 'thumbnail', '22/9b/921edd280f1c12fcdec1ebb19f3b.jpeg'),
(59, 30, 'main', '4e/b6/d913aefa4559d7504910c5cecc08.jpeg'),
(60, 30, 'thumbnail', '91/d2/0b465296cf1b25c2932f67c81fb1.jpeg'),
(61, 31, 'main', '0b/78/c6c0e05bde52e6f50028e277aceb.jpeg'),
(62, 31, 'thumbnail', '90/96/4451708804382a0e02acba2502fe.jpeg'),
(63, 32, 'main', '65/70/5e8d067bf4d5ec8dbeb385417d17.jpeg'),
(64, 32, 'thumbnail', 'e4/cb/c4e6224879900a0fe6450948803b.jpeg'),
(65, 33, 'main', 'c2/e6/f6af7b183263d69d0b78ac92a11f.jpeg'),
(66, 33, 'thumbnail', '96/4d/fe3cdfe9fe94bd9981103c147425.jpeg'),
(67, 34, 'main', '9c/e5/07c434d69b11fbfe6b84d90dfa6f.jpeg'),
(68, 34, 'thumbnail', 'b4/bc/abd068d8aeb777c479306bd63fc9.jpeg'),
(69, 35, 'main', '68/0d/babb14f0b0cb92d140d4536452cc.jpeg'),
(70, 35, 'thumbnail', '56/dd/ddc4c8eafb1d53246ce1b6a01b3d.jpeg'),
(71, 36, 'main', 'ea/ae/3bc028bee5a278fc7ba16a118236.jpeg'),
(72, 36, 'thumbnail', '98/f4/672dfaa069ba3d8a0658bc01497f.jpeg'),
(73, 37, 'main', '02/d3/1c98a326137ab56e04a23c3dabd0.jpeg'),
(74, 37, 'thumbnail', 'bd/c8/ce38ae431ec53db78c4848389ba7.jpeg'),
(75, 38, 'main', '4d/c0/e63eb1c1b4ce62e35a5ca739e3d4.jpeg'),
(76, 38, 'thumbnail', 'cc/98/7438797326ce7665c93047197a33.jpeg'),
(77, 39, 'main', 'f2/80/275c91d73a4ea04db17a44414170.jpeg'),
(78, 39, 'thumbnail', 'ce/db/3cf01ccd85c42a292dfc347c364b.jpeg'),
(79, 40, 'main', '82/df/63e840981a53d33b0a8eb9e72068.jpeg'),
(80, 40, 'thumbnail', 'cb/1e/9f7fe846705f014ff65331b847e1.jpeg'),
(81, 41, 'main', '67/de/cb3dd999ed1fb51b5ba4d4c949b0.jpeg'),
(82, 41, 'thumbnail', '07/b9/9b3b42b2579f3b5fe5aa6ea5cc94.jpeg'),
(83, 42, 'main', '79/5d/4db4bc855f8aab14ed388cb5fa38.jpeg'),
(84, 42, 'thumbnail', 'dd/09/c34bb041df6ee72e32fc36ce7273.jpeg'),
(85, 43, 'main', '66/6c/edae37bc47862f3432b53f433450.jpeg'),
(86, 43, 'thumbnail', 'f0/46/e49072a93f359e88c0867d1030d2.jpeg'),
(87, 44, 'main', '97/90/e429a18bdf7e337f71d5f99fe690.jpeg'),
(88, 44, 'thumbnail', 'fa/72/70f189fb89dfc593c6eca6375576.jpeg'),
(89, 45, 'main', 'c7/6b/a71a7d6395507f09e80913624630.jpeg'),
(90, 45, 'thumbnail', 'd8/a5/7e4cfa7faed466b20e8ace197b3b.jpeg'),
(91, 46, 'main', 'be/ac/2cdb96cc8bd41625b71c0072382d.jpeg'),
(92, 46, 'thumbnail', 'db/b3/c33fa1cc3b40d165290f051fc59b.jpeg'),
(93, 47, 'main', '77/aa/f69ef43d1d0ef704148a3b8cd885.jpeg'),
(94, 47, 'thumbnail', '61/d7/4c6b9c5ed56142a7410fd8c0d3db.jpeg'),
(95, 48, 'main', '61/72/2e1eae1b9394832e6224ba53c58e.jpeg'),
(96, 48, 'thumbnail', 'b8/4a/da02607c1bc951e812cf24a57fd2.jpeg'),
(97, 49, 'main', '4a/12/6a97bec27dd2376a48270fae9cc6.jpeg'),
(98, 49, 'thumbnail', '34/0e/36bf576ba82cf88ab8d47af4c594.jpeg'),
(99, 50, 'main', 'd3/43/cd95cbc24cce6ae860039b95e577.jpeg'),
(100, 50, 'thumbnail', '45/0e/0c07042b982f8f413f96dab456da.jpeg'),
(101, 51, 'main', '0c/6c/2b4f9bd6863e516c0e0f3a8efc6c.jpeg'),
(102, 51, 'thumbnail', '19/22/4a73779e86edb6ec95821dc89afb.jpeg'),
(103, 52, 'main', 'f1/f2/a43cdd1c9620694a160e3c6769c6.jpeg'),
(104, 52, 'thumbnail', '84/01/4aec2f3feb824f6d84f9d29c4f69.jpeg'),
(105, 53, 'main', '5f/60/1e9a110274c4dcd16faf67a1c209.jpeg'),
(106, 53, 'thumbnail', '3f/49/0c5bc6e8a546dc6e77e9709f9609.jpeg'),
(107, 54, 'main', '4a/8e/65b11571e2c6a4d72b1abf5d316f.jpeg'),
(108, 54, 'thumbnail', '1b/b7/81036f0d2b220c3905845e6a5b7c.jpeg'),
(109, 55, 'main', '2d/68/10b20a30259fb04de30857f9362c.jpeg'),
(110, 55, 'thumbnail', 'f6/b6/beee2d445fa58c29c4e577e459c0.jpeg'),
(111, 56, 'main', 'f3/09/a18ed9abe40b3dfbabdfb54d428d.jpeg'),
(112, 56, 'thumbnail', '64/94/6bda85675e31150be77c23dea9a2.jpeg'),
(113, 57, 'main', 'ff/ac/72254d1e658a73a62f89d54bb92f.jpeg'),
(114, 57, 'thumbnail', '2c/48/9ceaf55a4f9fe74d37b8a29c0d03.jpeg'),
(115, 58, 'main', '5a/e1/3c29204f47b16c3c87e59a698eb6.jpeg'),
(116, 58, 'thumbnail', 'd4/ce/a0dca354c6255972c79bc990e279.jpeg'),
(117, 59, 'main', '92/a9/d5cca5af3bf27ea7c3097b86b0e1.jpeg'),
(118, 59, 'thumbnail', 'fa/64/cfb59d79f333c58ca31eb0322f09.jpeg'),
(119, 60, 'main', '51/5f/bd38797c0c0431f3a5197d548630.jpeg'),
(120, 60, 'thumbnail', '22/87/3e0d308dce43ce934b07b621b1a7.jpeg'),
(121, 61, NULL, '69/74/869215cc13a8779eea7ce25bd9ff.jpeg'),
(122, 63, NULL, 'fc/63/73cfb641bbae73b8e30819aab1b4.jpeg'),
(123, 62, NULL, 'd0/bf/0cfcded8c6f8e2e7e5de133a855b.jpeg'),
(124, 64, NULL, 'ee/8c/4bc3994039f18de39b308eec1d24.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_image_product_variants`
--

DROP TABLE IF EXISTS `sylius_product_image_product_variants`;
CREATE TABLE IF NOT EXISTS `sylius_product_image_product_variants` (
  `image_id` int(11) NOT NULL,
  `variant_id` int(11) NOT NULL,
  PRIMARY KEY (`image_id`,`variant_id`),
  KEY `IDX_8FFDAE8D3DA5256D` (`image_id`),
  KEY `IDX_8FFDAE8D3B69A9AF` (`variant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_option`
--

DROP TABLE IF EXISTS `sylius_product_option`;
CREATE TABLE IF NOT EXISTS `sylius_product_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E4C0EBEF77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_option`
--

INSERT INTO `sylius_product_option` (`id`, `code`, `position`, `created_at`, `updated_at`) VALUES
(1, 'mug_type', 0, '2019-06-21 20:47:51', '2019-06-21 20:47:51'),
(2, 'sticker_size', 1, '2019-06-21 20:47:54', '2019-06-21 20:47:54'),
(3, 't_shirt_color', 2, '2019-06-21 20:47:57', '2019-06-21 20:47:57'),
(4, 't_shirt_size', 3, '2019-06-21 20:47:57', '2019-06-21 20:47:57');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_options`
--

DROP TABLE IF EXISTS `sylius_product_options`;
CREATE TABLE IF NOT EXISTS `sylius_product_options` (
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`option_id`),
  KEY `IDX_2B5FF0094584665A` (`product_id`),
  KEY `IDX_2B5FF009A7C41D6F` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_options`
--

INSERT INTO `sylius_product_options` (`product_id`, `option_id`) VALUES
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(46, 3),
(46, 4),
(47, 3),
(47, 4),
(48, 3),
(48, 4),
(49, 3),
(49, 4),
(50, 3),
(50, 4),
(51, 3),
(51, 4),
(52, 3),
(52, 4),
(53, 3),
(53, 4),
(54, 3),
(54, 4),
(55, 3),
(55, 4),
(56, 3),
(56, 4),
(57, 3),
(57, 4),
(58, 3),
(58, 4),
(59, 3),
(59, 4),
(60, 3),
(60, 4);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_option_translation`
--

DROP TABLE IF EXISTS `sylius_product_option_translation`;
CREATE TABLE IF NOT EXISTS `sylius_product_option_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_product_option_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_CBA491AD2C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_option_translation`
--

INSERT INTO `sylius_product_option_translation` (`id`, `translatable_id`, `name`, `locale`) VALUES
(1, 1, 'Mug type', 'en_US'),
(2, 2, 'Sticker size', 'en_US'),
(3, 3, 'T-Shirt color', 'en_US'),
(4, 4, 'T-Shirt size', 'en_US');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_option_value`
--

DROP TABLE IF EXISTS `sylius_product_option_value`;
CREATE TABLE IF NOT EXISTS `sylius_product_option_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F7FF7D4B77153098` (`code`),
  KEY `IDX_F7FF7D4BA7C41D6F` (`option_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_option_value`
--

INSERT INTO `sylius_product_option_value` (`id`, `option_id`, `code`) VALUES
(1, 1, 'mug_type_medium'),
(2, 1, 'mug_type_double'),
(3, 1, 'mug_type_monster'),
(4, 2, 'sticker_size_3'),
(5, 2, 'sticker_size_5'),
(6, 2, 'sticker_size_7'),
(7, 3, 't_shirt_color_red'),
(8, 3, 't_shirt_color_black'),
(9, 3, 't_shirt_color_white'),
(10, 4, 't_shirt_size_s'),
(11, 4, 't_shirt_size_m'),
(12, 4, 't_shirt_size_l'),
(13, 4, 't_shirt_size_xl'),
(14, 4, 't_shirt_size_xxl');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_option_value_translation`
--

DROP TABLE IF EXISTS `sylius_product_option_value_translation`;
CREATE TABLE IF NOT EXISTS `sylius_product_option_value_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_product_option_value_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_8D4382DC2C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_option_value_translation`
--

INSERT INTO `sylius_product_option_value_translation` (`id`, `translatable_id`, `value`, `locale`) VALUES
(1, 1, 'Medium mug', 'en_US'),
(2, 2, 'Double mug', 'en_US'),
(3, 3, 'Monster mug', 'en_US'),
(4, 4, '3\"', 'en_US'),
(5, 5, '5\"', 'en_US'),
(6, 6, '7\"', 'en_US'),
(7, 7, 'Red', 'en_US'),
(8, 8, 'Black', 'en_US'),
(9, 9, 'White', 'en_US'),
(10, 10, 'S', 'en_US'),
(11, 11, 'M', 'en_US'),
(12, 12, 'L', 'en_US'),
(13, 13, 'XL', 'en_US'),
(14, 14, 'XXL', 'en_US');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_review`
--

DROP TABLE IF EXISTS `sylius_product_review`;
CREATE TABLE IF NOT EXISTS `sylius_product_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rating` int(11) NOT NULL,
  `comment` longtext COLLATE utf8_unicode_ci,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C7056A994584665A` (`product_id`),
  KEY `IDX_C7056A99F675F31B` (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_review`
--

INSERT INTO `sylius_product_review` (`id`, `product_id`, `author_id`, `title`, `rating`, `comment`, `status`, `created_at`, `updated_at`) VALUES
(1, 21, 15, 'placeat magnam deserunt', 1, 'Voluptatem aut quia similique quas nisi consequatur iste. Consectetur possimus omnis sint itaque ab atque animi. Amet enim facere alias.', 'accepted', '2019-06-21 20:48:01', '2019-06-21 20:48:01'),
(2, 57, 9, 'assumenda unde quo', 2, 'Voluptas neque itaque vel minima et iusto quia. Consequuntur deserunt praesentium quia amet. Tempore modi nam cupiditate repellat consequuntur dicta error.', 'rejected', '2019-06-21 20:48:01', '2019-06-21 20:48:01'),
(3, 38, 2, 'dignissimos dignissimos non', 5, 'Sed quod incidunt in facilis. Sed in unde corrupti est porro porro voluptatem officia. Doloremque maiores quae odio aut corrupti sint fugiat.', 'rejected', '2019-06-21 20:48:01', '2019-06-21 20:48:01'),
(4, 2, 16, 'repudiandae neque doloremque', 2, 'Eum nobis necessitatibus illo veritatis distinctio. Tempore ad ut at. Quam a qui vero consectetur.', 'new', '2019-06-21 20:48:01', '2019-06-21 20:48:01'),
(6, 55, 5, 'debitis delectus dolorum', 5, 'Dolor quia vero illum aperiam. A repellat asperiores debitis aut sed. Sapiente sunt eveniet quidem et.', 'new', '2019-06-21 20:48:01', '2019-06-21 20:48:01'),
(7, 60, 21, 'quos nisi et', 3, 'Perspiciatis officia asperiores quo vero. Eligendi est blanditiis voluptatem illum. Consectetur sed recusandae itaque rerum omnis.', 'rejected', '2019-06-21 20:48:01', '2019-06-21 20:48:01'),
(8, 32, 21, 'est voluptas reprehenderit', 4, 'Et quos illo accusantium explicabo. Et rerum sit voluptate nobis iure quia. Provident porro eum et magnam corporis.', 'rejected', '2019-06-21 20:48:01', '2019-06-21 20:48:01'),
(9, 19, 13, 'unde delectus beatae', 3, 'Perspiciatis velit non voluptas aliquam soluta a eum architecto. Minima voluptatem repellendus error ipsa. Pariatur nam perferendis distinctio doloremque deleniti natus quidem ut.', 'new', '2019-06-21 20:48:01', '2019-06-21 20:48:01'),
(10, 12, 21, 'distinctio impedit nemo', 5, 'Voluptatum harum omnis qui cupiditate et sed dolorem voluptates. Aut ut est quia vel. In saepe qui fugiat aut nobis mollitia libero.', 'new', '2019-06-21 20:48:01', '2019-06-21 20:48:01'),
(11, 58, 15, 'et alias iure', 5, 'Est veniam reprehenderit omnis ut iusto non. Voluptatem error voluptatum cumque reprehenderit incidunt maiores illum. Cum aspernatur quam animi esse modi.', 'new', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(12, 3, 21, 'omnis amet velit', 3, 'Qui nulla nisi provident suscipit magni. Est et quae eum asperiores officia. Deserunt molestiae eos illo repellendus sunt dolorum.', 'new', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(13, 55, 16, 'ipsam quia quis', 1, 'Doloribus laboriosam consequatur impedit saepe sint dolorem. Quibusdam ducimus molestiae quis omnis. Impedit quaerat qui cum eum.', 'accepted', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(14, 29, 2, 'aperiam dolor ad', 4, 'Ipsum saepe est et sit assumenda. Iure nemo repudiandae odit accusantium. Tenetur ea officiis dicta atque maxime.', 'new', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(15, 54, 9, 'dignissimos architecto consectetur', 2, 'Delectus tempora velit quis ut aut. Autem et id recusandae iure culpa itaque. Qui totam corporis et expedita id voluptate omnis.', 'accepted', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(16, 53, 19, 'velit velit nam', 5, 'Suscipit exercitationem odit in dicta fugit cum ut. Harum dicta quia corporis quia quo. Est facere est aut.', 'new', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(17, 27, 16, 'in rerum alias', 5, 'Doloremque voluptas nisi laborum et. Sapiente ut aut rem blanditiis. Est ut eius illo.', 'rejected', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(18, 10, 17, 'dolor et labore', 2, 'A quos harum dolorum libero. Ut laborum quos alias itaque. Ipsam est tempora sed sed.', 'rejected', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(19, 37, 15, 'non sed voluptas', 3, 'Ut sunt iure consequatur quia. Nisi dolorem ut itaque aperiam. Possimus eos et ex et nisi.', 'accepted', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(20, 5, 20, 'dolore cum aut', 5, 'Et dolores quae dolor quasi cumque natus. Molestiae est est id enim mollitia laboriosam. Aut officiis voluptas sit beatae.', 'rejected', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(21, 56, 5, 'excepturi ut maxime', 4, 'Consequatur sunt voluptatem voluptatem velit. Minima mollitia et aut ipsa molestias. Aut aperiam voluptatem voluptatem.', 'rejected', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(22, 40, 15, 'animi unde possimus', 3, 'Et nobis voluptate mollitia. Eligendi consectetur saepe quia aspernatur eos. Cupiditate sequi consequatur dolores et.', 'accepted', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(23, 49, 14, 'et et sint', 4, 'Consequatur autem veniam dolores quis quas ea iste. Sed eos dignissimos sunt ut distinctio. Saepe nobis laboriosam et consequatur ex.', 'accepted', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(24, 53, 8, 'et ipsa eius', 1, 'Voluptate sit aut magnam nemo. Ad dolorem ad esse aut deserunt illum eligendi. Alias molestiae pariatur quia rerum dolorem.', 'new', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(25, 41, 16, 'culpa cum est', 2, 'Minus voluptas eum dolores sed. Id voluptates aut adipisci praesentium ipsa porro nesciunt. Reprehenderit voluptas eos nobis aut voluptatem officiis.', 'rejected', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(26, 18, 9, 'adipisci et maxime', 3, 'Deleniti deleniti perferendis exercitationem mollitia qui quia et. At pariatur voluptas inventore blanditiis itaque pariatur. Quos deserunt vel eos eos animi libero.', 'rejected', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(27, 47, 16, 'aut rerum eligendi', 5, 'Est modi et consequuntur quis at voluptates nesciunt dolor. Beatae provident eius vitae nihil temporibus qui non. Explicabo temporibus molestiae qui repellat quidem voluptates.', 'rejected', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(28, 45, 10, 'aut aut quo', 3, 'Maxime omnis dolores qui est velit dolorem explicabo. Similique voluptatem soluta eos in. Hic cupiditate veniam nesciunt sunt.', 'rejected', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(29, 42, 5, 'vel aut et', 2, 'Et alias necessitatibus aut atque repudiandae dolorem et. Quia in sequi et vero voluptas aliquam. Aut ad quos facere ratione mollitia quia maxime dolorum.', 'accepted', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(30, 58, 6, 'dicta est dolorum', 2, 'Autem doloribus officia suscipit maiores nesciunt voluptatem. Praesentium quis adipisci dicta et beatae deleniti. Ipsam sapiente consequatur quam qui.', 'new', '2019-06-21 20:48:02', '2019-06-21 20:48:02'),
(31, 22, 21, 'omnis repudiandae dolorem', 4, 'Provident qui itaque consequatur qui dolorem. Cum omnis asperiores dolor nobis. Ipsam qui quo dolore doloremque.', 'new', '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(32, 24, 6, 'est explicabo sint', 1, 'Repudiandae eos aut hic quis. Sed repellat fugiat iusto occaecati ea quaerat incidunt. Rerum ut nam beatae architecto sapiente vitae.', 'rejected', '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(33, 19, 14, 'itaque dolorem quos', 3, 'Sunt ullam vitae non laborum sequi dignissimos. Et eum ea sint at. Qui consequatur et ipsam doloremque et quisquam.', 'new', '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(34, 16, 9, 'magnam quia nemo', 1, 'Ut tenetur et numquam nihil. Est rerum autem explicabo voluptatem consequatur laborum. Est sapiente ut maiores quia aspernatur.', 'rejected', '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(35, 53, 12, 'excepturi dolor quae', 2, 'Sequi omnis non ut velit voluptas odio et sit. Qui saepe sit nostrum ducimus magnam perspiciatis. Repellendus sed aut distinctio.', 'new', '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(36, 60, 8, 'nemo aspernatur dolores', 5, 'Deleniti neque omnis nam quis. Facilis in architecto nihil deleniti eos voluptate laborum velit. Ipsum excepturi culpa occaecati qui.', 'rejected', '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(37, 15, 9, 'modi tempora beatae', 1, 'Et totam minus ratione voluptates tempore. Nostrum explicabo placeat doloribus reiciendis incidunt repellendus qui. Veritatis eum explicabo hic eum hic.', 'rejected', '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(38, 51, 11, 'nulla repudiandae et', 3, 'Consequatur quasi possimus consequatur nihil fuga et eum. Pariatur sit doloribus voluptas pariatur fugit maiores ut velit. Qui sit quia facere fuga expedita eius quis itaque.', 'accepted', '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(39, 55, 8, 'eum occaecati minus', 5, 'Quasi corporis dolorem autem dolores aut a quaerat ex. Libero est voluptatum hic. Debitis qui et et soluta asperiores magni.', 'accepted', '2019-06-21 20:48:03', '2019-06-21 20:48:03'),
(40, 16, 9, 'facere voluptatem nemo', 1, 'Beatae eos ipsa aperiam molestias unde deleniti. Adipisci voluptate nam commodi laudantium ad explicabo veritatis fuga. Optio aspernatur pariatur sit dignissimos odio qui.', 'accepted', '2019-06-21 20:48:03', '2019-06-21 20:48:03');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_taxon`
--

DROP TABLE IF EXISTS `sylius_product_taxon`;
CREATE TABLE IF NOT EXISTS `sylius_product_taxon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `taxon_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_taxon_idx` (`product_id`,`taxon_id`),
  KEY `IDX_169C6CD94584665A` (`product_id`),
  KEY `IDX_169C6CD9DE13F470` (`taxon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_taxon`
--

INSERT INTO `sylius_product_taxon` (`id`, `product_id`, `taxon_id`, `position`) VALUES
(2, 2, 2, 1),
(3, 3, 2, 2),
(4, 4, 2, 3),
(5, 5, 2, 4),
(6, 6, 2, 5),
(7, 7, 2, 6),
(8, 8, 2, 7),
(9, 9, 2, 8),
(10, 10, 2, 9),
(11, 11, 2, 10),
(12, 12, 2, 11),
(13, 13, 2, 12),
(14, 14, 2, 13),
(15, 15, 2, 14),
(16, 16, 3, 0),
(17, 17, 3, 1),
(18, 18, 3, 2),
(19, 19, 3, 3),
(20, 20, 3, 4),
(21, 21, 3, 5),
(22, 22, 3, 6),
(23, 23, 3, 7),
(24, 24, 3, 8),
(25, 25, 3, 9),
(26, 26, 3, 10),
(27, 27, 3, 11),
(28, 28, 3, 12),
(29, 29, 3, 13),
(30, 30, 3, 14),
(31, 31, 4, 0),
(32, 32, 4, 1),
(33, 33, 4, 2),
(34, 34, 4, 3),
(35, 35, 4, 4),
(36, 36, 4, 5),
(37, 37, 4, 6),
(38, 38, 4, 7),
(39, 39, 4, 8),
(40, 40, 4, 9),
(41, 41, 4, 10),
(42, 42, 4, 11),
(43, 43, 4, 12),
(44, 44, 4, 13),
(45, 45, 4, 14),
(46, 46, 5, 0),
(47, 46, 7, 0),
(48, 47, 5, 1),
(49, 47, 6, 0),
(50, 48, 5, 2),
(51, 48, 6, 1),
(52, 49, 5, 3),
(53, 49, 6, 2),
(54, 50, 5, 4),
(55, 50, 7, 1),
(56, 51, 5, 5),
(57, 51, 6, 3),
(58, 52, 5, 6),
(59, 52, 6, 4),
(60, 53, 5, 7),
(61, 53, 7, 2),
(62, 54, 5, 8),
(63, 54, 7, 3),
(64, 55, 5, 9),
(65, 55, 6, 5),
(66, 56, 5, 10),
(67, 56, 7, 4),
(68, 57, 5, 11),
(69, 57, 7, 5),
(70, 58, 5, 12),
(71, 58, 7, 6),
(72, 59, 5, 13),
(73, 59, 7, 7),
(74, 60, 5, 14),
(75, 60, 7, 8);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_translation`
--

DROP TABLE IF EXISTS `sylius_product_translation`;
CREATE TABLE IF NOT EXISTS `sylius_product_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` longtext COLLATE utf8_unicode_ci,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_105A9084180C698989D9B62` (`locale`,`slug`),
  UNIQUE KEY `sylius_product_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_105A9082C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_translation`
--

INSERT INTO `sylius_product_translation` (`id`, `translatable_id`, `name`, `slug`, `description`, `meta_keywords`, `meta_description`, `short_description`, `locale`) VALUES
(2, 2, 'Mug \"atque\"', 'mug-atque', 'Non et corrupti est odio eaque vel. Nihil labore ut numquam recusandae. Quia impedit dolor molestias aliquid dolorum.\n\nOfficia repellendus quisquam esse error facilis. Et sunt consequatur consectetur sunt. Quam et voluptates ex impedit.\n\nExplicabo sit nisi nemo odio harum distinctio voluptatum. Officiis odio vero libero fuga.', NULL, NULL, 'Eveniet voluptatem voluptates distinctio aut. Eveniet ad qui commodi dolores et. Eum impedit dolor voluptate ipsam aliquam molestiae et rerum. Dolor recusandae sit et dolorem et molestias. Fugit enim aut molestias nihil dolorem quae dolorum.', 'en_US'),
(3, 3, 'Mug \"aut\"', 'mug-aut', 'Voluptatum voluptate enim ea. Veritatis velit aut ut iste illo unde a. Ut aperiam delectus nobis officiis dolores.\n\nAt omnis explicabo quis enim qui amet. Deleniti architecto deleniti enim dolor qui a qui nemo. Similique error recusandae nemo veniam aliquid occaecati.\n\nQuis sunt sunt a autem. Explicabo vel qui sed laborum.', NULL, NULL, 'Consequatur veniam aspernatur pariatur doloremque minus ab doloremque. Voluptatum reiciendis enim odio quis nemo mollitia id. Rerum aut dolor amet id id. Voluptatum quo suscipit sint id sit eius non.', 'en_US'),
(4, 4, 'Mug \"non\"', 'mug-non', 'Provident quo omnis vero unde ut neque. Facilis dignissimos reiciendis vel ut. Quo necessitatibus et atque voluptatem.\n\nUnde odit error libero ullam fugit dolorem laboriosam. Repudiandae expedita labore nisi vel delectus possimus sapiente. Enim ipsa perferendis nisi. Numquam dignissimos ratione laborum perferendis non libero deserunt id.\n\nAspernatur excepturi est debitis deserunt quibusdam explicabo sed. Cumque temporibus sed aliquam suscipit.', NULL, NULL, 'Repellendus reprehenderit at sequi unde adipisci quo. Rem debitis impedit aliquam ex. Sapiente placeat commodi consequuntur natus tempore fugit sit veniam.', 'en_US'),
(5, 5, 'Mug \"eum\"', 'mug-eum', 'Voluptas deleniti modi reiciendis consequatur facilis quidem iure sapiente. Ducimus quod doloribus enim non illo repudiandae laborum ad. Aut quo doloribus ut id vero aut veniam.\n\nMaiores omnis ad ipsa occaecati expedita fuga molestiae qui. Accusamus et nulla nesciunt facilis. Sapiente id dolor voluptas dolor ex. Aut quidem sit iure eligendi iusto.\n\nAut officia recusandae sequi dolorem amet nam suscipit non. Eos quam officiis quos tempora aspernatur beatae. Et nesciunt recusandae eaque ut ea necessitatibus in laboriosam. Ut recusandae dolores accusamus veniam labore fugiat laudantium.', NULL, NULL, 'Corrupti ratione et omnis. Et doloribus praesentium quibusdam consequatur ab. Ut asperiores repellat ducimus laboriosam ad numquam omnis. Aliquid qui modi et repellendus sit optio sunt. Natus vitae consequatur quod qui deleniti facere ut.', 'en_US'),
(6, 6, 'Mug \"expedita\"', 'mug-expedita', 'Cupiditate consectetur velit maiores soluta nulla voluptatem. Amet culpa voluptatem nulla eaque nostrum odit optio. Qui cum distinctio quas id et error.\n\nQuia cupiditate enim non dignissimos molestiae autem laborum. Sint eveniet vero eos. Quo et aut modi amet ut. Reiciendis consequatur illo voluptate ut.\n\nDoloremque tenetur distinctio eius. Nisi dolor aliquid non ea magnam.', NULL, NULL, 'Magnam possimus quia quo saepe fuga consequatur iste totam. Fuga a facere et consectetur ea beatae iure. Temporibus quod aut corporis optio. Ut nam exercitationem rerum.', 'en_US'),
(7, 7, 'Mug \"tempore\"', 'mug-tempore', 'Autem sapiente nesciunt at autem voluptates magnam quae magni. Sed illo commodi quia voluptatem ex quia. At soluta a laborum quis et voluptas beatae.\n\nDignissimos omnis ratione animi optio nisi debitis. Vel illo aut minus quaerat rem aut. Quia nulla consequatur sit perferendis. A eaque mollitia nihil iusto.\n\nIpsa labore quae magni nemo quos. Est vero est autem est omnis ipsam ipsum. Consectetur tempora consequuntur voluptate eius voluptatibus qui. Aut ipsa fugiat harum.', NULL, NULL, 'Ut dolor pariatur ad ad a ratione. Cupiditate ipsam accusamus et dolorum. Ut officia ratione quis quis doloremque voluptas consequuntur est. Aperiam nemo omnis inventore est quaerat est.', 'en_US'),
(8, 8, 'Mug \"ratione\"', 'mug-ratione', 'Ipsa esse dolores minus. Aliquid iste dignissimos est iste ut distinctio. Quia architecto architecto rerum magnam.\n\nMagnam qui voluptatibus quisquam dolores. Id adipisci eum corrupti enim optio perspiciatis. Vel ratione quo sit quidem qui. Repudiandae in aut qui deserunt eaque enim recusandae.\n\nMolestiae quo odio et ut. Sequi et ducimus non qui ducimus quisquam. Repudiandae ipsum velit itaque et aut impedit.', NULL, NULL, 'Asperiores qui dolore voluptas. Aut impedit iure beatae praesentium necessitatibus aspernatur perspiciatis. Eaque et adipisci unde omnis molestiae qui aspernatur. Ipsa ex in deleniti distinctio.', 'en_US'),
(9, 9, 'Mug \"rerum\"', 'mug-rerum', 'Consequatur fugiat est voluptatem dolores. Error laboriosam facere aliquid aut. Voluptatem libero occaecati quibusdam quibusdam similique. Ipsam sed dolores recusandae tempora ea.\n\nUt expedita similique quos iure qui qui iure. Quia inventore quas eos maxime laudantium. Repellat enim et reiciendis aut. Rerum architecto qui ipsum amet accusantium accusamus.\n\nFugit placeat rerum repudiandae voluptate totam. Deserunt libero nemo ducimus ea impedit quasi. Ut iure repudiandae et delectus quae.', NULL, NULL, 'Quo eos omnis corrupti et. Corporis unde dolore eveniet ut neque et facilis. Repellendus sit quia occaecati doloribus accusantium omnis repudiandae.', 'en_US'),
(10, 10, 'Mug \"doloribus\"', 'mug-doloribus', 'Sed occaecati beatae aut ea. Velit animi iste ex aliquam. Quas et eius aspernatur delectus.\n\nConsequuntur occaecati reiciendis ipsum exercitationem neque blanditiis laudantium. Voluptas ut maiores perspiciatis placeat rerum vel. Ea quod et occaecati excepturi explicabo fugit omnis velit.\n\nDoloribus quod veniam sequi repellat. Tenetur quam quam ipsum iusto enim harum. Mollitia vero cupiditate explicabo nihil hic laborum excepturi. Illo omnis quam velit nihil sit voluptas magni.', NULL, NULL, 'Ut non sint sunt ab. Quisquam qui saepe illo ullam enim quis recusandae iusto. Ut recusandae doloremque error qui ducimus.', 'en_US'),
(11, 11, 'Mug \"ducimus\"', 'mug-ducimus', 'Voluptas asperiores nihil aut excepturi illum. Consectetur iste quam cum. Aliquid hic reiciendis odio. Repellendus quo magnam quia at est error.\n\nEum eos magnam neque aut distinctio placeat dicta in. Et expedita quis nam est tenetur.\n\nAt assumenda sint dolore qui. Provident fuga vitae dolorem quisquam quibusdam deserunt quidem. Laboriosam illum molestias ea omnis quis possimus.', NULL, NULL, 'Autem id harum quos cum aliquid. Similique aut voluptatem est eius consequatur exercitationem quo. Esse omnis aut numquam ut non maxime. Ipsam sed cupiditate qui doloremque.', 'en_US'),
(12, 12, 'Mug \"veritatis\"', 'mug-veritatis', 'Sed repudiandae numquam consequatur vel accusamus necessitatibus. Et rerum veritatis doloremque. Nulla aut consectetur perferendis autem error unde et recusandae.\n\nFuga omnis iste ut laborum praesentium. Ut tempora soluta quisquam sequi ut repellat. Dolores ut omnis voluptates sed a eos.\n\nQui culpa dolores rerum expedita voluptas non ea. Eveniet vitae et adipisci consequuntur. Nesciunt eos quos magnam facere praesentium numquam eos.', NULL, NULL, 'Officia sunt minima eligendi autem cupiditate debitis. Ratione dolore nihil beatae reprehenderit illum. Minus ut eius libero eos exercitationem ut. Odit nihil quia ex maiores temporibus ab rerum.', 'en_US'),
(13, 13, 'Mug \"sapiente\"', 'mug-sapiente', 'Sit in quia distinctio et magnam nostrum consequatur. Ut perferendis tempora quos. Sint tempore sit magnam at. Molestiae odit repellat fugit error aliquam eum.\n\nAut enim magnam libero repudiandae quidem. Id nostrum qui non accusamus molestiae omnis.\n\nEst aut nemo veniam unde non. Iste quia dolorum possimus. Quas nihil id harum non. Quas qui ut sunt dolorem.', NULL, NULL, 'Eaque qui atque ut quia voluptas maxime assumenda. Architecto expedita impedit nihil et magnam. Deleniti sapiente omnis omnis debitis ut sunt. Repellat perferendis quam aut eos expedita nulla.', 'en_US'),
(14, 14, 'Mug \"ipsum\"', 'mug-ipsum', 'Nostrum et qui quia amet. Iste in rerum excepturi qui dolor corporis. In dicta quam aperiam sunt reprehenderit eos accusamus. Libero quas provident vero asperiores sequi. Rem quae perferendis corrupti dolore est ab.\n\nAliquid officiis ad quia non ad alias culpa. Provident ut consequatur numquam et quae laudantium. Dolorem sapiente a debitis saepe velit.\n\nLaborum adipisci animi hic adipisci qui. Tenetur consectetur nostrum non voluptatem. Magnam minima molestias neque blanditiis id laborum. Fuga nesciunt qui ut dolores.', NULL, NULL, 'In amet perferendis id nobis sit error ea. Magni voluptas provident quia beatae saepe qui. Voluptas harum rerum voluptatum qui.', 'en_US'),
(15, 15, 'Mug \"eligendi\"', 'mug-eligendi', 'Id asperiores qui itaque. Architecto deserunt vel aut et rerum. Vitae nostrum sequi dignissimos exercitationem beatae.\n\nQui ipsum id qui ea ipsam laudantium. Quisquam nemo maiores aliquam et atque. Repudiandae ea quia deserunt voluptatem ipsa nihil et similique. Sed pariatur autem consequatur.\n\nDolorum quia voluptas expedita dignissimos exercitationem. Non laudantium voluptates sunt deleniti ut. Non sit suscipit minus eius est rerum sint esse.', NULL, NULL, 'Ab numquam esse laudantium amet deleniti. Delectus corrupti error omnis iste et nihil. Molestias sit provident hic. Dicta reiciendis perspiciatis voluptas maxime voluptates possimus reprehenderit.', 'en_US'),
(16, 16, 'Sticker \"qui\"', 'sticker-qui', 'Enim in voluptates eveniet labore. Nesciunt voluptate reiciendis eos omnis nihil ut. Eos maxime quam et labore.\n\nIste et hic et magni molestiae ut. Ullam delectus eum quisquam nam fugit reprehenderit sit. Voluptates id voluptates ullam quos architecto aut.\n\nDistinctio explicabo reiciendis incidunt voluptatem consequatur occaecati sed. In quo iste numquam voluptatem ut consequatur. Accusantium quidem est alias in aut.', NULL, NULL, 'Perspiciatis numquam et velit. Perspiciatis modi veritatis veritatis et. Aut culpa consectetur dolorem quia. Suscipit amet perferendis deleniti modi praesentium est consectetur.', 'en_US'),
(17, 17, 'Sticker \"cumque\"', 'sticker-cumque', 'Libero sit blanditiis et harum sit amet ut. Omnis itaque harum ut laudantium ullam. Vel placeat illo officiis error. Aliquam et accusantium adipisci voluptatem.\n\nEnim dolorum adipisci qui quod similique. Ullam sequi numquam consequatur dolor. Nemo praesentium nihil aut. Laboriosam hic hic ullam delectus recusandae tenetur aut.\n\nPorro amet modi inventore vel sunt sunt. Magnam incidunt nesciunt et ex doloremque. Corrupti cupiditate distinctio consequatur nihil omnis excepturi optio sint.', NULL, NULL, 'Ut similique explicabo enim. Enim laboriosam consequatur illum. Ut nostrum et et ea perferendis. Consequatur culpa autem qui provident.', 'en_US'),
(18, 18, 'Sticker \"aut\"', 'sticker-aut', 'Repellendus voluptas nemo voluptatem recusandae. Corporis odit eaque non vel expedita dolore eos. Alias veniam explicabo voluptatum quaerat non placeat et.\n\nVoluptates aut alias nostrum. Sit maxime non aut qui et aut. Eum aliquam impedit et sed. Ipsum ullam cupiditate ut velit enim ab voluptas.\n\nEt quo illum dolorum molestiae vitae. Qui molestias eum quis laborum dolores ullam.', NULL, NULL, 'Nemo quia assumenda libero et aut unde. Odit doloremque sed est aperiam inventore. Fugit ea et deserunt.', 'en_US'),
(19, 19, 'Sticker \"incidunt\"', 'sticker-incidunt', 'Ea aut praesentium distinctio labore ut sed. Quasi dolorem architecto perferendis alias adipisci voluptatem reprehenderit. Et accusamus consequatur laborum provident exercitationem consequuntur sequi. Inventore tempore et aut impedit omnis maiores praesentium non.\n\nEt doloribus ea laborum hic. Expedita dolor ratione tenetur saepe non.\n\nMagnam natus ex aut quo perferendis perspiciatis ipsa vero. Repudiandae et id omnis dolores distinctio optio maiores. Velit accusamus itaque quo voluptas enim ab.', NULL, NULL, 'Iure labore eveniet architecto sed eius excepturi velit. Dignissimos tenetur fugit veniam dolores minus est velit. Cumque harum et dolores minus. Animi nesciunt consequuntur eaque sed.', 'en_US'),
(20, 20, 'Sticker \"rerum\"', 'sticker-rerum', 'Doloribus at nisi quasi. Unde rerum quia amet ut omnis. Omnis et atque ipsum quia autem. Totam cupiditate est odio.\n\nNeque a voluptatem officiis in. In eaque nihil et aut sapiente corrupti fugit sed. Sint architecto sunt ut nostrum. Nulla et quia laborum sit labore.\n\nEt et unde aliquam natus. Sequi voluptas nihil dicta laudantium debitis necessitatibus suscipit nisi. Vero voluptate rerum non. Nobis quasi eveniet aut nemo.', NULL, NULL, 'Modi sunt vero id non soluta. Cum est omnis molestiae inventore distinctio et maiores perferendis. Illo doloremque autem architecto vel excepturi officia voluptas.', 'en_US'),
(21, 21, 'Sticker \"sint\"', 'sticker-sint', 'Ratione non ea non quibusdam. Voluptas illo accusantium sunt ipsa dolores. Fugit qui quis corporis iste.\n\nOmnis sit mollitia nesciunt deleniti. Aut ut itaque voluptatem omnis a.\n\nEa officiis laborum voluptatum ipsum voluptates. Quod et repudiandae rerum eaque aut ab harum. Dicta hic sed molestias asperiores deserunt.', NULL, NULL, 'Repellendus nostrum id voluptatem alias est alias exercitationem nostrum. Non explicabo eligendi aut neque.', 'en_US'),
(22, 22, 'Sticker \"harum\"', 'sticker-harum', 'Vero rem voluptas dolorem. Magnam commodi aut quod sed. Illo magni asperiores optio ratione molestiae eius.\n\nSunt rem molestiae sapiente quod cum a voluptatibus. Sunt sapiente sapiente rem consequatur est iure est. Dolorem amet explicabo omnis aut cumque. Quidem mollitia voluptatum temporibus rem expedita commodi ducimus velit. Ut est dolor aut quo maiores reprehenderit ut.\n\nEt unde consequatur qui eum at dolor sint. Est labore officiis cupiditate ut corrupti libero expedita. Qui rerum in dolores quae illum. Maiores reiciendis delectus eius voluptates culpa doloribus.', NULL, NULL, 'Mollitia assumenda quam quas quia illum libero. Quo fuga eos in perferendis reprehenderit culpa. Reprehenderit dignissimos ex est eos.', 'en_US'),
(23, 23, 'Sticker \"doloribus\"', 'sticker-doloribus', 'Rerum aut ut in atque nobis. Quia sit et sunt modi autem labore mollitia. Velit quia voluptatem et quibusdam. Veritatis quia nisi voluptas maxime quaerat dolorem ut tempore.\n\nRepudiandae quia et occaecati neque. Dolorum ipsam quia id totam aut consequuntur.\n\nAspernatur magnam impedit numquam delectus a. Maxime eum id qui sint. Qui voluptatibus et consequatur placeat architecto possimus.', NULL, NULL, 'Labore quisquam veniam voluptas rem non asperiores. Voluptas dolores cum omnis consequatur quis officia. Blanditiis aut repudiandae voluptate consectetur exercitationem non corporis.', 'en_US'),
(24, 24, 'Sticker \"exercitationem\"', 'sticker-exercitationem', 'Magni quia veritatis est repudiandae. Consequatur minus deleniti veritatis. Officia sit aut quibusdam sapiente. Sunt dolorem minus deleniti perspiciatis.\n\nError voluptas autem aperiam non. Repudiandae totam nihil voluptate perferendis. Pariatur odit distinctio saepe mollitia omnis eius omnis accusantium.\n\nRepellat similique sed adipisci. Esse placeat ratione expedita. Dignissimos nesciunt quo autem id et et nihil. Veniam in quam aut dolor odit.', NULL, NULL, 'Qui minima adipisci dolorem qui rerum aspernatur aut qui. Molestiae velit voluptas magnam odio voluptas aut est. Sed est aliquid vel sapiente perspiciatis quo corrupti facere. Voluptas accusantium totam nihil consequuntur odio expedita eveniet.', 'en_US'),
(25, 25, 'Sticker \"dolorum\"', 'sticker-dolorum', 'Tempora asperiores est dolorem distinctio officia molestiae laboriosam. Perspiciatis quae vitae velit sunt optio dolor. Occaecati iure error et amet recusandae aut. Aspernatur rerum culpa pariatur maiores.\n\nEst eligendi expedita et sed minus. Ut cumque tenetur illum quis expedita ipsum inventore. Quia maxime non numquam quos veritatis deleniti facilis.\n\nIure molestiae eum non eaque et. Vel fugiat qui sit. Minima nobis et officiis ut voluptatem enim quam.', NULL, NULL, 'Similique voluptatem temporibus nobis quo impedit nisi pariatur quia. Possimus dolor voluptas consectetur recusandae earum eveniet. Expedita necessitatibus illo enim est sunt sit rerum.', 'en_US'),
(26, 26, 'Sticker \"molestias\"', 'sticker-molestias', 'Eveniet ex architecto tenetur voluptates natus odio placeat sint. Quos id assumenda consequuntur nemo rerum quisquam recusandae corporis. Quibusdam nulla sint aliquid et consequatur. Alias et illo rem laudantium dicta cum quas voluptatem.\n\nConsequatur soluta animi optio. Et tempore consequatur qui qui neque et ipsa. Quae necessitatibus placeat velit ipsam voluptatibus explicabo et.\n\nMinima qui facere quia. Consequatur non eum a quas autem aspernatur. Tenetur labore dolore dolorem.', NULL, NULL, 'Aliquid ratione corrupti voluptate beatae sunt. Optio debitis accusantium maxime maxime et eaque. Excepturi voluptatum et repellendus eligendi aut dolorem et cupiditate.', 'en_US'),
(27, 27, 'Sticker \"illo\"', 'sticker-illo', 'Minus et quia dolores eius porro. Occaecati cupiditate incidunt distinctio dolores et. Ratione quis nihil officia sunt. Omnis optio ut laudantium quas nostrum.\n\nSuscipit amet rerum vero. Nisi provident labore odit ipsa minima deserunt aliquam. Magni nobis impedit blanditiis voluptatem delectus voluptatibus rerum.\n\nVitae et similique et. Modi nostrum et aspernatur deserunt corrupti. Corporis nihil perferendis suscipit necessitatibus unde. Numquam minus provident quis blanditiis non cumque molestiae.', NULL, NULL, 'Non officia sit soluta. Voluptatibus cum facere et rerum sed. Vero sed accusantium consequuntur et magni.', 'en_US'),
(28, 28, 'Sticker \"velit\"', 'sticker-velit', 'Illo enim ipsum deleniti hic molestiae. Temporibus sit voluptatem sapiente perferendis. Iste maxime et et aut non. Laboriosam nisi qui quia facilis pariatur quod sed.\n\nTenetur nostrum facilis consequatur aut impedit qui animi. Incidunt laboriosam ipsam cupiditate aperiam commodi et rerum. Eum cumque temporibus dolor. Officiis et quo quia aut.\n\nEt impedit est eligendi ut et nulla ratione corrupti. Eligendi illum in voluptatem autem perferendis nihil. Voluptatem nisi et error culpa fuga non dolor. Enim quis odio soluta quo pariatur amet distinctio.', NULL, NULL, 'Sed qui corporis dicta et non enim architecto. Quos ut modi neque rerum repudiandae. Expedita consequatur est nemo praesentium debitis omnis inventore. Omnis nisi odit est doloribus excepturi.', 'en_US'),
(29, 29, 'Sticker \"unde\"', 'sticker-unde', 'Suscipit assumenda inventore nesciunt non. Ea deserunt eligendi eius magnam qui.\n\nDoloremque enim tempore voluptates ex aut sint hic. Consequatur dolorem ut tenetur debitis deleniti distinctio dolorem. Et autem neque eum illo numquam consequuntur.\n\nAmet nulla cum quisquam. Aperiam ea ea sapiente ducimus ab quo laborum commodi. Eum dolores sint consequatur sed quia laboriosam. Dicta provident quod animi unde alias aut.', NULL, NULL, 'Officia aspernatur ullam sit ad voluptates magni. Aut est asperiores et. Ipsum ad repellat non dolores aut nihil. Eaque eum adipisci incidunt sint facilis dolor impedit.', 'en_US'),
(30, 30, 'Sticker \"ducimus\"', 'sticker-ducimus', 'Eum tempore quis asperiores nisi maxime ut exercitationem. Ut sunt quos sunt magnam impedit fugiat excepturi non.\n\nQui rerum iste nihil aperiam nesciunt. Voluptatem consequuntur dolor qui quasi. Consequatur inventore itaque deleniti esse omnis impedit et doloribus.\n\nAsperiores placeat accusantium provident soluta. Est magni minus quibusdam voluptas totam eligendi corrupti consequatur. Voluptas velit qui corporis qui voluptas ab. Officia aut aut veritatis omnis atque consequatur adipisci.', NULL, NULL, 'Maiores fugiat eum expedita vitae quis similique inventore. Placeat et accusamus debitis qui. Praesentium ut quo eos perferendis vel velit libero temporibus.', 'en_US'),
(31, 31, 'Book \"quidem\" by Eladio Smitham', 'book-quidem-by-eladio-smitham', 'Et voluptatum officia totam eos quis est molestiae. Et dolorum provident blanditiis eum voluptatem. Quos veritatis aut blanditiis in earum vero.\n\nQuia ipsam explicabo nam. Atque nam esse commodi eveniet eius debitis. Eaque placeat error aliquid vel id in quo.\n\nPossimus suscipit inventore vel est unde. Nemo deleniti est enim molestiae cupiditate. Voluptates perferendis deserunt delectus ut ut.', NULL, NULL, 'Itaque rerum et veritatis eligendi aspernatur. Velit accusantium et deserunt accusantium commodi minus optio. Occaecati dolorem repellendus corporis.', 'en_US'),
(32, 32, 'Book \"ea\" by Mrs. Maggie Mitchell III', 'book-ea-by-mrs-maggie-mitchell-iii', 'Sed placeat aspernatur dolorum eius ut sint. Nesciunt rerum eius nemo omnis iste magni explicabo. Vel quae qui deserunt deleniti numquam. Voluptate repellendus laboriosam doloribus quam ratione eum unde.\n\nFacilis voluptas aut omnis qui perspiciatis expedita. Suscipit doloribus fuga libero ipsa. Qui eaque eos aliquam voluptatibus esse. Recusandae eligendi corrupti corporis omnis quae quis molestiae quidem.\n\nIpsa esse error fugit. Occaecati perferendis iure delectus molestiae voluptas praesentium fugit fugiat. Non quis omnis hic voluptatum quo.', NULL, NULL, 'Dolorum omnis illo id rerum optio beatae porro. Expedita maxime molestiae nobis rerum ab. Repellendus inventore maiores id et. Culpa dolorem eius ut exercitationem in sit.', 'en_US'),
(33, 33, 'Book \"eveniet\" by Kyla Heidenreich', 'book-eveniet-by-kyla-heidenreich', 'Nostrum necessitatibus consequatur dolorem quisquam et. Laborum animi beatae animi eum. Quas aliquid dignissimos quod aut nam est architecto. Omnis magni ea minima praesentium libero labore ut.\n\nUt esse suscipit quo. Aliquam et voluptate magnam rerum sunt perferendis nihil voluptatum. Id ut consequatur accusamus sed facilis et reiciendis. Perferendis alias odio blanditiis placeat dolores ut dolore.\n\nEum nulla incidunt dolorum ut consequatur velit. Rerum rerum ea iusto tempora. Id tempore quis facilis nihil eligendi qui libero.', NULL, NULL, 'Nulla corrupti doloribus ipsum. Ab est adipisci commodi reprehenderit consequatur quibusdam temporibus. Itaque quia vel fugit repellat. Ab aut aut et voluptas facilis iure velit est.', 'en_US'),
(34, 34, 'Book \"qui\" by Harrison Fritsch', 'book-qui-by-harrison-fritsch', 'Sunt et officiis pariatur eum consectetur veritatis quia. In totam iusto quia ullam. Exercitationem consequuntur consequatur voluptatibus sed sapiente qui quasi tenetur. Fugit vitae optio corporis autem iusto dolorem rerum.\n\nArchitecto ut voluptatem aut dolore voluptatem blanditiis. Suscipit molestiae qui molestiae sequi. Dolore facilis veritatis optio cum aut expedita itaque.\n\nRecusandae in doloribus maxime error quo. Quo magni accusantium totam. Quis architecto quia dolorem voluptas.', NULL, NULL, 'Sint vitae nisi in repellat voluptatum consequatur. Porro voluptates magni non eveniet eum. Sequi voluptatem reprehenderit minima iure iusto.', 'en_US'),
(35, 35, 'Book \"porro\" by Ms. Damaris Lindgren Sr.', 'book-porro-by-ms-damaris-lindgren-sr', 'Et maiores vel aut sit odit nulla non at. Voluptatem voluptatem eos qui aut animi qui reprehenderit. Labore tenetur iste laboriosam dicta asperiores quasi enim eligendi.\n\nVelit distinctio enim recusandae provident aut perferendis eum. Voluptatem similique distinctio est dolorem. Necessitatibus recusandae facere magnam itaque adipisci amet neque. Dolor fugit accusamus omnis quidem enim voluptas incidunt.\n\nDoloribus laudantium voluptate cumque illo sint. Illo vitae quisquam totam et impedit. Natus odit nisi aspernatur esse vitae.', NULL, NULL, 'Nulla alias est provident qui est quidem illum. Illum voluptatem dolor consectetur id expedita eum. Dolor eveniet est et quo porro. Ea aut sint porro.', 'en_US'),
(36, 36, 'Book \"et\" by Dorothea Hartmann', 'book-et-by-dorothea-hartmann', 'Possimus explicabo maiores eum omnis et. Rerum aut facere deleniti id eaque error. Dolores eum non rerum debitis explicabo quam. Saepe architecto quo quis ad omnis omnis et.\n\nRem mollitia omnis ipsa aut laborum delectus. Reiciendis dolore illum et quis. Id dolorem nesciunt asperiores.\n\nAccusamus dolores ad dolore a fuga dolores veritatis. Assumenda enim ipsum et quia eum asperiores quidem. Illum non aut ut. Vero optio omnis repellendus et maiores facere.', NULL, NULL, 'Culpa dicta reiciendis ut. Eligendi magnam dolore iusto et assumenda ducimus. Ut aut quia doloremque nemo fugiat.', 'en_US'),
(37, 37, 'Book \"dolore\" by Jalen Russel', 'book-dolore-by-jalen-russel', 'Quo distinctio aliquam cum adipisci ea. Expedita explicabo sit dolorem cum quia id. Optio quo explicabo odit soluta.\n\nVoluptate natus architecto reiciendis et neque molestiae et et. Sit dolor sed placeat consequuntur voluptatem. Itaque odio vero recusandae ut.\n\nAut aut reprehenderit vel quas officiis eius voluptatibus. Officiis necessitatibus sapiente et eos iure nulla sunt. Soluta et itaque id nemo ut repellendus impedit laudantium.', NULL, NULL, 'Recusandae soluta sint molestias ullam asperiores voluptas. Cum qui rem commodi quam delectus ea consequuntur incidunt. Reprehenderit et magni aliquid sequi odio iste. Minima voluptatem a nisi.', 'en_US'),
(38, 38, 'Book \"est\" by Nash Stanton', 'book-est-by-nash-stanton', 'Enim dolorem dignissimos doloremque atque quasi rerum. Libero eos sint quam delectus reiciendis laboriosam quo odit.\n\nQuod molestiae provident ut quia. Impedit necessitatibus repellat mollitia voluptatem. Quae dicta mollitia dicta nam repellat ratione. Omnis non quam quasi eligendi.\n\nNon id vitae aliquam nemo officia. Enim quisquam quos voluptatum aut quasi sunt. Aut qui sint incidunt mollitia rem consequatur. Eum qui quia est in qui molestias optio eos. In culpa aut rem tenetur.', NULL, NULL, 'Aut repudiandae soluta unde dignissimos. Dolores consequatur fugit est qui in. Consequatur provident eum ducimus ad. Qui delectus omnis ducimus magnam et autem.', 'en_US'),
(39, 39, 'Book \"vero\" by Mrs. Joelle Zulauf', 'book-vero-by-mrs-joelle-zulauf', 'Et est velit et. Consectetur omnis praesentium non vel nihil quod vitae. Soluta temporibus quia rerum quia.\n\nIusto non fuga est non eaque cupiditate ipsam. Ut eos est qui pariatur. Excepturi sed tenetur facilis dolorem quibusdam id aut. Odit vero in tempore similique commodi magnam necessitatibus adipisci. Similique suscipit nesciunt qui aperiam blanditiis quis nisi.\n\nSoluta non id nulla porro. Enim porro quaerat at dolorem in quia molestiae qui. Corrupti quos est perferendis explicabo sed laborum accusamus. Pariatur placeat itaque quas molestiae repellendus voluptates. Aperiam doloremque sapiente iste sint.', NULL, NULL, 'Eligendi illum aliquam quod autem eius omnis voluptates. Voluptatibus labore qui itaque eaque quo nulla. Similique illum atque sapiente et aut error libero iure.', 'en_US'),
(40, 40, 'Book \"possimus\" by Marlin Marquardt I', 'book-possimus-by-marlin-marquardt-i', 'Itaque nihil soluta sunt omnis. Sapiente iure sunt adipisci est voluptatem quis. Sequi rerum minima non nam ut odio nemo.\n\nOfficiis quaerat qui eveniet et iusto possimus non dolorem. Est commodi accusamus veritatis optio ex et fugit. Commodi non assumenda unde. Repudiandae reiciendis dolore quos eum rerum ipsa placeat. Fuga at aut quia pariatur necessitatibus minima deserunt.\n\nMagnam autem qui ut ut. Atque aut molestias voluptas asperiores repudiandae. Sit adipisci autem nemo provident. Qui optio eos officia. Iure aut sint esse est nam.', NULL, NULL, 'Officia ducimus rerum ea deserunt debitis modi recusandae. Veniam id nihil enim et dolorum quis. Consequatur qui dolores nihil et. Ad sunt voluptas dicta eveniet commodi.', 'en_US'),
(41, 41, 'Book \"tempora\" by Pietro Gaylord', 'book-tempora-by-pietro-gaylord', 'Voluptatum consequatur dignissimos doloremque molestias nihil. Placeat beatae repellat sit voluptatibus. At dolorum provident voluptatem iusto perferendis voluptatem quia.\n\nEt alias est nisi quaerat voluptas quibusdam. Est optio laudantium esse et debitis omnis dolores. Enim ut nam necessitatibus iusto earum cupiditate qui.\n\nEligendi adipisci et itaque et. Repellat vero sint rerum et. Consequatur facilis quia earum omnis necessitatibus sequi. Et et fugiat consequatur voluptatem aut et.', NULL, NULL, 'Sint est pariatur velit quia quo corporis ut nemo. Labore eum ex omnis aspernatur voluptatum eveniet excepturi. Quia fuga et porro quidem sunt illum. Et dolorem minus est tenetur eius sed.', 'en_US'),
(42, 42, 'Book \"architecto\" by Manuela Metz', 'book-architecto-by-manuela-metz', 'Quia nemo dolorum ut sequi. Voluptatum pariatur qui fuga. Fuga sit mollitia pariatur.\n\nRepellat eos et sit voluptas asperiores. Aut saepe est expedita molestias excepturi labore iste. Voluptas aut aut fugit ducimus et distinctio sit sit. Cupiditate esse aliquam fugit nostrum.\n\nMolestiae eaque cumque et consequuntur facilis et beatae. Nesciunt eos praesentium repellat aut alias. Non eius nostrum odit debitis aut. Deleniti est quae hic quidem.', NULL, NULL, 'Facere qui praesentium deleniti blanditiis eos. Tempora explicabo reprehenderit illo molestiae nihil voluptas. Velit est voluptatem eos id sed sed optio.', 'en_US'),
(43, 43, 'Book \"eos\" by Noble Kshlerin I', 'book-eos-by-noble-kshlerin-i', 'Modi et non autem sed amet beatae. Eligendi modi eaque voluptatem. Et nesciunt commodi ex unde aperiam ut nulla.\n\nOdit in dolore magni voluptatem. Sit blanditiis ab voluptatem ut. Deserunt quo cum ea iste minima minima possimus. Sit nostrum quo ut veniam aperiam voluptatem harum.\n\nPerferendis possimus ut vero eum sint. Natus ducimus aspernatur libero ad nostrum. Dignissimos harum qui beatae qui quo numquam molestiae.', NULL, NULL, 'Officia iure aut minima ut numquam. Optio ut nisi amet nobis nobis nesciunt. Sed corrupti delectus reprehenderit nam nostrum. Error reiciendis voluptatum quasi tempore sed.', 'en_US'),
(44, 44, 'Book \"explicabo\" by Ms. Amalia Ratke', 'book-explicabo-by-ms-amalia-ratke', 'Dolorum sit illo et nihil. Amet ducimus sunt dolores. Voluptatem labore ipsa fugiat ipsam reiciendis quaerat.\n\nDolor ut rerum accusamus fugit quia ut sit. Numquam et aspernatur omnis nihil. Veritatis quaerat est culpa esse libero inventore voluptatem.\n\nIn atque voluptatem veniam voluptatem aspernatur quam delectus. Nihil consequatur voluptatem ullam perspiciatis. Delectus architecto sequi odit eveniet explicabo consequatur at.', NULL, NULL, 'Eveniet velit consequatur dolore impedit architecto qui ipsum. Pariatur quo culpa alias fuga autem. Eligendi amet impedit suscipit exercitationem qui. Omnis soluta voluptates sint.', 'en_US'),
(45, 45, 'Book \"debitis\" by Prof. Quincy Gaylord MD', 'book-debitis-by-prof-quincy-gaylord-md', 'Pariatur enim in in fugiat. Eligendi laudantium commodi est eum dicta et. Labore adipisci corrupti omnis.\n\nEt qui optio quidem rerum mollitia. Consequuntur expedita mollitia nostrum accusamus aut officia quis. Et et tempora natus reprehenderit qui atque. Temporibus ea facilis est nihil quia odio.\n\nVoluptatem quae sint qui ratione iusto consectetur quas. Amet eius corporis tempora. Occaecati quisquam qui molestias sit non nihil nulla. Eveniet laboriosam in aut excepturi voluptas et. Molestiae aut molestias sunt.', NULL, NULL, 'Praesentium qui in recusandae et nobis aut debitis. Excepturi rerum quaerat eum totam. Qui nobis sit consequatur adipisci qui modi inventore accusantium.', 'en_US'),
(46, 46, 'T-Shirt \"suscipit\"', 't-shirt-suscipit', 'Totam sit necessitatibus impedit esse. Excepturi mollitia laboriosam voluptatem quia repellat soluta quia. Voluptate quis qui earum quod sequi non.\n\nFugit culpa excepturi quibusdam maxime quisquam sint cum. Amet temporibus ut beatae et minus eum incidunt.\n\nVel ex fugiat accusamus. In adipisci omnis exercitationem enim officiis omnis. Quia itaque dolorem non ea voluptas officiis. Assumenda tempore optio dolorem dolorum. Qui commodi occaecati nobis quidem rerum.', NULL, NULL, 'Et ad voluptatem libero voluptates provident consectetur sit. Aut velit expedita sit qui maiores. Dolores recusandae voluptates et placeat expedita. Dolores provident laborum dolores asperiores dicta.', 'en_US'),
(47, 47, 'T-Shirt \"vel\"', 't-shirt-vel', 'Et ut voluptatem animi ut voluptas deleniti. Ad doloribus corrupti pariatur exercitationem. Blanditiis fuga blanditiis doloremque dolores. Delectus aut qui quo nisi pariatur aut et ex.\n\nNihil ut qui fuga ipsum ab in non voluptatum. Mollitia ut ut consequuntur excepturi. Sit voluptatem necessitatibus iusto voluptatum aut ea.\n\nQui enim aliquid vitae necessitatibus quis quis ab. Quidem occaecati omnis ut. Id nihil ad commodi unde voluptatem repellat numquam.', NULL, NULL, 'Nostrum sunt et explicabo consequuntur natus nobis magnam. Rerum nulla est illo expedita neque rerum magnam. Dolor iste placeat eaque perspiciatis.', 'en_US'),
(48, 48, 'T-Shirt \"unde\"', 't-shirt-unde', 'Odit qui reiciendis sed eos doloribus alias. Earum quam vel voluptatem ut iste qui. Et quo quasi dolorem aliquid provident qui sed. Quas nobis corrupti suscipit nobis quis quas ut.\n\nOfficiis et suscipit qui itaque blanditiis. Et ea sapiente omnis. Placeat accusamus illo magnam quasi tenetur voluptate.\n\nAmet quo consequuntur voluptatem dolorum porro natus culpa ab. Nostrum aut non repellat animi amet rerum eos. In accusamus alias earum et provident.', NULL, NULL, 'Sunt molestiae excepturi esse vel vel nam illum accusamus. Ab ea placeat iste similique quia maxime. Possimus quae qui eius iusto voluptatem expedita. Alias rerum sequi nisi saepe neque hic quo.', 'en_US'),
(49, 49, 'T-Shirt \"error\"', 't-shirt-error', 'Labore voluptatum in quia saepe. Atque dolorum molestiae quisquam. Est et aut sapiente assumenda. Velit quidem fuga dolores non assumenda vitae.\n\nIn soluta doloribus est quisquam. Minima eligendi consequuntur nam exercitationem eos omnis. Provident nisi occaecati ea tempore.\n\nUt animi rerum ratione esse. Dicta eum harum dolores quam rerum. Accusantium et a quaerat perferendis alias sequi et.', NULL, NULL, 'Qui vero cumque minus voluptate error accusantium itaque amet. Molestiae rem porro et voluptatem ut sed quam.', 'en_US'),
(50, 50, 'T-Shirt \"quibusdam\"', 't-shirt-quibusdam', 'Sit et et sed molestias nostrum autem. Nisi et sint non ut repellendus illum sit. Omnis aut quaerat cupiditate quae est.\n\nDignissimos nesciunt facilis commodi cumque iure reiciendis. Quia omnis dignissimos sunt nulla id quia. Quos sed praesentium doloribus officiis perferendis rerum ea. Consectetur atque et quae voluptate sed.\n\nIpsa veritatis et et rem dicta praesentium maiores. Et tenetur saepe et accusamus eaque et consequatur. Harum nam veniam ex nihil dignissimos. Aspernatur consequatur quidem provident iusto.', NULL, NULL, 'Fugiat aut et in et. Odio nesciunt blanditiis asperiores quo similique quas fuga. Aliquid omnis ut debitis aspernatur sequi.', 'en_US'),
(51, 51, 'T-Shirt \"ipsam\"', 't-shirt-ipsam', 'Alias voluptatem ex delectus beatae aliquam ipsa et. Asperiores reprehenderit qui voluptas incidunt. Molestiae laboriosam adipisci asperiores necessitatibus sunt non. Voluptatem ratione autem neque.\n\nAut libero quam cum soluta voluptatum et. Temporibus harum molestias sed vero voluptas molestias voluptatibus.\n\nMagni est sit et sed eligendi voluptates nulla. Tempore iste consequatur pariatur omnis omnis eum. Et qui quas quo est atque.', NULL, NULL, 'Sit excepturi assumenda explicabo nobis similique aperiam corporis. Doloribus pariatur maiores cum. Delectus id repellendus sit ut.', 'en_US'),
(52, 52, 'T-Shirt \"nihil\"', 't-shirt-nihil', 'Repellat reprehenderit mollitia dolorem rerum eveniet. Harum dolor quis ipsa. Sunt soluta reprehenderit nihil autem.\n\nIpsa nisi architecto tenetur ab fuga mollitia est molestias. Et voluptatum corporis eos molestiae doloremque. Voluptatem corrupti dolorem laudantium atque et ut est. Vel incidunt et quia earum dolorum nobis quam.\n\nNostrum est odio et vel est officia quam animi. Beatae quas totam odit similique et. Iste dicta impedit quam ut fugiat.', NULL, NULL, 'Asperiores dicta facere beatae. Dolore nam inventore neque sunt sit inventore. Est quis quisquam dolores tempora. Esse ut nam itaque inventore. Quas quo sunt qui iusto.', 'en_US'),
(53, 53, 'T-Shirt \"voluptas\"', 't-shirt-voluptas', 'Repellat nesciunt porro pariatur qui et adipisci. Saepe sint et eos occaecati libero. Velit id ipsa quos fuga sed iusto provident. Et consectetur et iure cum accusantium.\n\nAutem rerum nobis dolorum earum et quibusdam. Dolore sit exercitationem quia quo itaque vel quas molestiae. Totam et est dicta sint natus autem perspiciatis. Fugit molestias debitis fugiat ipsam.\n\nNihil quia tempore accusamus. Beatae at voluptatem id. Deserunt debitis ut temporibus modi eaque.', NULL, NULL, 'Est doloremque ad distinctio cumque consequuntur. Ut voluptatibus quisquam nemo ad et numquam quia. Ut quidem porro odit iusto qui provident facere. Earum occaecati suscipit iste id eos a qui ipsum.', 'en_US'),
(54, 54, 'T-Shirt \"eum\"', 't-shirt-eum', 'Minus tenetur placeat ab eius voluptatem impedit. Dolorum eveniet ipsum iste dolorem possimus facere aperiam beatae. Nulla neque ut iste est. Deleniti consectetur repellat id sint sit suscipit quia culpa.\n\nNam ex eum vel. Itaque nesciunt adipisci consequatur deserunt sed iusto et quae. Qui porro quo optio sint enim. Maxime et reprehenderit consequatur.\n\nOfficiis architecto unde odio laudantium. Omnis ex dolor enim delectus magnam totam. Aut voluptatum eos doloremque dicta reiciendis earum. Maiores tenetur dolores dolor ipsa optio.', NULL, NULL, 'Molestias adipisci autem temporibus et veniam accusamus eum iure. Corrupti sint animi enim quaerat provident. Consequatur tempore ut quis ut saepe et.', 'en_US'),
(55, 55, 'T-Shirt \"et\"', 't-shirt-et', 'Vel assumenda beatae est. Ipsam perferendis rerum voluptatem modi consequatur. Et voluptas ea ipsam qui animi fugit est.\n\nBlanditiis consequatur ut enim aut deserunt sint. Repudiandae praesentium quod cum assumenda qui ut quis. Iste est sit quae ea ut similique quas. Molestias harum alias aliquam sit vero explicabo delectus cum.\n\nNisi fugit et et voluptatem rerum. Eum cumque et accusantium temporibus iure iste cumque. Sint perspiciatis alias nemo.', NULL, NULL, 'Corporis omnis officia omnis et eveniet dolorum rerum id. Aspernatur ipsum minima neque. Deleniti officiis eum dignissimos voluptatem laborum totam laboriosam.', 'en_US'),
(56, 56, 'T-Shirt \"veniam\"', 't-shirt-veniam', 'Ullam vel distinctio voluptate soluta magnam. Dolor qui numquam dolor. Et impedit voluptatibus iste in.\n\nDistinctio unde veritatis voluptatibus quisquam. A et enim facere officia.\n\nAmet provident illum nobis quis aut est aut. Quia minima rerum ad sit aut enim aperiam. Ullam quidem atque fugiat in dolores quia dolores. Ea iste libero corrupti exercitationem nihil.', NULL, NULL, 'Enim est nemo eos molestiae repudiandae autem. Soluta sed et voluptatem dolores. Dolorem perferendis perspiciatis ipsum cumque. Et eum nam suscipit inventore iusto.', 'en_US'),
(57, 57, 'T-Shirt \"numquam\"', 't-shirt-numquam', 'Molestiae quo quidem fuga. Omnis aliquam adipisci enim. Excepturi unde reprehenderit fugit veritatis quod.\n\nNihil et excepturi voluptates totam. Laborum et rerum explicabo et quod dicta. Numquam est perferendis excepturi et.\n\nAliquid reiciendis reiciendis rerum eveniet iste consectetur et quasi. Ut velit nihil dignissimos minima mollitia doloremque quod. Perspiciatis saepe facilis hic possimus aut dicta reprehenderit ea. Animi aperiam cupiditate quaerat ab quidem minima voluptatem.', NULL, NULL, 'Quia sit autem voluptatem quasi in perspiciatis. Neque sint similique et omnis. Quia nihil voluptas laboriosam adipisci modi sit.', 'en_US'),
(58, 58, 'T-Shirt \"omnis\"', 't-shirt-omnis', 'Eum magni velit iste expedita et qui. Rerum aut quo amet rerum adipisci dolores culpa. Dignissimos et consequatur inventore ut nemo. Voluptatum ut earum alias aut quia ullam.\n\nTemporibus quod ut quis. Consequatur perspiciatis aut unde velit quo voluptate quaerat. Deserunt maxime et magnam est qui inventore numquam quia. Minima odio doloremque et sunt sunt ut corrupti.\n\nDolore et sit tempora quia placeat. Fugiat expedita laborum ut ea. Qui aliquam voluptatem sit. Incidunt et velit aut qui alias illum cum.', NULL, NULL, 'Vero nesciunt commodi ut consequuntur facilis quibusdam. Accusamus quidem sapiente delectus aut consequatur ipsam quia. A cumque dolores adipisci neque.', 'en_US'),
(59, 59, 'T-Shirt \"non\"', 't-shirt-non', 'Officiis quia et accusantium illum eligendi magni. Autem nam non quae laboriosam ut voluptatem. Accusantium magnam eos sed culpa non officia. Id dolores aspernatur qui natus voluptatem assumenda recusandae.\n\nEt illo sunt ea voluptatem aliquam. Cupiditate dolores quae vel nihil sunt. Eligendi quos est eius et. Aut aut dolores distinctio ea pariatur voluptas.\n\nAlias provident dicta deserunt consequuntur distinctio consequatur et magnam. Ut non ut eius facere dolor provident. Esse tenetur necessitatibus libero asperiores explicabo sunt. Optio reiciendis dolorem saepe veniam facilis consequatur nobis.', NULL, NULL, 'Sint esse quasi tempore voluptates sint non ullam. Odit labore at perferendis ut quia assumenda ad alias. Est cupiditate fuga delectus fugit. Quia fuga tenetur minima magnam et qui non.', 'en_US'),
(60, 60, 'T-Shirt \"voluptatem\"', 't-shirt-voluptatem', 'Accusamus repellat sit voluptatem facere veritatis eligendi. Delectus sunt nostrum deserunt quia aut. Deserunt recusandae qui sed magni autem id quo.\n\nExplicabo incidunt optio adipisci maxime veritatis totam. Totam et praesentium impedit et eos. Suscipit et dolores est ut saepe.\n\nVel recusandae omnis sed. Porro et et explicabo assumenda. Eveniet id dolore et et est porro corrupti. Quam earum illum consequatur voluptatem officiis dolorum exercitationem et.', NULL, NULL, 'Voluptatem eum quae et deleniti aperiam voluptatem facilis. Repudiandae qui ut assumenda voluptas. Quasi necessitatibus sunt cumque nulla eos expedita cum nulla.', 'en_US'),
(61, 61, 'BMW PACK2 118 d 143 CV', 'bmw-pack2-118-d-143-cv', 'Critères\r\nMarque\r\nBmw\r\nModèle\r\nSerie 1\r\nAnnée-modèle\r\n2012\r\nKilométrage\r\n174000 km\r\nCarburant\r\nDiesel\r\nBoîte de vitesse\r\nManuelle\r\nDescription\r\nBMW Série 1\r\n8 600', NULL, NULL, 'Critères\r\nMarque\r\nBmw\r\nModèle\r\nSerie 1\r\nAnnée-modèle\r\n2012\r\nKilométrage\r\n174000 km\r\nCarburant\r\nDiesel\r\nBoîte de vitesse\r\nManuelle\r\nDescription\r\nBMW Série 1\r\n8 600', 'en_US'),
(62, 62, 'Peugeot 508 (2) 1.6 bluehdi 120 s&s active busines', 'peugeot-508-2-1-6-bluehdi-120-s-s-active-busines', 'PEUGEOT 508\r\n(2) 1.6 BLUEHDI 120 S&S ACTIVE BUSINESS\r\n125 108 km compteur non garanti\r\nEmission de CO2 : 95g/km\r\nDiesel\r\nmécanique\r\n6CV - 15 / 02 / 2017 - 1ère main\r\nRéf. : ek591k6\r\nVéhicule garanti 3 mois\r\nPrix : 11 999 € *', NULL, NULL, 'PEUGEOT 508\r\n(2) 1.6 BLUEHDI 120 S&S ACTIVE BUSINESS\r\n125 108 km compteur non garanti\r\nEmission de CO2 : 95g/km\r\nDiesel\r\nmécanique\r\n6CV - 15 / 02 / 2017 - 1ère main\r\nRéf. : ek591k6\r\nVéhicule garanti 3 mois\r\nPrix : 11 999 € *', 'en_US'),
(63, 63, 'Mercedes SLK 200K Final Edition Excellent état', 'mercedes-slk-200k-final-edition-excellent-etat', 'Vends Mercedes SLK 200 Kompressor Final Edition Automatique, noire, jantes noires, excellent état.\r\n\r\n141.300 km peu évolutifs.\r\n\r\nExcellent état intérieur/extérieur, nombreux frais récents, sort de révision avec remplacement des rotules de suspension avant et réglage du parallélisme, CT ok du 12 juin 2019.\r\n\r\nPremière mise en circulation en janvier 2004, il s\'agit d\'un des tout derniers SLK R170 sortis d’usine.\r\nLa voiture est entièrement d’origine.', NULL, NULL, 'Vends Mercedes SLK 200 Kompressor Final Edition Automatique, noire, jantes noires, excellent état.\r\n\r\n141.300 km peu évolutifs.\r\n\r\nExcellent état intérieur/extérieur, nombreux frais récents, sort de révision avec remplacement des rotules de suspension avant et réglage du parallélisme, CT ok du 12 juin 2019.\r\n\r\nPremière mise en circulation en janvier 2004, il s\'agit d\'un des tout derniers SLK R170 sortis d’usine.\r\nLa voiture est entièrement d’origine.', 'en_US'),
(64, 64, 'Porsche 996 turbo 3,6l full options', 'porsche-996-turbo-3-6l-full-options', 'Exceptionnelle Porsche 911 - 996 Turbo 3,6L\r\nBoite Automatique Tiptronic\r\nFutur Collector\r\n\r\nCouleur Gris Kerguelen\r\n\r\nFULL OPTIONS:\r\n- Interieur Cartier rouge-orangé\r\n- Sièges électriques\r\n- Toit ouvrant\r\n- Sono Bose\r\n- GPS Alpine monté par Porsche\r\n- PSM\r\n- Alarme Optionnelle Anti Vol / CarJacking / Soulèvement avec Sonnerie + Warnings + Coupure moteur (= en cas de vol même avec les clefs, laissez partir, vous la retrouverez 1km plus loin.)\r\n...etc etc', NULL, NULL, 'Exceptionnelle Porsche 911 - 996 Turbo 3,6L\r\nBoite Automatique Tiptronic\r\nFutur Collector\r\n\r\nCouleur Gris Kerguelen\r\n\r\nFULL OPTIONS:\r\n- Interieur Cartier rouge-orangé\r\n- Sièges électriques\r\n- Toit ouvrant\r\n- Sono Bose\r\n- GPS Alpine monté par Porsche\r\n- PSM\r\n- Alarme Optionnelle Anti Vol / CarJacking / Soulèvement avec Sonnerie + Warnings + Coupure moteur (= en cas de vol même avec les clefs, laissez partir, vous la retrouverez 1km plus loin.)\r\n...etc etc', 'en_US');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_variant`
--

DROP TABLE IF EXISTS `sylius_product_variant`;
CREATE TABLE IF NOT EXISTS `sylius_product_variant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `tax_category_id` int(11) DEFAULT NULL,
  `shipping_category_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `position` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `on_hold` int(11) NOT NULL,
  `on_hand` int(11) NOT NULL,
  `tracked` tinyint(1) NOT NULL,
  `width` double DEFAULT NULL,
  `height` double DEFAULT NULL,
  `depth` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `shipping_required` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A29B52377153098` (`code`),
  KEY `IDX_A29B5234584665A` (`product_id`),
  KEY `IDX_A29B5239DF894ED` (`tax_category_id`),
  KEY `IDX_A29B5239E2D1A41` (`shipping_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=335 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_variant`
--

INSERT INTO `sylius_product_variant` (`id`, `product_id`, `tax_category_id`, `shipping_category_id`, `code`, `created_at`, `updated_at`, `position`, `version`, `on_hold`, `on_hand`, `tracked`, `width`, `height`, `depth`, `weight`, `shipping_required`) VALUES
(4, 2, NULL, NULL, '18f4dffe-cdb2-308f-ac6f-83878bc1a062-variant-0', '2019-06-21 20:47:51', '2019-06-21 20:47:51', 0, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(5, 2, NULL, NULL, '18f4dffe-cdb2-308f-ac6f-83878bc1a062-variant-1', '2019-06-21 20:47:51', '2019-06-21 20:47:51', 1, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(6, 2, NULL, NULL, '18f4dffe-cdb2-308f-ac6f-83878bc1a062-variant-2', '2019-06-21 20:47:51', '2019-06-21 20:47:51', 2, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(7, 3, NULL, NULL, 'bdbea748-3f59-3162-bb3e-ece47eca7daf-variant-0', '2019-06-21 20:47:51', '2019-06-21 20:47:51', 0, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(8, 3, NULL, NULL, 'bdbea748-3f59-3162-bb3e-ece47eca7daf-variant-1', '2019-06-21 20:47:51', '2019-06-21 20:47:51', 1, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(9, 3, NULL, NULL, 'bdbea748-3f59-3162-bb3e-ece47eca7daf-variant-2', '2019-06-21 20:47:51', '2019-06-21 20:47:51', 2, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(10, 4, NULL, NULL, '9200760b-ae9d-338c-9c30-d220768fc33d-variant-0', '2019-06-21 20:47:51', '2019-06-21 20:47:52', 0, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(11, 4, NULL, NULL, '9200760b-ae9d-338c-9c30-d220768fc33d-variant-1', '2019-06-21 20:47:51', '2019-06-21 20:47:52', 1, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(12, 4, NULL, NULL, '9200760b-ae9d-338c-9c30-d220768fc33d-variant-2', '2019-06-21 20:47:51', '2019-06-21 20:47:52', 2, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(13, 5, NULL, NULL, 'bf45361c-9c3c-3ca9-ada4-c7d284daabed-variant-0', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 0, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(14, 5, NULL, NULL, 'bf45361c-9c3c-3ca9-ada4-c7d284daabed-variant-1', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(15, 5, NULL, NULL, 'bf45361c-9c3c-3ca9-ada4-c7d284daabed-variant-2', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 2, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(16, 6, NULL, NULL, '8507e667-3ce6-3cff-9efb-481c92553141-variant-0', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(17, 6, NULL, NULL, '8507e667-3ce6-3cff-9efb-481c92553141-variant-1', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 1, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(18, 6, NULL, NULL, '8507e667-3ce6-3cff-9efb-481c92553141-variant-2', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 2, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(19, 7, NULL, NULL, '3b375da0-0844-3d2b-b6d0-1a23e8e37fa8-variant-0', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(20, 7, NULL, NULL, '3b375da0-0844-3d2b-b6d0-1a23e8e37fa8-variant-1', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 1, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(21, 7, NULL, NULL, '3b375da0-0844-3d2b-b6d0-1a23e8e37fa8-variant-2', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 2, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(22, 8, NULL, NULL, '1b202c66-4b6d-396d-9b33-c11e13bb2ff1-variant-0', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 0, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(23, 8, NULL, NULL, '1b202c66-4b6d-396d-9b33-c11e13bb2ff1-variant-1', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 1, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(24, 8, NULL, NULL, '1b202c66-4b6d-396d-9b33-c11e13bb2ff1-variant-2', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 2, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(25, 9, NULL, NULL, 'e853369a-2e46-32e0-8079-0f63aa838d74-variant-0', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 0, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(26, 9, NULL, NULL, 'e853369a-2e46-32e0-8079-0f63aa838d74-variant-1', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 1, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(27, 9, NULL, NULL, 'e853369a-2e46-32e0-8079-0f63aa838d74-variant-2', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 2, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(28, 10, NULL, NULL, '3b8bf6bf-619d-3d32-aa46-405fd34ceb7d-variant-0', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(29, 10, NULL, NULL, '3b8bf6bf-619d-3d32-aa46-405fd34ceb7d-variant-1', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(30, 10, NULL, NULL, '3b8bf6bf-619d-3d32-aa46-405fd34ceb7d-variant-2', '2019-06-21 20:47:52', '2019-06-21 20:47:52', 2, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(31, 11, NULL, NULL, '0bef49bd-90c1-36da-a544-c5813c470eb0-variant-0', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 0, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(32, 11, NULL, NULL, '0bef49bd-90c1-36da-a544-c5813c470eb0-variant-1', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 1, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(33, 11, NULL, NULL, '0bef49bd-90c1-36da-a544-c5813c470eb0-variant-2', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 2, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(34, 12, NULL, NULL, 'b06353c6-6ec6-3e9f-971d-419674bb6d3e-variant-0', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 0, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(35, 12, NULL, NULL, 'b06353c6-6ec6-3e9f-971d-419674bb6d3e-variant-1', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 1, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(36, 12, NULL, NULL, 'b06353c6-6ec6-3e9f-971d-419674bb6d3e-variant-2', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 2, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(37, 13, NULL, NULL, 'bca41952-a193-3d7d-9607-a6c0cf730e8b-variant-0', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 0, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(38, 13, NULL, NULL, 'bca41952-a193-3d7d-9607-a6c0cf730e8b-variant-1', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 1, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(39, 13, NULL, NULL, 'bca41952-a193-3d7d-9607-a6c0cf730e8b-variant-2', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 2, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(40, 14, NULL, NULL, '1bef578d-ce2b-30b6-b38c-534ab26991ee-variant-0', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 0, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(41, 14, NULL, NULL, '1bef578d-ce2b-30b6-b38c-534ab26991ee-variant-1', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 1, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(42, 14, NULL, NULL, '1bef578d-ce2b-30b6-b38c-534ab26991ee-variant-2', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 2, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(43, 15, NULL, NULL, 'e61ebc69-3197-3bb5-a80c-ae67a9f377ad-variant-0', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 0, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(44, 15, NULL, NULL, 'e61ebc69-3197-3bb5-a80c-ae67a9f377ad-variant-1', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 1, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(45, 15, NULL, NULL, 'e61ebc69-3197-3bb5-a80c-ae67a9f377ad-variant-2', '2019-06-21 20:47:53', '2019-06-21 20:47:53', 2, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(46, 16, NULL, NULL, '67a04451-51a7-3579-bc98-e9374c193034-variant-0', '2019-06-21 20:47:54', '2019-06-21 20:47:55', 0, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(47, 16, NULL, NULL, '67a04451-51a7-3579-bc98-e9374c193034-variant-1', '2019-06-21 20:47:54', '2019-06-21 20:47:55', 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(48, 16, NULL, NULL, '67a04451-51a7-3579-bc98-e9374c193034-variant-2', '2019-06-21 20:47:54', '2019-06-21 20:47:55', 2, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(49, 17, NULL, NULL, '72ea7df3-af00-3a27-987c-afabf6a1046f-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(50, 17, NULL, NULL, '72ea7df3-af00-3a27-987c-afabf6a1046f-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(51, 17, NULL, NULL, '72ea7df3-af00-3a27-987c-afabf6a1046f-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(52, 18, NULL, NULL, 'f22bb104-1f21-336e-beed-04e5b559012c-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(53, 18, NULL, NULL, 'f22bb104-1f21-336e-beed-04e5b559012c-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(54, 18, NULL, NULL, 'f22bb104-1f21-336e-beed-04e5b559012c-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(55, 19, NULL, NULL, 'a38ccc2b-5e04-3c19-a4f9-cc16f22a70bf-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(56, 19, NULL, NULL, 'a38ccc2b-5e04-3c19-a4f9-cc16f22a70bf-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(57, 19, NULL, NULL, 'a38ccc2b-5e04-3c19-a4f9-cc16f22a70bf-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(58, 20, NULL, NULL, '2af6b2f5-9c45-3495-940b-b94664981b3f-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(59, 20, NULL, NULL, '2af6b2f5-9c45-3495-940b-b94664981b3f-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(60, 20, NULL, NULL, '2af6b2f5-9c45-3495-940b-b94664981b3f-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(61, 21, NULL, NULL, '77565c18-5d7c-3689-b588-d869e7a780b9-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(62, 21, NULL, NULL, '77565c18-5d7c-3689-b588-d869e7a780b9-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(63, 21, NULL, NULL, '77565c18-5d7c-3689-b588-d869e7a780b9-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(64, 22, NULL, NULL, '0a86bd2a-f39b-304e-9a1d-1df98ae51b5a-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(65, 22, NULL, NULL, '0a86bd2a-f39b-304e-9a1d-1df98ae51b5a-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(66, 22, NULL, NULL, '0a86bd2a-f39b-304e-9a1d-1df98ae51b5a-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(67, 23, NULL, NULL, '9eecb9b2-7c08-3fbd-894f-685147062cf7-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(68, 23, NULL, NULL, '9eecb9b2-7c08-3fbd-894f-685147062cf7-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(69, 23, NULL, NULL, '9eecb9b2-7c08-3fbd-894f-685147062cf7-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(70, 24, NULL, NULL, '371714cf-e546-39af-96f4-1ff2f53cf7e2-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(71, 24, NULL, NULL, '371714cf-e546-39af-96f4-1ff2f53cf7e2-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(72, 24, NULL, NULL, '371714cf-e546-39af-96f4-1ff2f53cf7e2-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(73, 25, NULL, NULL, 'd3bb2c6a-db02-3b40-9721-cf3af92d65fe-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(74, 25, NULL, NULL, 'd3bb2c6a-db02-3b40-9721-cf3af92d65fe-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(75, 25, NULL, NULL, 'd3bb2c6a-db02-3b40-9721-cf3af92d65fe-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(76, 26, NULL, NULL, '87fa3ede-6769-30dc-b807-828977916204-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(77, 26, NULL, NULL, '87fa3ede-6769-30dc-b807-828977916204-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(78, 26, NULL, NULL, '87fa3ede-6769-30dc-b807-828977916204-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(79, 27, NULL, NULL, '8cdc99ad-8801-37ff-b137-4af9e006d731-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(80, 27, NULL, NULL, '8cdc99ad-8801-37ff-b137-4af9e006d731-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(81, 27, NULL, NULL, '8cdc99ad-8801-37ff-b137-4af9e006d731-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(82, 28, NULL, NULL, 'a545a0bb-bfdd-3eca-aae0-4eb48f9eb772-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(83, 28, NULL, NULL, 'a545a0bb-bfdd-3eca-aae0-4eb48f9eb772-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(84, 28, NULL, NULL, 'a545a0bb-bfdd-3eca-aae0-4eb48f9eb772-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(85, 29, NULL, NULL, 'b726df34-5ffd-33f9-a0d4-b422ab855089-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(86, 29, NULL, NULL, 'b726df34-5ffd-33f9-a0d4-b422ab855089-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(87, 29, NULL, NULL, 'b726df34-5ffd-33f9-a0d4-b422ab855089-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(88, 30, NULL, NULL, 'f383d495-9eb7-3d28-8f51-22f3b39ea81f-variant-0', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 0, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(89, 30, NULL, NULL, 'f383d495-9eb7-3d28-8f51-22f3b39ea81f-variant-1', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 1, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(90, 30, NULL, NULL, 'f383d495-9eb7-3d28-8f51-22f3b39ea81f-variant-2', '2019-06-21 20:47:55', '2019-06-21 20:47:55', 2, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(91, 31, NULL, NULL, '3549d8d8-406f-3688-ad75-d1c53f3d8999-variant-0', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 0, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(92, 32, NULL, NULL, 'b70e7bdb-59b7-33db-a53e-0ad14df239bf-variant-0', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 0, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(93, 33, NULL, NULL, 'b8277950-b5c1-3ead-a0ba-2165ee520f18-variant-0', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 0, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(94, 34, NULL, NULL, '065beed8-fd4f-3dc3-b3fd-3d8ab238f6b7-variant-0', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 0, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(95, 35, NULL, NULL, '1485879f-277c-358f-8001-0cc5802c2022-variant-0', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 0, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(96, 36, NULL, NULL, '462acf83-c96a-3d40-aab3-1f5d26c9e6c7-variant-0', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 0, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(97, 37, NULL, NULL, 'f672687e-92d5-3637-8dcb-a554e811af1d-variant-0', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 0, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(98, 38, NULL, NULL, 'a997746f-2fc9-3b22-8cce-71ad80937e12-variant-0', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(99, 39, NULL, NULL, '10821f4e-2226-34a1-88f7-d291ae7bfcc4-variant-0', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 0, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(100, 40, NULL, NULL, '1ad980b4-adcd-3988-bc0b-0e66818191eb-variant-0', '2019-06-21 20:47:56', '2019-06-21 20:47:56', 0, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(101, 41, NULL, NULL, '3a28413f-bd97-3cff-8d63-9f5b63fd23d8-variant-0', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 0, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(102, 42, NULL, NULL, 'df3dab06-c287-3fbc-9175-942fd8272c7c-variant-0', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 0, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(103, 43, NULL, NULL, 'f661adc9-4384-3c03-8092-8d1f92918274-variant-0', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 0, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(104, 44, NULL, NULL, '551bc608-1911-3d9d-923d-c908ec5c5894-variant-0', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 0, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(105, 45, NULL, NULL, '5ba4f7de-4e62-3554-a909-028183d6d444-variant-0', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 0, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(106, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-0', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 0, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(107, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-1', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 1, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(108, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-2', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 2, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(109, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-3', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 3, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(110, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-4', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 4, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(111, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-5', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 5, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(112, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-6', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 6, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(113, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-7', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 7, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(114, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-8', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 8, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(115, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-9', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 9, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(116, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-10', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 10, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(117, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-11', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 11, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(118, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-12', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 12, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(119, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-13', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 13, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(120, 46, NULL, NULL, '4dc1f1c0-bdce-3c03-a569-c7113dab0097-variant-14', '2019-06-21 20:47:57', '2019-06-21 20:47:57', 14, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(121, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-0', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 0, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(122, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-1', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 1, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(123, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-2', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 2, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(124, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-3', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 3, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(125, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-4', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 4, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(126, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-5', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 5, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(127, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-6', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 6, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(128, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-7', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 7, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(129, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-8', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 8, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(130, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-9', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 9, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(131, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-10', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 10, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(132, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-11', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 11, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(133, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-12', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 12, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(134, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-13', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 13, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(135, 47, NULL, NULL, '8ddfa8a3-7377-3f6f-94ab-73fdaccff472-variant-14', '2019-06-21 20:47:57', '2019-06-21 20:47:58', 14, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(136, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-0', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(137, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-1', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 1, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(138, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-2', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 2, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(139, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-3', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 3, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(140, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-4', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 4, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(141, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-5', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 5, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(142, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-6', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 6, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(143, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-7', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 7, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(144, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-8', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 8, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(145, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-9', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 9, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(146, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-10', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 10, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(147, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-11', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 11, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(148, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-12', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 12, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(149, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-13', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 13, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(150, 48, NULL, NULL, 'fc4353a0-e399-3501-902b-0fcf1db3c280-variant-14', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 14, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(151, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-0', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 0, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(152, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-1', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 1, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(153, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-2', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 2, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(154, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-3', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 3, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(155, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-4', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 4, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(156, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-5', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 5, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(157, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-6', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 6, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(158, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-7', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 7, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(159, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-8', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 8, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(160, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-9', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 9, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(161, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-10', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 10, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(162, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-11', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 11, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(163, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-12', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 12, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(164, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-13', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 13, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(165, 49, NULL, NULL, '648c848c-9f26-3575-8b1d-a5713d05243a-variant-14', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 14, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(166, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-0', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 0, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(167, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-1', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 1, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(168, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-2', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 2, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(169, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-3', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 3, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(170, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-4', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 4, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(171, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-5', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 5, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(172, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-6', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 6, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(173, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-7', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 7, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(174, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-8', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 8, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(175, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-9', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 9, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(176, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-10', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 10, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(177, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-11', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 11, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(178, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-12', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 12, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(179, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-13', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 13, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(180, 50, NULL, NULL, '61c0d973-7ce6-36d6-8e76-edebd75dfaa4-variant-14', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 14, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(181, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-0', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 0, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(182, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-1', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 1, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(183, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-2', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 2, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(184, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-3', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 3, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(185, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-4', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 4, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(186, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-5', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 5, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(187, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-6', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 6, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(188, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-7', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 7, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(189, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-8', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 8, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(190, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-9', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 9, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(191, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-10', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 10, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(192, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-11', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 11, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(193, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-12', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 12, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(194, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-13', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 13, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(195, 51, NULL, NULL, '8691c9b2-5b97-30e8-b785-eba2550b3add-variant-14', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 14, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(196, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-0', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 0, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(197, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-1', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 1, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(198, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-2', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 2, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(199, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-3', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 3, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(200, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-4', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 4, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(201, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-5', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 5, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(202, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-6', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 6, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(203, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-7', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 7, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(204, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-8', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 8, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(205, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-9', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 9, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(206, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-10', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 10, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(207, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-11', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 11, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(208, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-12', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 12, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(209, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-13', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 13, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(210, 52, NULL, NULL, 'e5b66509-2034-3a4a-8389-765b00260a2f-variant-14', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 14, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(211, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-0', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 0, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(212, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-1', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 1, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(213, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-2', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 2, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(214, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-3', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 3, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(215, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-4', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 4, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(216, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-5', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 5, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(217, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-6', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 6, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(218, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-7', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 7, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(219, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-8', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 8, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(220, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-9', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 9, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(221, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-10', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 10, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(222, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-11', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 11, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(223, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-12', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 12, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(224, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-13', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 13, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(225, 53, NULL, NULL, 'f10e11ce-d6e6-3a0e-bb1c-69bc58fd885d-variant-14', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 14, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(226, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-0', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 0, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(227, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-1', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 1, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(228, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-2', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 2, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(229, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-3', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 3, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(230, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-4', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 4, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(231, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-5', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 5, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(232, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-6', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 6, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(233, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-7', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 7, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(234, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-8', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 8, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(235, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-9', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 9, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(236, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-10', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 10, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(237, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-11', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 11, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(238, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-12', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 12, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(239, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-13', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 13, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(240, 54, NULL, NULL, '83dfbc83-b91c-3c30-8d8d-3406370f1e63-variant-14', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 14, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(241, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-0', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 0, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(242, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-1', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 1, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(243, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-2', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 2, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(244, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-3', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 3, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(245, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-4', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 4, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(246, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-5', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 5, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(247, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-6', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 6, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(248, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-7', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 7, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(249, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-8', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 8, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(250, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-9', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 9, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(251, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-10', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 10, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(252, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-11', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 11, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(253, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-12', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 12, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(254, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-13', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 13, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(255, 55, NULL, NULL, 'b7fa4dc6-9a9b-3e43-bb47-9cb1f81fb59c-variant-14', '2019-06-21 20:47:58', '2019-06-21 20:47:58', 14, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(256, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-0', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(257, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-1', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(258, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-2', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 2, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(259, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-3', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 3, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(260, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-4', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 4, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(261, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-5', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 5, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(262, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-6', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 6, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(263, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-7', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 7, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(264, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-8', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 8, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(265, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-9', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 9, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(266, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-10', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 10, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(267, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-11', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 11, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(268, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-12', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 12, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(269, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-13', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 13, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(270, 56, NULL, NULL, '5a9c0476-9be6-3025-899d-ce8dec3700ef-variant-14', '2019-06-21 20:47:59', '2019-06-21 20:48:00', 14, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(271, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-0', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 0, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(272, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-1', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 1, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(273, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-2', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 2, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(274, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-3', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 3, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(275, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-4', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 4, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(276, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-5', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 5, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(277, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-6', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 6, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(278, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-7', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 7, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(279, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-8', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 8, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(280, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-9', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 9, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(281, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-10', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 10, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(282, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-11', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 11, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(283, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-12', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 12, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(284, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-13', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 13, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(285, 57, NULL, NULL, '4d874db4-9b9f-3167-8568-88cec184e3fc-variant-14', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 14, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(286, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-0', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 0, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(287, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-1', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 1, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(288, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-2', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 2, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(289, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-3', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 3, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(290, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-4', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 4, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(291, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-5', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 5, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(292, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-6', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 6, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(293, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-7', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 7, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(294, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-8', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 8, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(295, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-9', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 9, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(296, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-10', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 10, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(297, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-11', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 11, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(298, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-12', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 12, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(299, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-13', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 13, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(300, 58, NULL, NULL, '4edf46d6-c08f-3ba0-a2d3-5c11407ba3ac-variant-14', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 14, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(301, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-0', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 0, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(302, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-1', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(303, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-2', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 2, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(304, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-3', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 3, 1, 0, 6, 0, NULL, NULL, NULL, NULL, 1),
(305, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-4', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 4, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(306, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-5', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 5, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(307, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-6', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 6, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(308, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-7', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 7, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(309, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-8', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 8, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(310, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-9', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 9, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(311, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-10', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 10, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(312, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-11', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 11, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(313, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-12', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 12, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(314, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-13', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 13, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(315, 59, NULL, NULL, 'd8491ca5-75c3-3f70-8eb2-86c055c6405b-variant-14', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 14, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `sylius_product_variant` (`id`, `product_id`, `tax_category_id`, `shipping_category_id`, `code`, `created_at`, `updated_at`, `position`, `version`, `on_hold`, `on_hand`, `tracked`, `width`, `height`, `depth`, `weight`, `shipping_required`) VALUES
(316, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-0', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 0, 1, 0, 4, 0, NULL, NULL, NULL, NULL, 1),
(317, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-1', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 1, 1, 0, 9, 0, NULL, NULL, NULL, NULL, 1),
(318, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-2', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 2, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(319, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-3', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 3, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(320, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-4', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 4, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(321, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-5', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 5, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(322, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-6', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 6, 1, 0, 5, 0, NULL, NULL, NULL, NULL, 1),
(323, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-7', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 7, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(324, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-8', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 8, 1, 0, 7, 0, NULL, NULL, NULL, NULL, 1),
(325, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-9', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 9, 1, 0, 8, 0, NULL, NULL, NULL, NULL, 1),
(326, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-10', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 10, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(327, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-11', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 11, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(328, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-12', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 12, 1, 0, 2, 0, NULL, NULL, NULL, NULL, 1),
(329, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-13', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 13, 1, 0, 3, 0, NULL, NULL, NULL, NULL, 1),
(330, 60, NULL, NULL, '9184b4e2-8af0-309d-9bd9-6795eb0fd4f8-variant-14', '2019-06-21 20:48:00', '2019-06-21 20:48:00', 14, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 1),
(331, 61, NULL, NULL, '0000000', '2019-06-23 12:22:50', '2019-06-23 12:23:18', 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(332, 62, NULL, NULL, 'P076537', '2019-06-23 22:02:02', '2019-06-23 22:02:09', 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1),
(333, 63, NULL, NULL, 'P8766565', '2019-06-23 22:04:25', '2019-06-23 22:04:32', 0, 1, 0, 10, 0, NULL, NULL, NULL, NULL, 1),
(334, 64, NULL, NULL, 'P9875775', '2019-06-23 22:12:43', '2019-06-23 22:12:51', 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_variant_option_value`
--

DROP TABLE IF EXISTS `sylius_product_variant_option_value`;
CREATE TABLE IF NOT EXISTS `sylius_product_variant_option_value` (
  `variant_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  PRIMARY KEY (`variant_id`,`option_value_id`),
  KEY `IDX_76CDAFA13B69A9AF` (`variant_id`),
  KEY `IDX_76CDAFA1D957CA06` (`option_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_variant_option_value`
--

INSERT INTO `sylius_product_variant_option_value` (`variant_id`, `option_value_id`) VALUES
(4, 1),
(5, 2),
(6, 3),
(7, 1),
(8, 2),
(9, 3),
(10, 1),
(11, 2),
(12, 3),
(13, 1),
(14, 2),
(15, 3),
(16, 1),
(17, 2),
(18, 3),
(19, 1),
(20, 2),
(21, 3),
(22, 1),
(23, 2),
(24, 3),
(25, 1),
(26, 2),
(27, 3),
(28, 1),
(29, 2),
(30, 3),
(31, 1),
(32, 2),
(33, 3),
(34, 1),
(35, 2),
(36, 3),
(37, 1),
(38, 2),
(39, 3),
(40, 1),
(41, 2),
(42, 3),
(43, 1),
(44, 2),
(45, 3),
(46, 4),
(47, 5),
(48, 6),
(49, 4),
(50, 5),
(51, 6),
(52, 4),
(53, 5),
(54, 6),
(55, 4),
(56, 5),
(57, 6),
(58, 4),
(59, 5),
(60, 6),
(61, 4),
(62, 5),
(63, 6),
(64, 4),
(65, 5),
(66, 6),
(67, 4),
(68, 5),
(69, 6),
(70, 4),
(71, 5),
(72, 6),
(73, 4),
(74, 5),
(75, 6),
(76, 4),
(77, 5),
(78, 6),
(79, 4),
(80, 5),
(81, 6),
(82, 4),
(83, 5),
(84, 6),
(85, 4),
(86, 5),
(87, 6),
(88, 4),
(89, 5),
(90, 6),
(106, 7),
(106, 10),
(107, 7),
(107, 11),
(108, 7),
(108, 12),
(109, 7),
(109, 13),
(110, 7),
(110, 14),
(111, 8),
(111, 10),
(112, 8),
(112, 11),
(113, 8),
(113, 12),
(114, 8),
(114, 13),
(115, 8),
(115, 14),
(116, 9),
(116, 10),
(117, 9),
(117, 11),
(118, 9),
(118, 12),
(119, 9),
(119, 13),
(120, 9),
(120, 14),
(121, 7),
(121, 10),
(122, 7),
(122, 11),
(123, 7),
(123, 12),
(124, 7),
(124, 13),
(125, 7),
(125, 14),
(126, 8),
(126, 10),
(127, 8),
(127, 11),
(128, 8),
(128, 12),
(129, 8),
(129, 13),
(130, 8),
(130, 14),
(131, 9),
(131, 10),
(132, 9),
(132, 11),
(133, 9),
(133, 12),
(134, 9),
(134, 13),
(135, 9),
(135, 14),
(136, 7),
(136, 10),
(137, 7),
(137, 11),
(138, 7),
(138, 12),
(139, 7),
(139, 13),
(140, 7),
(140, 14),
(141, 8),
(141, 10),
(142, 8),
(142, 11),
(143, 8),
(143, 12),
(144, 8),
(144, 13),
(145, 8),
(145, 14),
(146, 9),
(146, 10),
(147, 9),
(147, 11),
(148, 9),
(148, 12),
(149, 9),
(149, 13),
(150, 9),
(150, 14),
(151, 7),
(151, 10),
(152, 7),
(152, 11),
(153, 7),
(153, 12),
(154, 7),
(154, 13),
(155, 7),
(155, 14),
(156, 8),
(156, 10),
(157, 8),
(157, 11),
(158, 8),
(158, 12),
(159, 8),
(159, 13),
(160, 8),
(160, 14),
(161, 9),
(161, 10),
(162, 9),
(162, 11),
(163, 9),
(163, 12),
(164, 9),
(164, 13),
(165, 9),
(165, 14),
(166, 7),
(166, 10),
(167, 7),
(167, 11),
(168, 7),
(168, 12),
(169, 7),
(169, 13),
(170, 7),
(170, 14),
(171, 8),
(171, 10),
(172, 8),
(172, 11),
(173, 8),
(173, 12),
(174, 8),
(174, 13),
(175, 8),
(175, 14),
(176, 9),
(176, 10),
(177, 9),
(177, 11),
(178, 9),
(178, 12),
(179, 9),
(179, 13),
(180, 9),
(180, 14),
(181, 7),
(181, 10),
(182, 7),
(182, 11),
(183, 7),
(183, 12),
(184, 7),
(184, 13),
(185, 7),
(185, 14),
(186, 8),
(186, 10),
(187, 8),
(187, 11),
(188, 8),
(188, 12),
(189, 8),
(189, 13),
(190, 8),
(190, 14),
(191, 9),
(191, 10),
(192, 9),
(192, 11),
(193, 9),
(193, 12),
(194, 9),
(194, 13),
(195, 9),
(195, 14),
(196, 7),
(196, 10),
(197, 7),
(197, 11),
(198, 7),
(198, 12),
(199, 7),
(199, 13),
(200, 7),
(200, 14),
(201, 8),
(201, 10),
(202, 8),
(202, 11),
(203, 8),
(203, 12),
(204, 8),
(204, 13),
(205, 8),
(205, 14),
(206, 9),
(206, 10),
(207, 9),
(207, 11),
(208, 9),
(208, 12),
(209, 9),
(209, 13),
(210, 9),
(210, 14),
(211, 7),
(211, 10),
(212, 7),
(212, 11),
(213, 7),
(213, 12),
(214, 7),
(214, 13),
(215, 7),
(215, 14),
(216, 8),
(216, 10),
(217, 8),
(217, 11),
(218, 8),
(218, 12),
(219, 8),
(219, 13),
(220, 8),
(220, 14),
(221, 9),
(221, 10),
(222, 9),
(222, 11),
(223, 9),
(223, 12),
(224, 9),
(224, 13),
(225, 9),
(225, 14),
(226, 7),
(226, 10),
(227, 7),
(227, 11),
(228, 7),
(228, 12),
(229, 7),
(229, 13),
(230, 7),
(230, 14),
(231, 8),
(231, 10),
(232, 8),
(232, 11),
(233, 8),
(233, 12),
(234, 8),
(234, 13),
(235, 8),
(235, 14),
(236, 9),
(236, 10),
(237, 9),
(237, 11),
(238, 9),
(238, 12),
(239, 9),
(239, 13),
(240, 9),
(240, 14),
(241, 7),
(241, 10),
(242, 7),
(242, 11),
(243, 7),
(243, 12),
(244, 7),
(244, 13),
(245, 7),
(245, 14),
(246, 8),
(246, 10),
(247, 8),
(247, 11),
(248, 8),
(248, 12),
(249, 8),
(249, 13),
(250, 8),
(250, 14),
(251, 9),
(251, 10),
(252, 9),
(252, 11),
(253, 9),
(253, 12),
(254, 9),
(254, 13),
(255, 9),
(255, 14),
(256, 7),
(256, 10),
(257, 7),
(257, 11),
(258, 7),
(258, 12),
(259, 7),
(259, 13),
(260, 7),
(260, 14),
(261, 8),
(261, 10),
(262, 8),
(262, 11),
(263, 8),
(263, 12),
(264, 8),
(264, 13),
(265, 8),
(265, 14),
(266, 9),
(266, 10),
(267, 9),
(267, 11),
(268, 9),
(268, 12),
(269, 9),
(269, 13),
(270, 9),
(270, 14),
(271, 7),
(271, 10),
(272, 7),
(272, 11),
(273, 7),
(273, 12),
(274, 7),
(274, 13),
(275, 7),
(275, 14),
(276, 8),
(276, 10),
(277, 8),
(277, 11),
(278, 8),
(278, 12),
(279, 8),
(279, 13),
(280, 8),
(280, 14),
(281, 9),
(281, 10),
(282, 9),
(282, 11),
(283, 9),
(283, 12),
(284, 9),
(284, 13),
(285, 9),
(285, 14),
(286, 7),
(286, 10),
(287, 7),
(287, 11),
(288, 7),
(288, 12),
(289, 7),
(289, 13),
(290, 7),
(290, 14),
(291, 8),
(291, 10),
(292, 8),
(292, 11),
(293, 8),
(293, 12),
(294, 8),
(294, 13),
(295, 8),
(295, 14),
(296, 9),
(296, 10),
(297, 9),
(297, 11),
(298, 9),
(298, 12),
(299, 9),
(299, 13),
(300, 9),
(300, 14),
(301, 7),
(301, 10),
(302, 7),
(302, 11),
(303, 7),
(303, 12),
(304, 7),
(304, 13),
(305, 7),
(305, 14),
(306, 8),
(306, 10),
(307, 8),
(307, 11),
(308, 8),
(308, 12),
(309, 8),
(309, 13),
(310, 8),
(310, 14),
(311, 9),
(311, 10),
(312, 9),
(312, 11),
(313, 9),
(313, 12),
(314, 9),
(314, 13),
(315, 9),
(315, 14),
(316, 7),
(316, 10),
(317, 7),
(317, 11),
(318, 7),
(318, 12),
(319, 7),
(319, 13),
(320, 7),
(320, 14),
(321, 8),
(321, 10),
(322, 8),
(322, 11),
(323, 8),
(323, 12),
(324, 8),
(324, 13),
(325, 8),
(325, 14),
(326, 9),
(326, 10),
(327, 9),
(327, 11),
(328, 9),
(328, 12),
(329, 9),
(329, 13),
(330, 9),
(330, 14);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_product_variant_translation`
--

DROP TABLE IF EXISTS `sylius_product_variant_translation`;
CREATE TABLE IF NOT EXISTS `sylius_product_variant_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_product_variant_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_8DC18EDC2C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=335 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_product_variant_translation`
--

INSERT INTO `sylius_product_variant_translation` (`id`, `translatable_id`, `name`, `locale`) VALUES
(4, 4, 'sed', 'en_US'),
(5, 5, 'porro', 'en_US'),
(6, 6, 'ipsa', 'en_US'),
(7, 7, 'harum', 'en_US'),
(8, 8, 'doloremque', 'en_US'),
(9, 9, 'voluptates', 'en_US'),
(10, 10, 'illum', 'en_US'),
(11, 11, 'enim', 'en_US'),
(12, 12, 'ipsa', 'en_US'),
(13, 13, 'quia', 'en_US'),
(14, 14, 'debitis', 'en_US'),
(15, 15, 'id', 'en_US'),
(16, 16, 'atque', 'en_US'),
(17, 17, 'quo', 'en_US'),
(18, 18, 'et', 'en_US'),
(19, 19, 'doloribus', 'en_US'),
(20, 20, 'fuga', 'en_US'),
(21, 21, 'recusandae', 'en_US'),
(22, 22, 'non', 'en_US'),
(23, 23, 'occaecati', 'en_US'),
(24, 24, 'mollitia', 'en_US'),
(25, 25, 'similique', 'en_US'),
(26, 26, 'officiis', 'en_US'),
(27, 27, 'ipsa', 'en_US'),
(28, 28, 'aperiam', 'en_US'),
(29, 29, 'dignissimos', 'en_US'),
(30, 30, 'voluptas', 'en_US'),
(31, 31, 'provident', 'en_US'),
(32, 32, 'et', 'en_US'),
(33, 33, 'et', 'en_US'),
(34, 34, 'explicabo', 'en_US'),
(35, 35, 'sit', 'en_US'),
(36, 36, 'excepturi', 'en_US'),
(37, 37, 'magnam', 'en_US'),
(38, 38, 'deserunt', 'en_US'),
(39, 39, 'et', 'en_US'),
(40, 40, 'et', 'en_US'),
(41, 41, 'debitis', 'en_US'),
(42, 42, 'fugiat', 'en_US'),
(43, 43, 'consequuntur', 'en_US'),
(44, 44, 'magni', 'en_US'),
(45, 45, 'in', 'en_US'),
(46, 46, 'aut', 'en_US'),
(47, 47, 'amet', 'en_US'),
(48, 48, 'accusantium', 'en_US'),
(49, 49, 'eos', 'en_US'),
(50, 50, 'molestiae', 'en_US'),
(51, 51, 'maiores', 'en_US'),
(52, 52, 'minus', 'en_US'),
(53, 53, 'laborum', 'en_US'),
(54, 54, 'ipsam', 'en_US'),
(55, 55, 'ut', 'en_US'),
(56, 56, 'quis', 'en_US'),
(57, 57, 'nulla', 'en_US'),
(58, 58, 'quibusdam', 'en_US'),
(59, 59, 'minima', 'en_US'),
(60, 60, 'recusandae', 'en_US'),
(61, 61, 'aliquid', 'en_US'),
(62, 62, 'modi', 'en_US'),
(63, 63, 'aut', 'en_US'),
(64, 64, 'eos', 'en_US'),
(65, 65, 'pariatur', 'en_US'),
(66, 66, 'illum', 'en_US'),
(67, 67, 'sunt', 'en_US'),
(68, 68, 'placeat', 'en_US'),
(69, 69, 'officia', 'en_US'),
(70, 70, 'quidem', 'en_US'),
(71, 71, 'similique', 'en_US'),
(72, 72, 'quia', 'en_US'),
(73, 73, 'eius', 'en_US'),
(74, 74, 'dolore', 'en_US'),
(75, 75, 'velit', 'en_US'),
(76, 76, 'ratione', 'en_US'),
(77, 77, 'architecto', 'en_US'),
(78, 78, 'sapiente', 'en_US'),
(79, 79, 'aut', 'en_US'),
(80, 80, 'libero', 'en_US'),
(81, 81, 'sit', 'en_US'),
(82, 82, 'est', 'en_US'),
(83, 83, 'consectetur', 'en_US'),
(84, 84, 'libero', 'en_US'),
(85, 85, 'esse', 'en_US'),
(86, 86, 'sed', 'en_US'),
(87, 87, 'facilis', 'en_US'),
(88, 88, 'at', 'en_US'),
(89, 89, 'et', 'en_US'),
(90, 90, 'quis', 'en_US'),
(91, 91, 'ut', 'en_US'),
(92, 92, 'unde', 'en_US'),
(93, 93, 'veniam', 'en_US'),
(94, 94, 'aut', 'en_US'),
(95, 95, 'non', 'en_US'),
(96, 96, 'occaecati', 'en_US'),
(97, 97, 'ea', 'en_US'),
(98, 98, 'culpa', 'en_US'),
(99, 99, 'doloremque', 'en_US'),
(100, 100, 'facere', 'en_US'),
(101, 101, 'beatae', 'en_US'),
(102, 102, 'consequatur', 'en_US'),
(103, 103, 'perferendis', 'en_US'),
(104, 104, 'sapiente', 'en_US'),
(105, 105, 'vel', 'en_US'),
(106, 106, 'nemo', 'en_US'),
(107, 107, 'aperiam', 'en_US'),
(108, 108, 'consequatur', 'en_US'),
(109, 109, 'quia', 'en_US'),
(110, 110, 'nostrum', 'en_US'),
(111, 111, 'aut', 'en_US'),
(112, 112, 'vero', 'en_US'),
(113, 113, 'dicta', 'en_US'),
(114, 114, 'aut', 'en_US'),
(115, 115, 'qui', 'en_US'),
(116, 116, 'consequatur', 'en_US'),
(117, 117, 'quod', 'en_US'),
(118, 118, 'iusto', 'en_US'),
(119, 119, 'possimus', 'en_US'),
(120, 120, 'quia', 'en_US'),
(121, 121, 'nobis', 'en_US'),
(122, 122, 'sit', 'en_US'),
(123, 123, 'minima', 'en_US'),
(124, 124, 'sed', 'en_US'),
(125, 125, 'vero', 'en_US'),
(126, 126, 'est', 'en_US'),
(127, 127, 'ducimus', 'en_US'),
(128, 128, 'ratione', 'en_US'),
(129, 129, 'cumque', 'en_US'),
(130, 130, 'iusto', 'en_US'),
(131, 131, 'autem', 'en_US'),
(132, 132, 'accusamus', 'en_US'),
(133, 133, 'officiis', 'en_US'),
(134, 134, 'eum', 'en_US'),
(135, 135, 'et', 'en_US'),
(136, 136, 'modi', 'en_US'),
(137, 137, 'atque', 'en_US'),
(138, 138, 'dolore', 'en_US'),
(139, 139, 'veritatis', 'en_US'),
(140, 140, 'sint', 'en_US'),
(141, 141, 'voluptatibus', 'en_US'),
(142, 142, 'eligendi', 'en_US'),
(143, 143, 'minima', 'en_US'),
(144, 144, 'sit', 'en_US'),
(145, 145, 'porro', 'en_US'),
(146, 146, 'et', 'en_US'),
(147, 147, 'officiis', 'en_US'),
(148, 148, 'et', 'en_US'),
(149, 149, 'et', 'en_US'),
(150, 150, 'non', 'en_US'),
(151, 151, 'quaerat', 'en_US'),
(152, 152, 'perspiciatis', 'en_US'),
(153, 153, 'consequatur', 'en_US'),
(154, 154, 'pariatur', 'en_US'),
(155, 155, 'vero', 'en_US'),
(156, 156, 'in', 'en_US'),
(157, 157, 'sit', 'en_US'),
(158, 158, 'qui', 'en_US'),
(159, 159, 'id', 'en_US'),
(160, 160, 'ipsum', 'en_US'),
(161, 161, 'nesciunt', 'en_US'),
(162, 162, 'repudiandae', 'en_US'),
(163, 163, 'nobis', 'en_US'),
(164, 164, 'maxime', 'en_US'),
(165, 165, 'porro', 'en_US'),
(166, 166, 'cum', 'en_US'),
(167, 167, 'velit', 'en_US'),
(168, 168, 'dolorem', 'en_US'),
(169, 169, 'accusamus', 'en_US'),
(170, 170, 'sit', 'en_US'),
(171, 171, 'consequatur', 'en_US'),
(172, 172, 'quae', 'en_US'),
(173, 173, 'rerum', 'en_US'),
(174, 174, 'quis', 'en_US'),
(175, 175, 'optio', 'en_US'),
(176, 176, 'quia', 'en_US'),
(177, 177, 'non', 'en_US'),
(178, 178, 'quae', 'en_US'),
(179, 179, 'ut', 'en_US'),
(180, 180, 'facere', 'en_US'),
(181, 181, 'vero', 'en_US'),
(182, 182, 'facere', 'en_US'),
(183, 183, 'magni', 'en_US'),
(184, 184, 'quae', 'en_US'),
(185, 185, 'illo', 'en_US'),
(186, 186, 'consequuntur', 'en_US'),
(187, 187, 'minus', 'en_US'),
(188, 188, 'ad', 'en_US'),
(189, 189, 'vel', 'en_US'),
(190, 190, 'dolor', 'en_US'),
(191, 191, 'laborum', 'en_US'),
(192, 192, 'eveniet', 'en_US'),
(193, 193, 'sunt', 'en_US'),
(194, 194, 'ut', 'en_US'),
(195, 195, 'placeat', 'en_US'),
(196, 196, 'laboriosam', 'en_US'),
(197, 197, 'laudantium', 'en_US'),
(198, 198, 'repellat', 'en_US'),
(199, 199, 'ea', 'en_US'),
(200, 200, 'doloremque', 'en_US'),
(201, 201, 'voluptatem', 'en_US'),
(202, 202, 'quia', 'en_US'),
(203, 203, 'expedita', 'en_US'),
(204, 204, 'et', 'en_US'),
(205, 205, 'earum', 'en_US'),
(206, 206, 'et', 'en_US'),
(207, 207, 'natus', 'en_US'),
(208, 208, 'aut', 'en_US'),
(209, 209, 'odit', 'en_US'),
(210, 210, 'molestias', 'en_US'),
(211, 211, 'asperiores', 'en_US'),
(212, 212, 'nobis', 'en_US'),
(213, 213, 'voluptates', 'en_US'),
(214, 214, 'voluptatem', 'en_US'),
(215, 215, 'et', 'en_US'),
(216, 216, 'blanditiis', 'en_US'),
(217, 217, 'est', 'en_US'),
(218, 218, 'mollitia', 'en_US'),
(219, 219, 'rerum', 'en_US'),
(220, 220, 'asperiores', 'en_US'),
(221, 221, 'ipsum', 'en_US'),
(222, 222, 'et', 'en_US'),
(223, 223, 'dicta', 'en_US'),
(224, 224, 'laborum', 'en_US'),
(225, 225, 'quasi', 'en_US'),
(226, 226, 'quia', 'en_US'),
(227, 227, 'explicabo', 'en_US'),
(228, 228, 'debitis', 'en_US'),
(229, 229, 'quis', 'en_US'),
(230, 230, 'velit', 'en_US'),
(231, 231, 'molestias', 'en_US'),
(232, 232, 'accusantium', 'en_US'),
(233, 233, 'voluptatibus', 'en_US'),
(234, 234, 'officiis', 'en_US'),
(235, 235, 'aliquam', 'en_US'),
(236, 236, 'architecto', 'en_US'),
(237, 237, 'non', 'en_US'),
(238, 238, 'debitis', 'en_US'),
(239, 239, 'earum', 'en_US'),
(240, 240, 'aut', 'en_US'),
(241, 241, 'aut', 'en_US'),
(242, 242, 'officiis', 'en_US'),
(243, 243, 'vitae', 'en_US'),
(244, 244, 'doloribus', 'en_US'),
(245, 245, 'maxime', 'en_US'),
(246, 246, 'minima', 'en_US'),
(247, 247, 'nostrum', 'en_US'),
(248, 248, 'fugiat', 'en_US'),
(249, 249, 'voluptas', 'en_US'),
(250, 250, 'sit', 'en_US'),
(251, 251, 'repudiandae', 'en_US'),
(252, 252, 'doloremque', 'en_US'),
(253, 253, 'cum', 'en_US'),
(254, 254, 'soluta', 'en_US'),
(255, 255, 'odio', 'en_US'),
(256, 256, 'maxime', 'en_US'),
(257, 257, 'totam', 'en_US'),
(258, 258, 'pariatur', 'en_US'),
(259, 259, 'quod', 'en_US'),
(260, 260, 'enim', 'en_US'),
(261, 261, 'quam', 'en_US'),
(262, 262, 'quo', 'en_US'),
(263, 263, 'cumque', 'en_US'),
(264, 264, 'dolorum', 'en_US'),
(265, 265, 'ut', 'en_US'),
(266, 266, 'culpa', 'en_US'),
(267, 267, 'eligendi', 'en_US'),
(268, 268, 'praesentium', 'en_US'),
(269, 269, 'doloremque', 'en_US'),
(270, 270, 'iste', 'en_US'),
(271, 271, 'quia', 'en_US'),
(272, 272, 'voluptatem', 'en_US'),
(273, 273, 'est', 'en_US'),
(274, 274, 'magni', 'en_US'),
(275, 275, 'repellendus', 'en_US'),
(276, 276, 'a', 'en_US'),
(277, 277, 'corporis', 'en_US'),
(278, 278, 'dolores', 'en_US'),
(279, 279, 'enim', 'en_US'),
(280, 280, 'sint', 'en_US'),
(281, 281, 'et', 'en_US'),
(282, 282, 'est', 'en_US'),
(283, 283, 'delectus', 'en_US'),
(284, 284, 'a', 'en_US'),
(285, 285, 'iure', 'en_US'),
(286, 286, 'praesentium', 'en_US'),
(287, 287, 'ipsa', 'en_US'),
(288, 288, 'nesciunt', 'en_US'),
(289, 289, 'consequuntur', 'en_US'),
(290, 290, 'iste', 'en_US'),
(291, 291, 'rerum', 'en_US'),
(292, 292, 'iure', 'en_US'),
(293, 293, 'omnis', 'en_US'),
(294, 294, 'veritatis', 'en_US'),
(295, 295, 'delectus', 'en_US'),
(296, 296, 'eos', 'en_US'),
(297, 297, 'dolorem', 'en_US'),
(298, 298, 'eum', 'en_US'),
(299, 299, 'similique', 'en_US'),
(300, 300, 'deleniti', 'en_US'),
(301, 301, 'fugiat', 'en_US'),
(302, 302, 'ipsa', 'en_US'),
(303, 303, 'non', 'en_US'),
(304, 304, 'et', 'en_US'),
(305, 305, 'mollitia', 'en_US'),
(306, 306, 'reprehenderit', 'en_US'),
(307, 307, 'id', 'en_US'),
(308, 308, 'velit', 'en_US'),
(309, 309, 'eligendi', 'en_US'),
(310, 310, 'quia', 'en_US'),
(311, 311, 'aliquid', 'en_US'),
(312, 312, 'id', 'en_US'),
(313, 313, 'id', 'en_US'),
(314, 314, 'voluptate', 'en_US'),
(315, 315, 'quasi', 'en_US'),
(316, 316, 'et', 'en_US'),
(317, 317, 'ex', 'en_US'),
(318, 318, 'odit', 'en_US'),
(319, 319, 'vel', 'en_US'),
(320, 320, 'sed', 'en_US'),
(321, 321, 'eos', 'en_US'),
(322, 322, 'dolores', 'en_US'),
(323, 323, 'sit', 'en_US'),
(324, 324, 'aspernatur', 'en_US'),
(325, 325, 'nihil', 'en_US'),
(326, 326, 'sed', 'en_US'),
(327, 327, 'vitae', 'en_US'),
(328, 328, 'harum', 'en_US'),
(329, 329, 'officiis', 'en_US'),
(330, 330, 'odit', 'en_US'),
(331, 331, NULL, 'en_US'),
(332, 332, NULL, 'en_US'),
(333, 333, NULL, 'en_US'),
(334, 334, NULL, 'en_US');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_promotion`
--

DROP TABLE IF EXISTS `sylius_promotion`;
CREATE TABLE IF NOT EXISTS `sylius_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `exclusive` tinyint(1) NOT NULL,
  `usage_limit` int(11) DEFAULT NULL,
  `used` int(11) NOT NULL,
  `coupon_based` tinyint(1) NOT NULL,
  `starts_at` datetime DEFAULT NULL,
  `ends_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F157396377153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_promotion`
--

INSERT INTO `sylius_promotion` (`id`, `code`, `name`, `description`, `priority`, `exclusive`, `usage_limit`, `used`, `coupon_based`, `starts_at`, `ends_at`, `created_at`, `updated_at`) VALUES
(1, 'christmas', 'Christmas', 'Voluptatibus rem qui totam eaque incidunt error.', 0, 0, NULL, 20, 0, NULL, NULL, '2019-06-21 20:47:50', '2019-06-21 20:48:08'),
(2, 'new_year', 'New Year', 'Voluptatibus rem qui totam eaque incidunt error.', 2, 0, 10, 1, 0, '2019-06-14 20:47:50', '2019-06-28 20:47:50', '2019-06-21 20:47:50', '2019-06-21 20:48:08');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_promotion_action`
--

DROP TABLE IF EXISTS `sylius_promotion_action`;
CREATE TABLE IF NOT EXISTS `sylius_promotion_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configuration` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  KEY `IDX_933D0915139DF194` (`promotion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_promotion_action`
--

INSERT INTO `sylius_promotion_action` (`id`, `promotion_id`, `type`, `configuration`) VALUES
(1, 1, 'order_percentage_discount', 'a:1:{s:10:\"percentage\";d:0.85;}'),
(2, 2, 'order_fixed_discount', 'a:1:{s:6:\"US_WEB\";a:1:{s:6:\"amount\";i:1000;}}');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_promotion_channels`
--

DROP TABLE IF EXISTS `sylius_promotion_channels`;
CREATE TABLE IF NOT EXISTS `sylius_promotion_channels` (
  `promotion_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY (`promotion_id`,`channel_id`),
  KEY `IDX_1A044F64139DF194` (`promotion_id`),
  KEY `IDX_1A044F6472F5A1AA` (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_promotion_channels`
--

INSERT INTO `sylius_promotion_channels` (`promotion_id`, `channel_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_promotion_coupon`
--

DROP TABLE IF EXISTS `sylius_promotion_coupon`;
CREATE TABLE IF NOT EXISTS `sylius_promotion_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usage_limit` int(11) DEFAULT NULL,
  `used` int(11) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `per_customer_usage_limit` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B04EBA8577153098` (`code`),
  KEY `IDX_B04EBA85139DF194` (`promotion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_promotion_order`
--

DROP TABLE IF EXISTS `sylius_promotion_order`;
CREATE TABLE IF NOT EXISTS `sylius_promotion_order` (
  `order_id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`,`promotion_id`),
  KEY `IDX_BF9CF6FB8D9F6D38` (`order_id`),
  KEY `IDX_BF9CF6FB139DF194` (`promotion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_promotion_order`
--

INSERT INTO `sylius_promotion_order` (`order_id`, `promotion_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(17, 2),
(18, 1),
(19, 1),
(20, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_promotion_rule`
--

DROP TABLE IF EXISTS `sylius_promotion_rule`;
CREATE TABLE IF NOT EXISTS `sylius_promotion_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configuration` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  KEY `IDX_2C188EA8139DF194` (`promotion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_promotion_rule`
--

INSERT INTO `sylius_promotion_rule` (`id`, `promotion_id`, `type`, `configuration`) VALUES
(1, 1, 'cart_quantity', 'a:1:{s:5:\"count\";i:0;}'),
(2, 2, 'item_total', 'a:1:{s:6:\"US_WEB\";a:1:{s:6:\"amount\";i:10000;}}');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_province`
--

DROP TABLE IF EXISTS `sylius_province`;
CREATE TABLE IF NOT EXISTS `sylius_province` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `abbreviation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B5618FE477153098` (`code`),
  UNIQUE KEY `UNIQ_B5618FE4F92F3E705E237E06` (`country_id`,`name`),
  KEY `IDX_B5618FE4F92F3E70` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_shipment`
--

DROP TABLE IF EXISTS `sylius_shipment`;
CREATE TABLE IF NOT EXISTS `sylius_shipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `method_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tracking` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FD707B3319883967` (`method_id`),
  KEY `IDX_FD707B338D9F6D38` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_shipment`
--

INSERT INTO `sylius_shipment` (`id`, `method_id`, `order_id`, `state`, `tracking`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'ready', NULL, '2019-06-21 20:48:04', '2019-06-21 20:48:06'),
(2, 2, 2, 'ready', NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(3, 2, 3, 'ready', NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(4, 1, 4, 'ready', NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(5, 2, 5, 'ready', NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(6, 1, 6, 'ready', NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(7, 2, 7, 'ready', NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(8, 2, 8, 'ready', NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(9, 2, 9, 'ready', NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(10, 2, 10, 'ready', NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(11, 1, 11, 'ready', NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(12, 1, 12, 'ready', NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:07'),
(13, 2, 13, 'ready', NULL, '2019-06-21 20:48:07', '2019-06-21 20:48:08'),
(14, 1, 14, 'ready', NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(15, 2, 15, 'ready', NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(16, 2, 16, 'ready', NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(17, 1, 17, 'ready', NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(18, 2, 18, 'ready', NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(19, 2, 19, 'ready', NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08'),
(20, 1, 20, 'ready', NULL, '2019-06-21 20:48:08', '2019-06-21 20:48:08');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_shipping_category`
--

DROP TABLE IF EXISTS `sylius_shipping_category`;
CREATE TABLE IF NOT EXISTS `sylius_shipping_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B1D6465277153098` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_shipping_method`
--

DROP TABLE IF EXISTS `sylius_shipping_method`;
CREATE TABLE IF NOT EXISTS `sylius_shipping_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `zone_id` int(11) NOT NULL,
  `tax_category_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configuration` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `category_requirement` int(11) NOT NULL,
  `calculator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_enabled` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `archived_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5FB0EE1177153098` (`code`),
  KEY `IDX_5FB0EE1112469DE2` (`category_id`),
  KEY `IDX_5FB0EE119F2C3FAB` (`zone_id`),
  KEY `IDX_5FB0EE119DF894ED` (`tax_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_shipping_method`
--

INSERT INTO `sylius_shipping_method` (`id`, `category_id`, `zone_id`, `tax_category_id`, `code`, `configuration`, `category_requirement`, `calculator`, `is_enabled`, `position`, `archived_at`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, NULL, 'ups', 'a:1:{s:6:\"US_WEB\";a:1:{s:6:\"amount\";i:451;}}', 1, 'flat_rate', 1, 0, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49'),
(2, NULL, 1, NULL, 'dhl_express', 'a:1:{s:6:\"US_WEB\";a:1:{s:6:\"amount\";i:5618;}}', 1, 'flat_rate', 1, 1, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49'),
(3, NULL, 1, NULL, 'fedex', 'a:1:{s:6:\"US_WEB\";a:1:{s:6:\"amount\";i:869;}}', 1, 'flat_rate', 0, 2, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_shipping_method_channels`
--

DROP TABLE IF EXISTS `sylius_shipping_method_channels`;
CREATE TABLE IF NOT EXISTS `sylius_shipping_method_channels` (
  `shipping_method_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY (`shipping_method_id`,`channel_id`),
  KEY `IDX_2D9833355F7D6850` (`shipping_method_id`),
  KEY `IDX_2D98333572F5A1AA` (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_shipping_method_channels`
--

INSERT INTO `sylius_shipping_method_channels` (`shipping_method_id`, `channel_id`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sylius_shipping_method_translation`
--

DROP TABLE IF EXISTS `sylius_shipping_method_translation`;
CREATE TABLE IF NOT EXISTS `sylius_shipping_method_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_shipping_method_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_2B37DB3D2C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_shipping_method_translation`
--

INSERT INTO `sylius_shipping_method_translation` (`id`, `translatable_id`, `name`, `description`, `locale`) VALUES
(1, 1, 'UPS', 'In minus vel perferendis rerum.', 'en_US'),
(2, 2, 'DHL Express', 'Sit non at deserunt aut perferendis voluptas accusamus.', 'en_US'),
(3, 3, 'FedEx', 'Aut ea ratione id dolores consequatur quo iure.', 'en_US');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_shop_billing_data`
--

DROP TABLE IF EXISTS `sylius_shop_billing_data`;
CREATE TABLE IF NOT EXISTS `sylius_shop_billing_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_shop_user`
--

DROP TABLE IF EXISTS `sylius_shop_user`;
CREATE TABLE IF NOT EXISTS `sylius_shop_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `email_verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `encoder_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7C2B74809395C3F3` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_shop_user`
--

INSERT INTO `sylius_shop_user` (`id`, `customer_id`, `username`, `username_canonical`, `enabled`, `salt`, `password`, `last_login`, `password_reset_token`, `password_requested_at`, `email_verification_token`, `verified_at`, `locked`, `expires_at`, `credentials_expire_at`, `roles`, `email`, `email_canonical`, `created_at`, `updated_at`, `encoder_name`) VALUES
(1, 1, 'shop@example.com', 'shop@example.com', 1, '1q0gxiapn7esgsso08o8oow0ookckw8', '$argon2i$v=19$m=1024,t=2,p=2$a2lhc1BYQTlTcHJkODNNcQ$rjHL28OSBvKIzhOD5i6e4WiShf+YLZf4HMJ0ByjoT9g', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49', 'argon2i'),
(2, 2, 'rae.glover@yahoo.com', 'rae.glover@yahoo.com', 1, 'pjiog98rhdwgcooosc4ok444oswcg8s', '$argon2i$v=19$m=1024,t=2,p=2$eDU1TVdzYUVXajJDTm1USg$1CLfrfkUQcnn2W4oMIK8nx/vB2HH1nvzrtgh4QWiaRo', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49', 'argon2i'),
(3, 3, 'maxine.runolfsdottir@schmidt.com', 'maxine.runolfsdottir@schmidt.com', 1, '6t4rzh8qrz8kk4404c0s80w80kowogo', '$argon2i$v=19$m=1024,t=2,p=2$VkVXQll4RzZ3Si8yeC9PWQ$+DaDTk1ZzM1fwJkolfJjoN62PQc4qDIp0uOdJ2YDNJA', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49', 'argon2i'),
(4, 4, 'kiehn.dixie@yahoo.com', 'kiehn.dixie@yahoo.com', 1, 'kef30tkaw3k08wkwsgcgk0ck44scgwo', '$argon2i$v=19$m=1024,t=2,p=2$WDR2dmVaNHRTN1Y1VEpHYg$nnQMl4MGYaghnz5YQhcAlcvbXPqbLGyD54+SGmmoyeI', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49', 'argon2i'),
(5, 5, 'xrempel@hotmail.com', 'xrempel@hotmail.com', 1, 'e394ekvwn2o8kg0s0g88400s40gcsg0', '$argon2i$v=19$m=1024,t=2,p=2$ZTVpYkNEZmpiU3R4UnNHQQ$8x5PIh9d8agt9mtMwSm8qffI0GFrpwnYAqCXcV0xrZ8', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49', 'argon2i'),
(6, 6, 'rice.clinton@yahoo.com', 'rice.clinton@yahoo.com', 1, '295xtshind340sc8w000kg0ssksgo0s', '$argon2i$v=19$m=1024,t=2,p=2$OXZ4dHoyRHJWcHkxNUM0Rg$Agt7en2vIxKCftI2GqVGrcdpmy/k2UD2s5o1XzxwxLU', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49', 'argon2i'),
(7, 7, 'lucienne79@kessler.info', 'lucienne79@kessler.info', 1, 'lp3m0nj1p5sgcc44oggcgk4owoows40', '$argon2i$v=19$m=1024,t=2,p=2$amFCZzlmRHU1dURDWkNnRA$fg+JnW5j6AqeFrNsFkFW+dY2LWgWI/dQY1waYycDZIA', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49', 'argon2i'),
(8, 8, 'zboncak.kirk@conroy.com', 'zboncak.kirk@conroy.com', 1, 'egvvf4n0e1skcogk8s0s0g8s04k404o', '$argon2i$v=19$m=1024,t=2,p=2$S3RDUk5mN2VCV1ZFNXBDcQ$SMnezDvlxbJU9Wkoe/ZYUwtHgansunGTehBM+a/0NuE', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49', 'argon2i'),
(9, 9, 'alberta.cummerata@koss.com', 'alberta.cummerata@koss.com', 1, 'ng6crd8xixw0g4ksggogowskgk04ow0', '$argon2i$v=19$m=1024,t=2,p=2$RDc5a0NsUUdoN3l0Z1gzLg$MiK7HpLR/QKiOQ3qAnckSqABLu4+OAxQQUyHxrPvLk4', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49', 'argon2i'),
(10, 10, 'konopelski.earnest@murray.com', 'konopelski.earnest@murray.com', 1, 's8oyo53do80kgww84w44gsowcs88440', '$argon2i$v=19$m=1024,t=2,p=2$Mkg2SFVIY1BRb3pHMlBpag$B7RzsAsYTCXUFYc5nHDLMUQgYQwZY04+Ye3YkUTgrrk', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:49', '2019-06-21 20:47:49', 'argon2i'),
(11, 11, 'elnora36@marvin.com', 'elnora36@marvin.com', 1, '8yk6gan1pgwsgco080ssgsgkok0gso4', '$argon2i$v=19$m=1024,t=2,p=2$dGo4V0s5U0NUVmNheU5PWg$sHnU7ekWGb9gA7dGj5ep8CRXCrInBP6BmMJb3aJykuc', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:50', '2019-06-21 20:47:50', 'argon2i'),
(12, 12, 'rosenbaum.fleta@gmail.com', 'rosenbaum.fleta@gmail.com', 1, '20t7oljsweu8gwgooc8scck8sswwwsw', '$argon2i$v=19$m=1024,t=2,p=2$NHBITFBFbE5pd0FLeTJqSQ$qM/Y1lK9EQZvO4kmILpq+aDl2Zq7h9NZqWaxq7JTTZQ', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:50', '2019-06-21 20:47:50', 'argon2i'),
(13, 13, 'mayer.vinnie@hotmail.com', 'mayer.vinnie@hotmail.com', 1, '3l1awt3jl2o04koc808osgw84c480o4', '$argon2i$v=19$m=1024,t=2,p=2$Qi92WFdKV0J0SEpQb0F5aQ$5w37+c1/IUI3Ftj924zC0tEo8bkpFKUQ7154HufvaL0', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:50', '2019-06-21 20:47:50', 'argon2i'),
(14, 14, 'piper.rempel@dibbert.org', 'piper.rempel@dibbert.org', 1, 'e2azliqd3ogk8g0ocgggko0c44408gs', '$argon2i$v=19$m=1024,t=2,p=2$YmNRYjJHcklsZUgzODhMYQ$LMCq3zprWICygwsIbsQeiN4PyH7q43yeuAmKMiZom68', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:50', '2019-06-21 20:47:50', 'argon2i'),
(15, 15, 'ashanahan@williamson.com', 'ashanahan@williamson.com', 1, '9909g6zfyccgw4gwgg0gggsggokwwo4', '$argon2i$v=19$m=1024,t=2,p=2$ZWxuclc1UkFpV0dxR1ZWSQ$T+S1C+A3Nc2a6vXlZSWO2Lo/jbpIx16fwO/pyg6isSY', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:50', '2019-06-21 20:47:50', 'argon2i'),
(16, 16, 'bjones@yahoo.com', 'bjones@yahoo.com', 1, 'p1kc5eog51c0ccgsokk8ogwg888gkw0', '$argon2i$v=19$m=1024,t=2,p=2$Z05HVVRVbTlqeU03OEZaTQ$Kbq7n5WnQg5n4khy6bCGcQ72ieZ+Vc+/oxRtcD6EXJg', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:50', '2019-06-21 20:47:50', 'argon2i'),
(17, 17, 'greenfelder.adan@gmail.com', 'greenfelder.adan@gmail.com', 1, '7alkjh149dwko4o88ck84gg40w8ckow', '$argon2i$v=19$m=1024,t=2,p=2$VjZGR080Z1pzUUFrSmtGRA$unXfBnlPp/+rd2U4mbU0NhN81Ky63l/turujMLI6gzw', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:50', '2019-06-21 20:47:50', 'argon2i'),
(18, 18, 'qlubowitz@gmail.com', 'qlubowitz@gmail.com', 1, '6luos0qi1vwoc44ok4oww8wok8ow84s', '$argon2i$v=19$m=1024,t=2,p=2$ODhPV1FMRkNMRlZ5cHJ6WQ$DBdFREspnidc2gWZVWryx53ptHlg+VoFMUntBkD3+Dg', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:50', '2019-06-21 20:47:50', 'argon2i'),
(19, 19, 'nienow.helmer@hotmail.com', 'nienow.helmer@hotmail.com', 1, 'fdcqpjqr42ok4s4c8000ksc844k8kkc', '$argon2i$v=19$m=1024,t=2,p=2$aXRUdTF0SE1TdU8uRWlxSQ$4J4dmkI0zEkSQUrhd9TyfZdbvOQ0qqvfQN1s2cJYMKA', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:50', '2019-06-21 20:47:50', 'argon2i'),
(20, 20, 'hodkiewicz.beth@boehm.net', 'hodkiewicz.beth@boehm.net', 1, '8booc7419fs4k4g4gwokksgc088o8s8', '$argon2i$v=19$m=1024,t=2,p=2$WTg1OUEuQ1NpcjRsN3c1Tw$DQgimd1ikkJRo+rIhe8LJ0oUmTxmouGs6eXeWLNTi08', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:50', '2019-06-21 20:47:50', 'argon2i'),
(21, 21, 'pwitting@gmail.com', 'pwitting@gmail.com', 1, 'hg215eq9hn4800gwgsg480wk08cw0cw', '$argon2i$v=19$m=1024,t=2,p=2$UUQ0Y1NEaE85T0lBTWJkdg$c/8V3QbTEZMHliwgaCmyNZlIoP2XKxIr0gP8hkQ3vII', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', NULL, NULL, '2019-06-21 20:47:50', '2019-06-21 20:47:50', 'argon2i');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_taxon`
--

DROP TABLE IF EXISTS `sylius_taxon`;
CREATE TABLE IF NOT EXISTS `sylius_taxon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tree_root` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tree_left` int(11) NOT NULL,
  `tree_right` int(11) NOT NULL,
  `tree_level` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CFD811CA77153098` (`code`),
  KEY `IDX_CFD811CAA977936C` (`tree_root`),
  KEY `IDX_CFD811CA727ACA70` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_taxon`
--

INSERT INTO `sylius_taxon` (`id`, `tree_root`, `parent_id`, `code`, `tree_left`, `tree_right`, `tree_level`, `position`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'category', 1, 14, 0, 0, '2019-06-21 20:47:50', '2019-06-21 20:47:50'),
(2, 1, 1, 'mugs', 2, 3, 1, 0, '2019-06-21 20:47:50', '2019-06-21 20:47:50'),
(3, 1, 1, 'stickers', 4, 5, 1, 1, '2019-06-21 20:47:54', '2019-06-21 20:47:54'),
(4, 1, 1, 'books', 6, 7, 1, 2, '2019-06-21 20:47:55', '2019-06-21 20:47:55'),
(5, 1, 1, 't_shirts', 8, 13, 1, 3, '2019-06-21 20:47:57', '2019-06-21 20:47:57'),
(6, 1, 5, 'mens_t_shirts', 9, 10, 2, 0, '2019-06-21 20:47:57', '2019-06-21 20:47:57'),
(7, 1, 5, 'womens_t_shirts', 11, 12, 2, 1, '2019-06-21 20:47:57', '2019-06-21 20:47:57');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_taxon_image`
--

DROP TABLE IF EXISTS `sylius_taxon_image`;
CREATE TABLE IF NOT EXISTS `sylius_taxon_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DBE52B287E3C61F9` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_taxon_translation`
--

DROP TABLE IF EXISTS `sylius_taxon_translation`;
CREATE TABLE IF NOT EXISTS `sylius_taxon_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_uidx` (`locale`,`slug`),
  UNIQUE KEY `sylius_taxon_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_1487DFCF2C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_taxon_translation`
--

INSERT INTO `sylius_taxon_translation` (`id`, `translatable_id`, `name`, `slug`, `description`, `locale`) VALUES
(1, 1, 'Category', 'category', 'Commodi porro dolor occaecati autem minus. Et explicabo qui totam sed rerum animi sint facere. Aliquam aut ut aliquid voluptate. Ipsum blanditiis distinctio eos deserunt blanditiis non. Rerum cupiditate hic omnis eveniet recusandae.', 'en_US'),
(2, 2, 'Mugs', 'category/mugs', 'Dolor laborum distinctio voluptas dolor occaecati itaque. Quidem dolores sit aut mollitia qui sapiente provident harum. Ea architecto itaque tenetur nostrum ut ut quae. Accusamus sed ipsam ex eveniet nam corporis.', 'en_US'),
(3, 2, 'Tasses', 'category/tasses', 'Voluptatem inventore asperiores qui quae ex. Rerum sit amet cumque magni. Iure sequi molestias iste odit laudantium quam quidem.', 'fr_FR'),
(4, 3, 'Stickers', 'category/stickers', 'Labore exercitationem minus vel et et minima autem. Sint et et rerum eligendi. Ratione velit cupiditate odio corrupti. At tempora voluptates vel quia et voluptate cum.', 'en_US'),
(5, 3, 'Étiquettes', 'category/etiquettes', 'Dicta ea ut ut voluptate. Excepturi dolor esse provident ullam amet ab dolor. Rerum voluptatum dolor quia dolores. Molestiae quia aperiam vero repellendus molestiae veniam sequi.', 'fr_FR'),
(6, 4, 'Books', 'category/books', 'Dolor labore cupiditate ut in ut dicta. Minima numquam sit accusantium ut saepe aut vitae. Consequatur nihil doloribus sunt aut. Libero ut voluptatibus et iste aut ut sequi.', 'en_US'),
(7, 4, 'Livres', 'category/livres', 'Sit vero maxime accusamus unde sapiente. Eos qui iusto beatae et. Sunt voluptatem rerum nesciunt laudantium corrupti explicabo.', 'fr_FR'),
(8, 5, 'T-Shirts', 't-shirts', 'Doloremque ut quo soluta ut adipisci reprehenderit a est. Consectetur harum eos quia repellendus ullam. Cum tenetur voluptatem est id quia. Et sunt et omnis rerum facere ducimus amet.', 'en_US'),
(9, 6, 'Men', 't-shirts/men', 'Iure rerum aut unde quasi at labore repellat. Soluta numquam velit numquam sed. Fuga non itaque ut commodi incidunt.', 'en_US'),
(10, 6, 'Hommes', 't-shirts/hommes', 'Similique amet est praesentium perspiciatis. Dolorem eius qui repudiandae voluptatem culpa qui. Laboriosam est nihil nihil assumenda optio laborum. Ipsum corporis dicta corporis ullam qui.', 'fr_FR'),
(11, 7, 'Women', 't-shirts/women', 'Aut facere asperiores et tenetur nihil. Quia est laudantium est voluptatem maiores. Expedita officia incidunt id aut quis eaque.', 'en_US'),
(12, 7, 'Hommes', 't-shirts/femmes', 'Quia et eveniet qui voluptatem quia quo impedit. Aut voluptatibus exercitationem assumenda placeat quidem. Quidem omnis voluptas voluptatibus reiciendis.', 'fr_FR');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_tax_category`
--

DROP TABLE IF EXISTS `sylius_tax_category`;
CREATE TABLE IF NOT EXISTS `sylius_tax_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_221EB0BE77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_tax_category`
--

INSERT INTO `sylius_tax_category` (`id`, `code`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'clothing', 'Clothing', 'Quos enim quis laboriosam amet. Et voluptas et quis nobis nobis. Qui voluptas est exercitationem ipsa.', '2019-06-21 20:47:50', '2019-06-21 20:47:50'),
(2, 'books', 'Books', 'Ullam vitae asperiores consectetur corporis earum. Sit illum voluptatem animi et. Facere minima necessitatibus ratione totam rerum. Est aut et aliquid explicabo iusto excepturi.', '2019-06-21 20:47:50', '2019-06-21 20:47:50'),
(3, 'other', 'Other', 'Sunt error voluptatem ut aut qui qui. Pariatur occaecati optio sint temporibus eaque sed. Sit ut aliquid perferendis quia. Rerum animi dolorem fugiat ea est.', '2019-06-21 20:47:50', '2019-06-21 20:47:50');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_tax_rate`
--

DROP TABLE IF EXISTS `sylius_tax_rate`;
CREATE TABLE IF NOT EXISTS `sylius_tax_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(10,5) NOT NULL,
  `included_in_price` tinyint(1) NOT NULL,
  `calculator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_3CD86B2E77153098` (`code`),
  KEY `IDX_3CD86B2E12469DE2` (`category_id`),
  KEY `IDX_3CD86B2E9F2C3FAB` (`zone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_tax_rate`
--

INSERT INTO `sylius_tax_rate` (`id`, `category_id`, `zone_id`, `code`, `name`, `amount`, `included_in_price`, `calculator`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'clothing_sales_tax_7', 'Clothing Sales Tax 7%', '0.07000', 1, 'default', '2019-06-21 20:47:50', '2019-06-21 20:47:50'),
(2, 2, 1, 'books_sales_tax_2', 'Books Sales Tax 2%', '0.02000', 1, 'default', '2019-06-21 20:47:50', '2019-06-21 20:47:50'),
(3, 3, 1, 'sales_tax_20', 'Sales Tax 20%', '0.20000', 0, 'default', '2019-06-21 20:47:50', '2019-06-21 20:47:50');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_user_oauth`
--

DROP TABLE IF EXISTS `sylius_user_oauth`;
CREATE TABLE IF NOT EXISTS `sylius_user_oauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refresh_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_provider` (`user_id`,`provider`),
  KEY `IDX_C3471B78A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_zone`
--

DROP TABLE IF EXISTS `sylius_zone`;
CREATE TABLE IF NOT EXISTS `sylius_zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7BE2258E77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_zone`
--

INSERT INTO `sylius_zone` (`id`, `code`, `name`, `type`, `scope`) VALUES
(1, 'US', 'United States of America', 'country', 'all');

-- --------------------------------------------------------

--
-- Structure de la table `sylius_zone_member`
--

DROP TABLE IF EXISTS `sylius_zone_member`;
CREATE TABLE IF NOT EXISTS `sylius_zone_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `belongs_to` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E8B5ABF34B0E929B77153098` (`belongs_to`,`code`),
  KEY `IDX_E8B5ABF34B0E929B` (`belongs_to`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sylius_zone_member`
--

INSERT INTO `sylius_zone_member` (`id`, `belongs_to`, `code`) VALUES
(1, 1, 'US');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `odiseo_blog_articles_categories`
--
ALTER TABLE `odiseo_blog_articles_categories`
  ADD CONSTRAINT `FK_F090056C12469DE2` FOREIGN KEY (`category_id`) REFERENCES `odiseo_blog_article_category` (`id`),
  ADD CONSTRAINT `FK_F090056C7294869C` FOREIGN KEY (`article_id`) REFERENCES `odiseo_blog_article` (`id`);

--
-- Contraintes pour la table `odiseo_blog_articles_channels`
--
ALTER TABLE `odiseo_blog_articles_channels`
  ADD CONSTRAINT `FK_A4C0CF5F7294869C` FOREIGN KEY (`article_id`) REFERENCES `odiseo_blog_article` (`id`),
  ADD CONSTRAINT `FK_A4C0CF5F72F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`);

--
-- Contraintes pour la table `odiseo_blog_article_category_translation`
--
ALTER TABLE `odiseo_blog_article_category_translation`
  ADD CONSTRAINT `FK_2F9EF9492C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `odiseo_blog_article_category` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `odiseo_blog_article_image`
--
ALTER TABLE `odiseo_blog_article_image`
  ADD CONSTRAINT `FK_F97399C97E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `odiseo_blog_article` (`id`);

--
-- Contraintes pour la table `odiseo_blog_article_translation`
--
ALTER TABLE `odiseo_blog_article_translation`
  ADD CONSTRAINT `FK_7A606A2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `odiseo_blog_article` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_address`
--
ALTER TABLE `sylius_address`
  ADD CONSTRAINT `FK_B97FF0589395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `sylius_customer` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_adjustment`
--
ALTER TABLE `sylius_adjustment`
  ADD CONSTRAINT `FK_ACA6E0F28D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `sylius_order` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_ACA6E0F2E415FB15` FOREIGN KEY (`order_item_id`) REFERENCES `sylius_order_item` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_ACA6E0F2F720C233` FOREIGN KEY (`order_item_unit_id`) REFERENCES `sylius_order_item_unit` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_admin_api_access_token`
--
ALTER TABLE `sylius_admin_api_access_token`
  ADD CONSTRAINT `FK_2AA4915D19EB6921` FOREIGN KEY (`client_id`) REFERENCES `sylius_admin_api_client` (`id`),
  ADD CONSTRAINT `FK_2AA4915DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sylius_admin_user` (`id`);

--
-- Contraintes pour la table `sylius_admin_api_auth_code`
--
ALTER TABLE `sylius_admin_api_auth_code`
  ADD CONSTRAINT `FK_E366D84819EB6921` FOREIGN KEY (`client_id`) REFERENCES `sylius_admin_api_client` (`id`),
  ADD CONSTRAINT `FK_E366D848A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sylius_admin_user` (`id`);

--
-- Contraintes pour la table `sylius_admin_api_refresh_token`
--
ALTER TABLE `sylius_admin_api_refresh_token`
  ADD CONSTRAINT `FK_9160E3FA19EB6921` FOREIGN KEY (`client_id`) REFERENCES `sylius_admin_api_client` (`id`),
  ADD CONSTRAINT `FK_9160E3FAA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sylius_admin_user` (`id`);

--
-- Contraintes pour la table `sylius_channel`
--
ALTER TABLE `sylius_channel`
  ADD CONSTRAINT `FK_16C8119E3101778E` FOREIGN KEY (`base_currency_id`) REFERENCES `sylius_currency` (`id`),
  ADD CONSTRAINT `FK_16C8119E743BF776` FOREIGN KEY (`default_locale_id`) REFERENCES `sylius_locale` (`id`),
  ADD CONSTRAINT `FK_16C8119EA978C17` FOREIGN KEY (`default_tax_zone_id`) REFERENCES `sylius_zone` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_16C8119EB5282EDF` FOREIGN KEY (`shop_billing_data_id`) REFERENCES `sylius_shop_billing_data` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_channel_currencies`
--
ALTER TABLE `sylius_channel_currencies`
  ADD CONSTRAINT `FK_AE491F9338248176` FOREIGN KEY (`currency_id`) REFERENCES `sylius_currency` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_AE491F9372F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_channel_locales`
--
ALTER TABLE `sylius_channel_locales`
  ADD CONSTRAINT `FK_786B7A8472F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_786B7A84E559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `sylius_locale` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_channel_pricing`
--
ALTER TABLE `sylius_channel_pricing`
  ADD CONSTRAINT `FK_7801820CA80EF684` FOREIGN KEY (`product_variant_id`) REFERENCES `sylius_product_variant` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_customer`
--
ALTER TABLE `sylius_customer`
  ADD CONSTRAINT `FK_7E82D5E6BD94FB16` FOREIGN KEY (`default_address_id`) REFERENCES `sylius_address` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_7E82D5E6D2919A68` FOREIGN KEY (`customer_group_id`) REFERENCES `sylius_customer_group` (`id`);

--
-- Contraintes pour la table `sylius_exchange_rate`
--
ALTER TABLE `sylius_exchange_rate`
  ADD CONSTRAINT `FK_5F52B852A76BEED` FOREIGN KEY (`source_currency`) REFERENCES `sylius_currency` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_5F52B85B3FD5856` FOREIGN KEY (`target_currency`) REFERENCES `sylius_currency` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_order`
--
ALTER TABLE `sylius_order`
  ADD CONSTRAINT `FK_6196A1F917B24436` FOREIGN KEY (`promotion_coupon_id`) REFERENCES `sylius_promotion_coupon` (`id`),
  ADD CONSTRAINT `FK_6196A1F94D4CFF2B` FOREIGN KEY (`shipping_address_id`) REFERENCES `sylius_address` (`id`),
  ADD CONSTRAINT `FK_6196A1F972F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`),
  ADD CONSTRAINT `FK_6196A1F979D0C0E4` FOREIGN KEY (`billing_address_id`) REFERENCES `sylius_address` (`id`),
  ADD CONSTRAINT `FK_6196A1F99395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `sylius_customer` (`id`);

--
-- Contraintes pour la table `sylius_order_item`
--
ALTER TABLE `sylius_order_item`
  ADD CONSTRAINT `FK_77B587ED3B69A9AF` FOREIGN KEY (`variant_id`) REFERENCES `sylius_product_variant` (`id`),
  ADD CONSTRAINT `FK_77B587ED8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `sylius_order` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_order_item_unit`
--
ALTER TABLE `sylius_order_item_unit`
  ADD CONSTRAINT `FK_82BF226E7BE036FC` FOREIGN KEY (`shipment_id`) REFERENCES `sylius_shipment` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_82BF226EE415FB15` FOREIGN KEY (`order_item_id`) REFERENCES `sylius_order_item` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_payment`
--
ALTER TABLE `sylius_payment`
  ADD CONSTRAINT `FK_D9191BD419883967` FOREIGN KEY (`method_id`) REFERENCES `sylius_payment_method` (`id`),
  ADD CONSTRAINT `FK_D9191BD48D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `sylius_order` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_payment_method`
--
ALTER TABLE `sylius_payment_method`
  ADD CONSTRAINT `FK_A75B0B0DF23D6140` FOREIGN KEY (`gateway_config_id`) REFERENCES `sylius_gateway_config` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `sylius_payment_method_channels`
--
ALTER TABLE `sylius_payment_method_channels`
  ADD CONSTRAINT `FK_543AC0CC5AA1164F` FOREIGN KEY (`payment_method_id`) REFERENCES `sylius_payment_method` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_543AC0CC72F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_payment_method_translation`
--
ALTER TABLE `sylius_payment_method_translation`
  ADD CONSTRAINT `FK_966BE3A12C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_payment_method` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product`
--
ALTER TABLE `sylius_product`
  ADD CONSTRAINT `FK_677B9B74731E505` FOREIGN KEY (`main_taxon_id`) REFERENCES `sylius_taxon` (`id`);

--
-- Contraintes pour la table `sylius_product_association`
--
ALTER TABLE `sylius_product_association`
  ADD CONSTRAINT `FK_48E9CDAB4584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_48E9CDABB1E1C39` FOREIGN KEY (`association_type_id`) REFERENCES `sylius_product_association_type` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_association_product`
--
ALTER TABLE `sylius_product_association_product`
  ADD CONSTRAINT `FK_A427B9834584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_A427B983EFB9C8A5` FOREIGN KEY (`association_id`) REFERENCES `sylius_product_association` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_association_type_translation`
--
ALTER TABLE `sylius_product_association_type_translation`
  ADD CONSTRAINT `FK_4F618E52C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_product_association_type` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_attribute_translation`
--
ALTER TABLE `sylius_product_attribute_translation`
  ADD CONSTRAINT `FK_93850EBA2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_product_attribute` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_attribute_value`
--
ALTER TABLE `sylius_product_attribute_value`
  ADD CONSTRAINT `FK_8A053E544584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_8A053E54B6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `sylius_product_attribute` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_channels`
--
ALTER TABLE `sylius_product_channels`
  ADD CONSTRAINT `FK_F9EF269B4584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_F9EF269B72F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_image`
--
ALTER TABLE `sylius_product_image`
  ADD CONSTRAINT `FK_88C64B2D7E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_image_product_variants`
--
ALTER TABLE `sylius_product_image_product_variants`
  ADD CONSTRAINT `FK_8FFDAE8D3B69A9AF` FOREIGN KEY (`variant_id`) REFERENCES `sylius_product_variant` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_8FFDAE8D3DA5256D` FOREIGN KEY (`image_id`) REFERENCES `sylius_product_image` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_options`
--
ALTER TABLE `sylius_product_options`
  ADD CONSTRAINT `FK_2B5FF0094584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_2B5FF009A7C41D6F` FOREIGN KEY (`option_id`) REFERENCES `sylius_product_option` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_option_translation`
--
ALTER TABLE `sylius_product_option_translation`
  ADD CONSTRAINT `FK_CBA491AD2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_product_option` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_option_value`
--
ALTER TABLE `sylius_product_option_value`
  ADD CONSTRAINT `FK_F7FF7D4BA7C41D6F` FOREIGN KEY (`option_id`) REFERENCES `sylius_product_option` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_option_value_translation`
--
ALTER TABLE `sylius_product_option_value_translation`
  ADD CONSTRAINT `FK_8D4382DC2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_product_option_value` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_review`
--
ALTER TABLE `sylius_product_review`
  ADD CONSTRAINT `FK_C7056A994584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_C7056A99F675F31B` FOREIGN KEY (`author_id`) REFERENCES `sylius_customer` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_taxon`
--
ALTER TABLE `sylius_product_taxon`
  ADD CONSTRAINT `FK_169C6CD94584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_169C6CD9DE13F470` FOREIGN KEY (`taxon_id`) REFERENCES `sylius_taxon` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_translation`
--
ALTER TABLE `sylius_product_translation`
  ADD CONSTRAINT `FK_105A9082C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_variant`
--
ALTER TABLE `sylius_product_variant`
  ADD CONSTRAINT `FK_A29B5234584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_A29B5239DF894ED` FOREIGN KEY (`tax_category_id`) REFERENCES `sylius_tax_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_A29B5239E2D1A41` FOREIGN KEY (`shipping_category_id`) REFERENCES `sylius_shipping_category` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `sylius_product_variant_option_value`
--
ALTER TABLE `sylius_product_variant_option_value`
  ADD CONSTRAINT `FK_76CDAFA13B69A9AF` FOREIGN KEY (`variant_id`) REFERENCES `sylius_product_variant` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_76CDAFA1D957CA06` FOREIGN KEY (`option_value_id`) REFERENCES `sylius_product_option_value` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_product_variant_translation`
--
ALTER TABLE `sylius_product_variant_translation`
  ADD CONSTRAINT `FK_8DC18EDC2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_product_variant` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_promotion_action`
--
ALTER TABLE `sylius_promotion_action`
  ADD CONSTRAINT `FK_933D0915139DF194` FOREIGN KEY (`promotion_id`) REFERENCES `sylius_promotion` (`id`);

--
-- Contraintes pour la table `sylius_promotion_channels`
--
ALTER TABLE `sylius_promotion_channels`
  ADD CONSTRAINT `FK_1A044F64139DF194` FOREIGN KEY (`promotion_id`) REFERENCES `sylius_promotion` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_1A044F6472F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_promotion_coupon`
--
ALTER TABLE `sylius_promotion_coupon`
  ADD CONSTRAINT `FK_B04EBA85139DF194` FOREIGN KEY (`promotion_id`) REFERENCES `sylius_promotion` (`id`);

--
-- Contraintes pour la table `sylius_promotion_order`
--
ALTER TABLE `sylius_promotion_order`
  ADD CONSTRAINT `FK_BF9CF6FB139DF194` FOREIGN KEY (`promotion_id`) REFERENCES `sylius_promotion` (`id`),
  ADD CONSTRAINT `FK_BF9CF6FB8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `sylius_order` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_promotion_rule`
--
ALTER TABLE `sylius_promotion_rule`
  ADD CONSTRAINT `FK_2C188EA8139DF194` FOREIGN KEY (`promotion_id`) REFERENCES `sylius_promotion` (`id`);

--
-- Contraintes pour la table `sylius_province`
--
ALTER TABLE `sylius_province`
  ADD CONSTRAINT `FK_B5618FE4F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `sylius_country` (`id`);

--
-- Contraintes pour la table `sylius_shipment`
--
ALTER TABLE `sylius_shipment`
  ADD CONSTRAINT `FK_FD707B3319883967` FOREIGN KEY (`method_id`) REFERENCES `sylius_shipping_method` (`id`),
  ADD CONSTRAINT `FK_FD707B338D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `sylius_order` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_shipping_method`
--
ALTER TABLE `sylius_shipping_method`
  ADD CONSTRAINT `FK_5FB0EE1112469DE2` FOREIGN KEY (`category_id`) REFERENCES `sylius_shipping_category` (`id`),
  ADD CONSTRAINT `FK_5FB0EE119DF894ED` FOREIGN KEY (`tax_category_id`) REFERENCES `sylius_tax_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_5FB0EE119F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `sylius_zone` (`id`);

--
-- Contraintes pour la table `sylius_shipping_method_channels`
--
ALTER TABLE `sylius_shipping_method_channels`
  ADD CONSTRAINT `FK_2D9833355F7D6850` FOREIGN KEY (`shipping_method_id`) REFERENCES `sylius_shipping_method` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_2D98333572F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_shipping_method_translation`
--
ALTER TABLE `sylius_shipping_method_translation`
  ADD CONSTRAINT `FK_2B37DB3D2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_shipping_method` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_shop_user`
--
ALTER TABLE `sylius_shop_user`
  ADD CONSTRAINT `FK_7C2B74809395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `sylius_customer` (`id`);

--
-- Contraintes pour la table `sylius_taxon`
--
ALTER TABLE `sylius_taxon`
  ADD CONSTRAINT `FK_CFD811CA727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `sylius_taxon` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_CFD811CAA977936C` FOREIGN KEY (`tree_root`) REFERENCES `sylius_taxon` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_taxon_image`
--
ALTER TABLE `sylius_taxon_image`
  ADD CONSTRAINT `FK_DBE52B287E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `sylius_taxon` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_taxon_translation`
--
ALTER TABLE `sylius_taxon_translation`
  ADD CONSTRAINT `FK_1487DFCF2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_taxon` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sylius_tax_rate`
--
ALTER TABLE `sylius_tax_rate`
  ADD CONSTRAINT `FK_3CD86B2E12469DE2` FOREIGN KEY (`category_id`) REFERENCES `sylius_tax_category` (`id`),
  ADD CONSTRAINT `FK_3CD86B2E9F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `sylius_zone` (`id`);

--
-- Contraintes pour la table `sylius_user_oauth`
--
ALTER TABLE `sylius_user_oauth`
  ADD CONSTRAINT `FK_C3471B78A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sylius_shop_user` (`id`);

--
-- Contraintes pour la table `sylius_zone_member`
--
ALTER TABLE `sylius_zone_member`
  ADD CONSTRAINT `FK_E8B5ABF34B0E929B` FOREIGN KEY (`belongs_to`) REFERENCES `sylius_zone` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

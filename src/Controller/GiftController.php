<?php

namespace App\Controller;

use App\Entity\Gift\Gift;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class GiftController extends AbstractController
{
public function index(Request $request)
{


$gift = new Gift();


$form = $this->createFormBuilder($gift)
    ->add('nom', TextType::class)
    ->add('refPiece', TextType::class)
    ->add('prix', NumberType::class)
    ->add('quantityDisponible', NumberType::class)
    ->add('save', SubmitType::class, ['label' => 'Ajouter Piece Détachée'])
    ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {

        $gift = $form->getData();
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($gift);
        $entityManager->flush();

        return $this->redirectToRoute('show_piece_detachee');
    }


    return $this->render('gift/new.html.twig', [
    'form' => $form->createView(),
]);
}

public function lotery(){
    $em = $this->getDoctrine()->getManager();
    $connection = $em->getConnection();
    $statement = $connection->prepare("SELECT * from gift order by RAND() limit 1");
    $statement->execute();
    $results = $statement->fetchAll();
    echo '<div style="background: lightgray; position : absolute; top : 0; bottom: 0; right:0; left:0;">';
    echo "<div  style= \"border: 2px solid white; border-radius :20px ; width : 500px; margin: auto; padding: 20px; background: white\">";
    echo "<h1 style=\"font-weight: bold; color : grey\"> LE GAGNANT EST : </h1>". $results[0]['email'];
    echo "<h1 style=\"font-weight: bold; color : grey\"> PRENOM  : </h1>". $results[0]['firstname'];
    echo "<h1 style=\"font-weight: bold; color : grey\"> NOM  </h1>". $results[0]['lastname'];
    echo"</div>";
    echo"</div>";
  
    exit();
}

    public function show(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT * from gift");
        $statement->execute();
        $pieceDetaches = $statement->fetchAll();
        //$pieceDetaches = $em->getRepository('AppBundle:Gift')->findAll();

        return $this->render('gift/show.html.twig', array(
            'pieceDetaches' => $pieceDetaches,
        ));

    }

    public function edit(Request $request,Gift $gift)
    {

        $deleteForm = $this->createDeleteForm($gift);

        $editForm = $this->createFormBuilder($gift)
            ->add('nom', TextType::class)
            ->add('refPiece', TextType::class)
            ->add('prix', NumberType::class)
            ->add('quantityDisponible', NumberType::class)
            ->add('save', SubmitType::class, ['label' => 'Modifier'])
            ->getForm();


        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('edit_piece_detachee', array('id' => $gift->getId()));
        }

        return $this->render('gift/edit.html.twig', array(
            'gift' => $gift,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));


    }

    /**
     * Creates a form to delete a gift entity.
     *
     * @param Country $gift The gift entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Gift $gift)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('delete_piece_detachee', array('id' => $gift->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }


    /**
     * Deletes a gift entity.
     *
     */
    public function delete(Request $request, Gift $gift)
    {
        $form = $this->createDeleteForm($gift);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($gift);
            $em->flush();
        }

        return $this->redirectToRoute('show_piece_detachee');
    }


}

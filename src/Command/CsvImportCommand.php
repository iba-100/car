<?php


namespace App\Command;

use App\Entity\Gift\Gift;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CsvImportCommand extends Command
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CsvImportCommand constructor.
     *
     * @param EntityManagerInterface $em
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->em = $em;
    }

    /**
     * Configure
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this
            ->setName('csv:import')
            ->setDescription('Imports the piece CSV data file')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);


        $reader = Reader::createFromPath('%kernel.root_dir%/../src/Data/importPiece.csv');

        $results = $reader->fetchAssoc();
        //$io->progressStart(iterator_count($results));

        var_dump($results);
        foreach ($results as $row) {

                $pieceDetachee = (new Gift())
                    ->setRefPiece($row['RefPiece'])
                    ->setNom($row['Nom'])
                    ->setPrix($row['Prix'])
                    ->getQuantityDisponible($row['Quantite'])
                ;

            $this->em->persist($pieceDetachee);
        }

        $this->em->flush();
        //$io->progressFinish();
        $io->success('Command exited cleanly!');
    }


}
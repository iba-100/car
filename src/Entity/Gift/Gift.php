<?php

declare(strict_types=1);

namespace App\Entity\Gift;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;

/**
 * Gift.
 * @ORM\Table(name="gift")
 * @MappedSuperclass
 * @ORM\Entity()
 */
class Gift
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $refPiece;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantityDisponible;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRefPiece(): ?string
    {
        return $this->refPiece;
    }

    public function setRefPiece(?string $refPiece): self
    {
        $this->refPiece = $refPiece;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getQuantityDisponible(): ?int
    {
        return $this->quantityDisponible;
    }

    public function setQuantityDisponible(?int $quantityDisponible): self
    {
        $this->quantityDisponible = $quantityDisponible;

        return $this;
    }
}

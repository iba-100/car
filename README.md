<p align="center">
    <a href="https://sylius.com" target="_blank">
        <img src="https://demo.sylius.com/assets/shop/img/logo.png" />
    </a>
</p>

<h1 align="center">Sylius Standard Edition</h1>

<p align="center">This is Sylius Standard Edition repository for starting new projects.</p>

About
-----

Pour excuter le projet en local:

    1) recuperer le projet sur git: https://gitlab.com/iba-100/car.git
    2) Executer la commande "composer update"
    3) Importer la base de donnée qui est dans le dossier bdd qui est à la racine du projet
    4) Excuter "php bin/console server:start"

 Vous trouverez la base de donnée dans bdd
 Pour la gestion des pieces il est disponible sur :
        ../en_US/add-pieces_detachees
        ../en_US/pieces_detachees
        .....
        
  
  Pour les Pieces qui s'affichent sur la page détails de produit c'est disponible
  

Pour Importer un fichier CSV contenant des pieces détachées il faut:

         Déposer un fichir .csv dans le dossier "/src/Data/impoerPiece.csv"
             example de contenu :
                 RefPiece;Nom;Prix;Quantite
                 1;test1; 10.23;20
                 2;test2; 15.00;30
                 3;test3; 20.54;40
                 
          Pour valider l'import il faut executer la commande suivante via un terminal:
            
            "php bin/console csv:import"
            
            
            